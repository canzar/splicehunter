/* Copyright 2012 Tobias Marschall
 *
 * This file is part of CLEVER.
 *
 * Adapted by Stefan Canzar for use in SpliceHunter
 *
 * You should have received a copy of the GNU General Public License
 * along with CLEVER.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef NAMEDDNASEQUENCE_H
#define NAMEDDNASEQUENCE_H

#include <string>
#include <vector>

using std::string;

class NamedDnaSequence {
private:
	bool case_sensitive;
	string name;
	size_t length;
	std::vector<unsigned char> sequence;
	
	char decode(unsigned char c) const;       
	unsigned char encode(char c) const;
public:
	NamedDnaSequence(const string& name);
	NamedDnaSequence(const string& name, const string& sequence);
	NamedDnaSequence(const string& name, bool case_sensitive);
	NamedDnaSequence(const string& name, const string& sequence, bool case_sensitive);
	~NamedDnaSequence() {}

	void append(const string& s);

	const std::string& getName() const { return name; }
	size_t size() const { return length; }

	char operator[](size_t pos) const;
    string substr(size_t start, size_t end) const;
};

#endif /* NAMEDDNASEQUENCE_H */
