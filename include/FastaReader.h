/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef FASTAREADER_H
#define FASTAREADER_H

#include <iostream>
#include <sstream>
#include <unordered_map>
#include <memory>
#include <stdexcept>

#include "NamedDnaSequence.h"

class FastaReader {
private:
	FastaReader() {}
public:	
	static std::unique_ptr<std::unordered_map<std::string,NamedDnaSequence*> > parse(std::istream& is) {
	  std::unique_ptr<std::unordered_map<std::string,NamedDnaSequence*> > result(new std::unordered_map<std::string,NamedDnaSequence*>());
	  std::string line;
	  int n = 1;
	  NamedDnaSequence* current = 0;
	  while (std::getline(is,line)) {
	    if (line.size()==0) continue;	    
	    size_t start = line.find_first_not_of(" \t\r\n");
	    size_t end = line.find_last_not_of(" \t\r\n");
	    line = line.substr(start, end-start+1);
	    if (line.size()==0) continue;
	    if (line[0] == '>') {	     
	      start = line.find_first_not_of(" \t", 1);
	      end = line.find_first_of(" ", start);
	      if (end == std::string::npos) end = line.size();
	      std::string name = line.substr(start,end-start);
	      std::cerr << "Reading reference sequence \"" << name << "\"" << std::endl;
	      if (result->find(name)!=result->end()) {
		delete result->at(name);
	      }
	      current = new NamedDnaSequence(name);
	      (*result)[name] = current;
	    } else {
	      if (current == 0) {
		std::ostringstream oss;
		oss << "Error parsing FASTA input. Offending line: " << n << ": \"" << line << "\"";		
		throw std::runtime_error(oss.str());
	      } else {
		current->append(line);
	      }
	    }
	    n += 1;
	  }	 
	  return result;
	}	
};

#endif /* FASTAREADER_H */
