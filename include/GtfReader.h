/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef GTFREADER_H
#define GTFREADER_H

#include <string>
#include <vector>
#include <map>

using std::string;
using std::vector;

class GtfReader {
 public:
  struct Transcript
  {
    unsigned id;
    string name;
    vector<std::pair<unsigned,unsigned> > exons;
    unsigned start_codon;
  };
 private:
  std::map<unsigned, vector<vector<Transcript> > > knownTranscripts;
  std::map<unsigned, vector<string> > geneNames;
  std::map<unsigned, string> transId2Name;
  std::map<unsigned, string> contigId2Name;
  std::map<string, unsigned> contigName2Id;
 public:  
  GtfReader(const string& filename);  
  const std::map<unsigned, vector<vector<Transcript> > >& getKnownTranscripts();
  void get_introns(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_introns, 
                   std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_introns,
                   unsigned min_intron_length = 1) const;
  void get_introns(std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_introns,
                   std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_introns,
                   unsigned min_intron_length = 1) const;
  void get_introns(vector<std::pair<unsigned,unsigned> >& gene_introns,
		   unsigned contig_id, 
		   unsigned gene, 
		   unsigned min_intron_length = 1) const;
  void get_exons(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_exons,
                 std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_exons,
                 unsigned min_exon_length = 1) const;    
  void get_exons(vector<std::pair<unsigned,unsigned> >& gene_exons,
		 unsigned contig_id,
		 unsigned gene,
		 unsigned min_exon_length = 1) const;
  void get_splice_sites(std::map<unsigned, vector<unsigned> >& fw_ss,
                        std::map<unsigned, vector<unsigned> >& rv_ss,
                        bool donor,
                        unsigned min_exon_length = 1) const;
  void get_splice_sites(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_ss,
			std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_ss,
			bool donor, //acceptor if donor = false
			unsigned min_exon_length = 1) const;
  void get_splice_sites(vector<unsigned>& ss, bool donor, unsigned contig_id, unsigned gene, unsigned min_exon_length = 1) const;
  unsigned getTss(unsigned contig_id, unsigned gene) const;
  unsigned getTes(unsigned contig_id, unsigned gene) const;
  unsigned getStartCodonPos(unsigned contig_id, unsigned gene) const;
  bool is_gene_on_rv(unsigned contig_id, unsigned gene) const;
  void get_polyAs(std::map<unsigned, vector<unsigned> >& fw_polyas,
                  std::map<unsigned, vector<unsigned> >& rv_polyas) const;
  void getGenes(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_genes,
                std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_genes,
                std::map<unsigned, vector<string> >& fw_gene_names,
                std::map<unsigned, vector<string> >& rv_gene_names) const;
  void getGenes(std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
		std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes) const;
  string getGeneName(unsigned contig_id, unsigned gene) const;
  const std::map<unsigned, string>& getContigNames() const;
  const std::map<string, unsigned>& getContigIds() const;
  void writeBed() const;
    
  static bool gene_interval_order(const std::pair<std::pair<unsigned,unsigned>,unsigned>& a, const std::pair<std::pair<unsigned,unsigned>,unsigned>& b);
};

#endif /* GTFREADER_h */
