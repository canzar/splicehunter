/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#ifndef NOVELTRANS_H
#define NOVELTRANS_H

#include <string>
#include <vector>

#include "PacBioBamReader.h"
#include "GtfReader.h"

using std::vector;
using std::string;
using std::pair;

class NovelTrans {

public:
    
    typedef std::vector<std::pair<unsigned,unsigned> > intronchain_t;
    typedef std::pair<intronchain_t, vector<unsigned>* > intronchain_cnt_t;
    typedef std::set<intronchain_cnt_t, bool (*)(const intronchain_cnt_t&, const intronchain_cnt_t&)> isoforms_t;
    typedef std::pair<intronchain_t, vector<vector<string> >* > intronchain_reads_t;
    typedef std::set<intronchain_reads_t, bool (*)(const intronchain_reads_t&, const intronchain_reads_t&)> isoform_reads_t;
    typedef std::set<pair<intronchain_cnt_t, std::set<unsigned> >, bool (*)(const pair<intronchain_cnt_t, std::set<unsigned> >&, const pair<intronchain_cnt_t, std::set<unsigned> >&)> readthroughs_t;

    struct iso_features {
        string gene_name;
        string chr;
        bool gene_strand;
        string iso_id;
        bool iso_strand;
        vector<unsigned>* nof_sup;
        intronchain_t exons;
        vector<string> annot;
        vector<unsigned> nof_annot;
        vector<vector<string> >* sup_reads;
    };
    
private:

    std::map<unsigned, vector<unsigned> > ref_stop_codons; //ctg, gene -> stop codon implied by annotated start codon
    
    char gencode(string dna) const;
    string translate(string dna) const;

    bool build_mrna_fw(intronchain_t virt_iso_exons, const NamedDnaSequence& ref_sequence, unsigned start_codon, string& mrna) const;
    bool build_mrna_rv(intronchain_t virt_iso_exons, const NamedDnaSequence& ref_sequence, unsigned start_codon, string& mrna) const;

    void get_ic_exons(const intronchain_t& ic, intronchain_t& exon_chain) const;

    static bool pair_pair_order(const pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> >& i,
                                const pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> >& j);
    void mergeIntervals(vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >& input_intervals,
                        vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > >& merged_intervals, int nof_bams) const;
    
    static bool intronchain_cnt_cmp(const intronchain_cnt_t&, const intronchain_cnt_t&);
    static bool intronchain_reads_cmp(const intronchain_reads_t&, const intronchain_reads_t&);

    static bool readthrough_cmp(const pair<intronchain_cnt_t, std::set<unsigned> >&, const pair<intronchain_cnt_t, std::set<unsigned> >&);

    bool ic_equal(const intronchain_cnt_t&a, const intronchain_cnt_t& b) const;

    GtfReader gtf_reader;

    const vector<string>* batch;
    const vector<string>* sample;

    vector<PacBioBamReader*> bam_readers;

    bool is_strictly_contained(const vector<std::pair<unsigned,unsigned> >& reference,
                               const std::pair<unsigned,unsigned> query,
                               unsigned& certificate_idx) const;
    void contains_strictly(const vector<std::pair<unsigned,unsigned> >& introns,
                           const std::pair<unsigned,unsigned> exon,
                           vector<unsigned>& certificates) const;
    
    bool read_dir_recovery_attempt;
    
    std::unordered_map<string,NamedDnaSequence*>* reference_sequences;
    
    string getComplBase(char base) const;
    string getRevCompl(string dna) const;
    string get_dimer(const std::pair<unsigned, unsigned>& intron, const NamedDnaSequence& ref_sequence, bool on_reverse) const;
    string get_hexamers(const std::pair<unsigned, unsigned>& intron, const NamedDnaSequence& ref_sequence, bool on_reverse) const;
    bool is_canonical(const std::pair<unsigned, unsigned>&, const NamedDnaSequence& ref_sequence, bool on_reverse) const;
    void filter_inv_by_dimers(const vector<std::pair<unsigned,unsigned> >& introns,
                              vector<std::pair<unsigned,unsigned> >& introns_filtered,
                              const NamedDnaSequence& ref_sequence, bool on_reverse) const;
    void filter_by_dimers(const vector<std::pair<unsigned,unsigned> >& introns,
                          vector<std::pair<unsigned,unsigned> >& introns_filtered,
                          const NamedDnaSequence& ref_sequence, bool on_reverse) const;
    std::pair<string, string> get_intron_dimers(const std::string& ref_name,
                                                const std::pair<unsigned,unsigned> intron) const;

    void count_pw_introns(const vector<std::pair<unsigned, unsigned> >& gene_introns, const pair<unsigned,unsigned>& premRNA, unsigned tot_sup,
                          const std::set<unsigned>& ri,
                          vector<unsigned>& cnt_retained,  //total number of reads retained intron (independent of second intron)
                          vector<unsigned>& cnt_spliced,   //total number of reads splicing intron (independent of second intron)
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_retained, //intron i retained and j spliced
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_spliced,  //intron i spliced and j retained
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_retained, //both introns i and j retained
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_spliced  //both introns i and j spliced
                          ) const;

    void count_pw_intinex(const vector<std::pair<unsigned, unsigned> >& gene_exons, vector<std::pair<unsigned,unsigned> >& read_exons,
                          std::map<unsigned, std::set<pair<unsigned,unsigned> > >& introns_per_exon, unsigned tot_sup,
                          const std::set<unsigned>& ie,
                          vector<unsigned>& cnt_retained, //total number of reads spanning exon (independent of second exon)
                          vector<unsigned>& cnt_spliced, //total number of reads containing novel intron (independent of second exon)
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_retained, //exon i spanned and exon j contains novel intron
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_spliced, //exon i contains novel intron and exon j spanned
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_retained, //both exons i and j spanned
                          std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_spliced //both exons i and j contain novel intron
                          ) const;

    void count_pw_combi(const vector<std::pair<unsigned, unsigned> >& gene_exons, const vector<std::pair<unsigned, unsigned> >& gene_introns,
                        vector<std::pair<unsigned,unsigned> >& read_exons, std::map<unsigned, std::set<pair<unsigned,unsigned> > >& introns_per_exon,
                        unsigned tot_sup,
                        const std::set<unsigned>& ie,
                        const std::set<unsigned>& ri,
                        std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_retained_comb,
                        std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_spliced_comb,
                        std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_novel_comb,
                        std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_annot_comb
                        ) const;

    typedef std::pair<unsigned, std::map<std::pair<unsigned,unsigned>, unsigned>::iterator> interval_locator_t;
    typedef std::pair<unsigned, std::map<unsigned, unsigned>::iterator> ss_locator_t;
    static bool interval_order(const std::pair<unsigned, interval_locator_t> i, const std::pair<unsigned, interval_locator_t> j);
    static bool ss_order(const std::pair<unsigned, ss_locator_t> i, const std::pair<unsigned, ss_locator_t> j);
    
    bool get_query_ref_from_strand(const std::pair<unsigned,unsigned>& query,
                                   std::pair<unsigned,unsigned>& strand_unspec_query,
                                   const std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_ref,
                                   const std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_ref,
                                   unsigned ctg,
                                   const vector<std::pair<unsigned,unsigned> >*& reference) const;
    
    void sort_by_occ(std::map<unsigned, std::map<std::pair<unsigned,unsigned>, unsigned> >& novel_intervals) const;
    void sort_by_occ(std::map<unsigned, std::map<unsigned, unsigned> >& novel_ss, std::string ss_type, bool fw_strand) const;       

    int get_max_overlap(const vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >& reference, 
			const std::pair<unsigned,unsigned>& query,			
			vector<vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator>& max_overlap_genes) const;

    int resolve_ambiguity_by_overlap(std::set<unsigned>& ambig_genes,
                                     unsigned contig_id,
                                     std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& ctg_genes,
                                     std::pair<unsigned,unsigned>& alg_range,
                                     vector<std::pair<unsigned,unsigned> >& read_exons) const;

    int resolve_overlap_ambiguity(vector<std::pair<unsigned,unsigned> >& overlap_genes) const;

    int assign_gene_by_intron(vector<std::pair<unsigned,unsigned> >& read_introns, 
			      std::pair<unsigned,unsigned>& alg_range, 
                  vector<std::pair<unsigned,unsigned> >& read_exons,
			      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
			      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes,
			      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_introns,
			      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_introns,
			      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_donor,
			      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_donor,
			      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_acc,
			      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_acc,
                  unsigned contig_id,
                  std::set<unsigned>& multi_match_genes) const;


    void get_all_overlap(const vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >& reference, 
             const std::pair<unsigned,unsigned>& query,
			 vector<std::pair<unsigned,unsigned> >& overlap_genes) const;


    void get_all_overlap(unsigned contig_id,
                         std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& ctg_references,
                         const std::pair<unsigned,unsigned>& query,
                         const vector<pair<unsigned,unsigned> >& query_intervals, //e.g. read exons
                         vector<std::pair<unsigned,unsigned> >& overlap_genes) const;

    bool is_exon_novel(const vector<std::pair<unsigned, unsigned> >& reference, const std::pair<unsigned, unsigned>& query) const;

    bool get_max_hit_gene(std::set<unsigned>& intron_match_genes, std::map<unsigned, unsigned>& nof_occ, unsigned& best_gene) const;

    int assign_gene_by_overlap(std::pair<unsigned,unsigned>& alg_range, 
                               vector<std::pair<unsigned,unsigned> >& read_exons,
                               std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
                               std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes,
                               unsigned contig_id,
                               bool orig_fw_strand) const;
    
    unsigned get_exon_skippings(const vector<std::pair<unsigned,unsigned> >& read_introns,
                                const vector<std::pair<unsigned,unsigned> >& gene_exons,
                                const vector<std::pair<unsigned,unsigned> >& gene_introns,
                                vector<vector<unsigned> >& skipped_exons) const;
    
    unsigned get_intron_retentions(vector<std::pair<unsigned,unsigned> >& read_exons,
                               vector<std::pair<unsigned,unsigned> >& gene_exons,
                               vector<std::pair<unsigned,unsigned> >& gene_introns,
                               vector<vector<unsigned> >& retained_introns) const;
    
    unsigned get_introns_in_exons(const vector<std::pair<unsigned,unsigned> >& read_introns,
                              const vector<std::pair<unsigned,unsigned> >& gene_exons,
                              const vector<std::pair<unsigned,unsigned> >& gene_introns,
                              std::map<unsigned, std::set<std::pair<unsigned,unsigned> > >& intinex) const;

    void get_novel_introns(const vector<std::pair<unsigned,unsigned> >& read_introns,
                           const vector<unsigned>* nof_sup,
                           const vector<std::pair<unsigned,unsigned> >& gene_introns,
                           const vector<std::pair<unsigned,unsigned> >& gene_exons,
                           const vector<unsigned>& gene_donors,
                           const vector<unsigned>& gene_acc,
                           unsigned gene_tss,
                           unsigned gene_tes,
                           bool gene_on_rv,
                           //std::multiset<pair<unsigned,unsigned> >* novel_introns,
                           //std::multiset<pair<unsigned,unsigned> >* anno_introns,
                           std::set<pair<unsigned,unsigned> >* novel_introns,
                           std::set<pair<unsigned,unsigned> >* anno_introns,
                           vector<unsigned>& found) const;

    unsigned get_novel_exons(const vector<std::pair<unsigned,unsigned> >& read_exons,
                             const vector<std::pair<unsigned,unsigned> >& gene_exons,
                             const vector<std::pair<unsigned,unsigned> >& gene_introns,
                             bool inclusion = false) const;

    int get_sub_overlap(const vector<pair<unsigned,unsigned> >& query_intervals,
                                    const vector<pair<unsigned,unsigned> >& reference_intervals) const;
    
    static bool anno_ss_order(const pair<unsigned, pair<bool,bool> >& anno_ss_1, const pair<unsigned, pair<bool,bool> >& anno_ss_2);
    string compare_protseq(const intronchain_t& exon_chain_ref, const intronchain_t& exon_chain_novel, unsigned start_codon,
                           unsigned novel_stop_codon, unsigned ref_stop_codon, bool reverse) const;
    void print_uniq_protseq(string chr, unsigned ctg, unsigned gene, unsigned start_codon, isoforms_t& novel_ic, bool novel,
                            bool single_exon_iso, const NamedDnaSequence& ref_sequence, unsigned& prot_nr, std::ofstream& protseq_file, std::ofstream& as_rna_file,
                            std::set<unsigned>& broken_start_codon, std::set<unsigned>& no_end_codon, std::map<unsigned, string>& protseq_diff) const;
    void print_max_orf(string chr, isoforms_t& novel_ic, bool single_exon_iso, bool gene_on_rv, const NamedDnaSequence& ref_sequence, unsigned& prot_nr,
                       std::ofstream& protseq_file, std::ofstream& novel_rna_file, std::set<unsigned>& no_orf, unsigned single_exon_nr = 0) const;
    int get_max_orf(string chr, intronchain_t& novel_ic, bool gene_on_rv, const NamedDnaSequence& ref_sequence) const;
                       
    unsigned getStopCodonPos(intronchain_t exon_chain, unsigned prot_length, unsigned start_codon) const;
    unsigned getStopCodonPosRev(intronchain_t exon_chain, unsigned prot_length, unsigned start_codon) const;

public:

    NovelTrans(const string& gtf, const vector<string>& bams, const vector<string>* batch, const vector<string>* sample, const string& fasta = "");
    ~NovelTrans();

    void print_iso(iso_features& isof, std::ostream& out, std::ofstream* cert_file = NULL) const;
    void print_header(std::ostream& out) const;
    
    void reads2isoforms(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& isoforms_per_gene,
                        std::map<unsigned, std::map<unsigned, std::pair<isoforms_t,isoforms_t> > >& intret_chain_per_gene,
                        bool flonly,
                        std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* isoform_reads_per_gene = NULL) const;
    void cluster_by_ic(const isoforms_t& isoforms, vector<vector<intronchain_cnt_t> >& iso_clusters,
                       const isoform_reads_t* isoform_reads = NULL, vector<vector<intronchain_reads_t> >* isoread_clusters = NULL) const;
    void cluster_by_ic(const readthroughs_t& isoforms, vector<vector<pair<intronchain_cnt_t, std::set<unsigned> > > >& iso_clusters) const;
    void mergeTssTes(const readthroughs_t& isoforms, readthroughs_t& merged_isoforms) const;
    void mergeTssTes(const isoforms_t& isoforms, isoforms_t& merged_isoforms,
                     const isoform_reads_t* isoform_reads = NULL, isoform_reads_t* merged_isoform_reads = NULL) const;
    void mergeTssTes(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& isoforms_per_gene,
                     std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& merged_isoforms_per_gene,
                     int snap_dist, int window_size,
                     std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* isoform_reads_per_gene = NULL,
                     std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* merged_isoform_reads_per_gene = NULL) const;
    void all_events_per_isoform(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& isoforms_per_gene,
                                std::map<unsigned, std::map<unsigned, std::pair<isoforms_t,isoforms_t> > >& intret_chain_per_gene,
                                std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t, isoform_reads_t> > >* isoform_reads_per_gene = NULL) const;
       
    void get_novel_exons() const; //what is not covered by novel introns?
    void get_novel_polyAs() const;
    

};

#endif // NOVELTRANS_H
