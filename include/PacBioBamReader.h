/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PACBIOBAMREADER_H
#define PACBIOBAMREADER_H

#include <unordered_map>
#include <api/BamReader.h>

#include "NamedDnaSequence.h"

class PacBioBamReader {
 private:
  BamTools::BamReader bam_reader;
  bool finished;
  BamTools::BamAlignment next_read_aln;
  long long counter;
  bool unmapped;  
  
  char getComplBase(char base) const;
  
 public:
  PacBioBamReader(const std::string& filename);

  bool hasNext() const;

  void advance();

  const BamTools::BamAlignment* getAlignment() const;

  bool isUnmapped() const;
  bool isReverseStrand() const;
  bool isMultiMap() const;
  bool isUniqMap() const;
  bool isTranslocMap() const;

  size_t getNofAlnBases() const;
  size_t sizeOfHardClip() const;
  size_t getReadLength() const;
  size_t getNofIdentAlnBases(const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences) const;
  void getIntrons(std::vector<std::pair<unsigned, unsigned> >& introns) const;
  bool getGoodIntrons(std::vector<std::pair<unsigned, unsigned> >& introns,
                      const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences,
                      bool is_forward_read,
                      int window_size,
                      int snap_window_size,
                      std::vector<std::pair<unsigned, unsigned> >& gene_introns) const;
  size_t getExons(std::vector<std::pair<unsigned, unsigned> >& exons) const;
  size_t getLastAlgRefBase() const;
  size_t getFirstAlgRefBase() const;
  void getDimers(const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences, std::vector<std::pair<string, string> >& dimers, bool is_forward_read) const;
  int countIndelFlanks(size_t ss, size_t window_size) const;

  const BamTools::RefVector& getReferenceData() const { return bam_reader.GetReferenceData(); }  
  BamTools::SamHeader getHeader() const { return bam_reader.GetHeader(); }

  const std::string& getReadName() const { return next_read_aln.Name; }
  const std::string& getRefName() const;
    
  void rewind();
};

#endif /* PACBIOBAMREADER_H */
