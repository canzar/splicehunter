/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#ifndef PARAMETERS_H
#define PARAMETERS_H

#include <string>

using std::string;

namespace Parameters {

extern float min_overlap_diff_strand;
extern float min_overlap_same_strand;

extern float ambig_res_thr;

extern unsigned ss_window;
extern unsigned ss_snap_window;
    
extern unsigned snap_dist;
extern unsigned tsstes_window;
    
extern float mapqual;
extern float maplength;

extern string protseq_file;
extern string novel_rna_file;
extern string as_rna_file;
extern string hexamer_file;
extern string dimer_file;
extern string retint_length_file;
extern string pwcount_file;
extern string nc_anno_file;
extern string anno_intron_file;
extern string cert_file;
extern string iso_file;
}

#endif // PARAMETERS_H
