#!/bin/bash

DIR="/Users/scanzar/ttic/spombe_data/align_v2_29/batch1/"
SAMPLES="b1_1,b1_2,b1_3,b1_4,b1_5,b1_6"
SAMTOOLS="/Users/scanzar/sw/samtools-1.1/samtools"

colors="0,0,255;255,0,0;0.255,0;0,0,0;255,255,0;153,51,255"

line=`cat -`

IFS=':' read -ra s <<< "$line"
IFS='-' read -ra tp <<< "${s[1]}"

IFS=',' read -ra sid <<< "$SAMPLES"
IFS=';' read -ra col <<< "$colors"

j=0
for i_newline in "${tp[@]}"; do 
	i=$(echo -n $i_newline | tr -d '\n')
	if [ -n "$i" ]; then 
		query=`echo $i | sed s'/\,/\\\|/g'`
		rep=`echo ${sid[$j]} | sed s'/_.*//'`
		cond=`echo ${sid[$j]} | sed s'/.*_//'`
		${SAMTOOLS} view -H ${DIR}${rep}_*_${cond}*.bam > i$j.sam
		color="${col[j]}"
		${SAMTOOLS} view ${DIR}${rep}_*_${cond}*.bam | grep --line-buffered $query | awk -v var=$color '{print $0"\tYC:Z:"var}' >> i$j.sam
		${SAMTOOLS} view -b i$j.sam > i$j.bam
		${SAMTOOLS} sort i$j.bam i$j.sorted
		${SAMTOOLS} index i$j.sorted.bam
		rm i$j.sam
		rm i$j.bam
	fi
	j=$(($j+1))
done

