# SpliceHunter  #
### Alternative splicing detection and annotation from long read (time course) sequencing data ###

## Installing SpliceHunter ##

Before building SpliceHunter, make sure that the following libraries are installed on your system:

* [SeqAn](https://github.com/seqan/seqan) 
* Boost
* zlib

On a Linux system, the latter two are most likely available through your package manager or are even pre-installed. To simplify the installation
process, the [BamTools](https://github.com/pezmaster31/bamtools) library has been bundled with Splicehunter and is automatically compiled and 
installed through the build system. After cloning SpliceHunter, change to the newly created directory (default splicehunter) and use [CMAKE](http://www.cmake.org) to build SpliceHunter.

```
cd splicehunter
mkdir build
cd build
cmake -DSEQAN_INCLUDE_PATH=<YOUR_SEQAN_INCLUDE_DIR> ..
make
```

This will create binary SpliceHunter in directory `splicehunter/build/bin`.

## Running SpliceHunter ##

The following detailed description can also be obtained by typing 

```
SpliceHunter --help
```

### Usage ###

```
SpliceHunter [options] -g <anno> -f <ref> [-m <dir>]
```

### Main arguments ###

`-g <anno>`
:     The annotation in `gtf` format which is used as reference when defining alternative splicing events. 

`-f <ref>`
:     The reference sequence in `fasta` format

`-m <dir>`
:     Path to the directory containing aligned reads in `bam` format.


### Options ###

`-e  <int>`
:      error-free splice site window

`-w  <int>`
:      snap erroneous novel splice site to annotated splice
                         site within window

`-s  <int>`
:      snap read start/end sites to annotated TSS/TES if
                         within snap distance
`-t  <int>`
:      snap read start/end sites to leftmost/rightmost
                         start/end within window
`-u  <float>`
:      ambiguity resolving threshold

`-d  <int>`
:    min overlap on different strand

  `-o  <int>`
:    min overlap on same strand

`-l`
:    consider only FL reads

`-c`
:    output certificates

`-a  <float>`
: minimum required fraction of aligned bases

`-q  <float>`
:   minimum required fraction of identically aligned bases

`-p  <file_name>`
:  output protein sequence to file

`-r  <file_name>`
:   output novel rna sequence to file

`-A  <file_name>`
:    output AS rna sequence to file

`-x  <file_name>`
:   output intron hexamers to file

`-i  <file_name>`
:     output intron dimers to file

`-n  <file_name>`
: output retained intron length to file

`-j  <file_name>`
:   output pairwise splicing event counts to file

  `-y  <file_name>`
: output longest ORFs of all annotated genes to file

