/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include "GtfReader.h"

#include <fstream>
#include <stdexcept>

#include <seqan/store.h>

using std::vector;

GtfReader::GtfReader(const string& filename)
{
  typedef seqan::FragmentStore<> TFragmentStore;
  typedef TFragmentStore::TAnnotationStore TAnnotationStore;
  typedef seqan::Iterator<TFragmentStore, seqan::AnnotationTree<> >::Type TAnnoTreeIter;
  typedef seqan::Value<TAnnoTreeIter>::Type Tid;
  typedef seqan::Value<TAnnotationStore>::Type TAnnotation;

  TFragmentStore store;
  seqan::GffFileIn inFile;
  if (!open(inFile, filename.c_str()))
  {
      std::cerr << "could not open file " << filename << std::endl;
      exit(1);
  } 
  readRecords(store, inFile);
  //std::ifstream annotation_stream(filename.c_str(), std::ios::binary);
  //if (annotation_stream.fail()) {
  //    throw std::runtime_error("Error opening file \"" + filename + "\".");
  //}
  //read(annotation_stream, store, seqan::Gtf());
  //annotation_stream.close();   

  unsigned exonType = 0;
  //unsigned subExonType = 0;
  unsigned geneType = 0;
  unsigned transcriptType = 0;

  seqan::_storeAppendType(store, exonType, "exon");
  //seqan::_storeAppendType(store, subExonType, "subexon");
  seqan::_storeAppendType(store, geneType, "gene");
  seqan::_storeAppendType(store, transcriptType, "mRNA");

  size_t tId = 0;

  seqan::CharString geneIdStr;
  seqan::CharString transIdStr;
  //seqan::CharString exonNrStr;
  bool newTrans = false;
  bool newGene = false;

  Transcript* activeTransPtr = NULL;

  TAnnoTreeIter dfsIt = seqan::begin(store, seqan::AnnotationTree<>());

  while (!atEnd(dfsIt))
  {
    TAnnotation& anno = getAnnotation(dfsIt);
    unsigned contigId = anno.contigId;

    if(anno.typeId == geneType)
    {
      geneIdStr = seqan::getName(dfsIt);
      //std::cerr << "gene: " << geneIdStr << std::endl;
      newGene = true;
    }

    if(anno.typeId == transcriptType)
    {
      transIdStr = seqan::getName(dfsIt);      
      //std::cerr << "transcript: " << transIdStr << std::endl;
      newTrans = true;
    }

    if(anno.typeId == exonType)
    {
      bool strand = anno.beginPos < anno.endPos; 
      //std::cerr << store.contigNameStore[contigId] << std::endl;
      contigId2Name[contigId] = seqan::toCString(store.contigNameStore[contigId]);
      contigName2Id[seqan::toCString(store.contigNameStore[contigId])] = contigId;

      //seqan::annotationGetValueByKey(store, anno, "exon_number", exonNrStr);
      //std::cerr << "exon: " << exonNrStr << std::endl;

      //size_t exonNr;
      //std::stringstream ss(seqan::toCString(exonNrStr));
      //ss >> exonNr; 

      if(newGene)
      {
	newGene = false;
    knownTranscripts[contigId].push_back(vector<Transcript>());
    geneNames[contigId].push_back(seqan::toCString(geneIdStr));
      }

      if(newTrans)
      {
	newTrans = false;

	Transcript t;
	t.id = tId;
	t.name = seqan::toCString(transIdStr);
    t.start_codon = -1u;

	transId2Name[tId] = seqan::toCString(transIdStr);
	++tId;	

        knownTranscripts[contigId].back().push_back(t);
        activeTransPtr = &knownTranscripts[contigId].back().back();
      }
      
      if(activeTransPtr != NULL)      
	activeTransPtr->exons.push_back(std::make_pair(anno.beginPos,anno.endPos));
    }

    if (getType(dfsIt)=="start_codon")
    {
        //bool strand = anno.beginPos < anno.endPos;
        unsigned old_start_codon = knownTranscripts[contigId].back().back().start_codon; //if start codon interrupted by intron
        //if(old_start_codon != -1u)
        //    std::cout << old_start_codon << std::endl;
        bool strand = knownTranscripts[contigId].back().back().exons[0].first < knownTranscripts[contigId].back().back().exons[0].second;
        if(strand)
        {
            unsigned new_start_codon = anno.beginPos;
            if(old_start_codon == -1u)
                knownTranscripts[contigId].back().back().start_codon = new_start_codon;
            else {
                knownTranscripts[contigId].back().back().start_codon = std::min(new_start_codon, old_start_codon);
            }
        }
        else {
            unsigned new_start_codon = anno.beginPos-1;
            if(old_start_codon == -1u)
                knownTranscripts[contigId].back().back().start_codon = new_start_codon; //not right boundary of half open interval but singleton
            else {
                knownTranscripts[contigId].back().back().start_codon = std::max(new_start_codon, old_start_codon);
            }
        }
        //std::cerr <<  knownTranscripts[contigId].back().back().start_codon << std::endl;
    }
    ++dfsIt;
  }
}

const std::map<unsigned, vector<vector<GtfReader::Transcript> > >& GtfReader::getKnownTranscripts()
{
  return knownTranscripts;
}

void GtfReader::writeBed() const
{  
  std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
  for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
  {
    for(size_t i=0; i<it->second.size(); i++)
    {         
      for(size_t j=0; j<it->second[i].size(); j++)
      {
	for(size_t k=0; k<it->second[i][j].exons.size(); k++)
	  if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second)	    
	    std::cout << contigId2Name.at(it->first) << "\t" << it->second[i][j].exons[k].first << "\t" << it->second[i][j].exons[k].second << std::endl;
	  else
	    std::cout << contigId2Name.at(it->first) << "\t" << it->second[i][j].exons[k].second << "\t" << it->second[i][j].exons[k].first << std::endl;
      }
    }
  }
}

const std::map<unsigned, string>& GtfReader::getContigNames() const
{
  return contigId2Name;
}

const std::map<string, unsigned>& GtfReader::getContigIds() const
{
  return contigName2Id;
}

void GtfReader::get_polyAs(std::map<unsigned, vector<unsigned> >& fw_polyas,
                           std::map<unsigned, vector<unsigned> >& rv_polyas) const
{
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //std::cerr << "new locus: " << std::endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //std::cerr << "number of exons: " << it->second[i][j].exons.size() << std::endl;
                if(it->second[i][j].exons.back().first < it->second[i][j].exons.back().second) //forward strand
                {
		  //std::cerr << "forward: " << it->first <<  " " << it->second[i][j].exons.back().second << std::endl;		 
                    fw_polyas[it->first].push_back(it->second[i][j].exons.back().second);
                } else //reverse strand
                {
		  //std::cerr << "reverse: " << it->first <<  " " << it->second[i][j].exons.back().second-1 << std::endl;		 
                    rv_polyas[it->first].push_back(it->second[i][j].exons.back().second-1);
                }
            }
        }
    }
}

//get all annotated donor or acceptor sites (specified by boolean donor argument) in increasing order
void GtfReader::get_splice_sites(std::map<unsigned, vector<unsigned> >& fw_ss,
                std::map<unsigned, vector<unsigned> >& rv_ss,
                bool donor, //acceptor if donor = false
                unsigned min_exon_length) const
{
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //std::cerr << "new locus: " << std::endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //std::cerr << it->first << " number of exons: " << it->second[i][j].exons.size() << std::endl;
                for(size_t k=0; k<it->second[i][j].exons.size(); k++)
                {
                    //std::cerr << "exon: " << it->second[i][j].exons[k].first << " " << it->second[i][j].exons[k].second << std::endl;
                    if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second) //forward strand
                    {
                        if(it->second[i][j].exons[k].second - it->second[i][j].exons[k].first >= min_exon_length) //is exon large enough
                        {
                            if (donor && k < it->second[i][j].exons.size()-1) { //no donor for last exon                                
                                fw_ss[it->first].push_back(it->second[i][j].exons[k].second);
                                //std::cerr << " fw donor: " << it->second[i][j].exons[k].second << std::endl;
                            }
                            if (!donor && k > 0) { //no acceptor for first exon
                                fw_ss[it->first].push_back(it->second[i][j].exons[k].first-1);
                                //std::cerr << " fw acceptor: " << it->second[i][j].exons[k].first-1 << std::endl;
                            }
                        }
                    } else //reverse strand
                    {
                        if(it->second[i][j].exons[k].first - it->second[i][j].exons[k].second >= min_exon_length) //is exon large enough
                        {
                            if (donor && k < it->second[i][j].exons.size()-1)  {//no donor for last exons - reverse order of exons on reverse strand!
                                rv_ss[it->first].push_back(it->second[i][j].exons[k].second-1);
                                //std::cerr << "rv donor: " << it->second[i][j].exons[k].second-1 << std::endl;
                            }
                            if (!donor && k >0) {
                                rv_ss[it->first].push_back(it->second[i][j].exons[k].first);
                                //std::cerr << "rv acceptor: " << it->second[i][j].exons[k].first << std::endl;
                            }
                        }
                    }
                }
            }
        }
    }
    //sort splice sites sites in increasing order
    std::map<unsigned, vector<unsigned> >::iterator it_ss;
    for(it_ss=fw_ss.begin(); it_ss!=fw_ss.end(); ++it_ss)
    {
        std::sort(it_ss->second.begin(), it_ss->second.end());
    }
    for(it_ss=rv_ss.begin(); it_ss!=rv_ss.end(); ++it_ss)
    {
        std::sort(it_ss->second.begin(), it_ss->second.end());
    }
}


//get all annotated donor or acceptor sites (specified by boolean donor argument) in increasing order
void GtfReader::get_splice_sites(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_ss,
				 std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_ss,
				 bool donor, //acceptor if donor = false
				 unsigned min_exon_length) const
{
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //std::cerr << "new locus: " << std::endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //std::cerr << it->first << " number of exons: " << it->second[i][j].exons.size() << std::endl;
                for(size_t k=0; k<it->second[i][j].exons.size(); k++)
                {
                    //std::cerr << "exon: " << it->second[i][j].exons[k].first << " " << it->second[i][j].exons[k].second << std::endl;
                    if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second) //forward strand
                    {
                        if(it->second[i][j].exons[k].second - it->second[i][j].exons[k].first >= min_exon_length) //is exon large enough
                        {
                            if (donor && k < it->second[i][j].exons.size()-1) { //no donor for last exon                                
			      fw_ss[it->first].push_back(std::make_pair(it->second[i][j].exons[k].second, i));
			      //std::cerr << " fw donor: " << it->second[i][j].exons[k].second << std::endl;
                            }
                            if (!donor && k > 0) { //no acceptor for first exon
			      fw_ss[it->first].push_back(std::make_pair(it->second[i][j].exons[k].first-1, i));
                                //std::cerr << " fw acceptor: " << it->second[i][j].exons[k].first-1 << std::endl;
                            }
                        }
                    } else //reverse strand
                    {
                        if(it->second[i][j].exons[k].first - it->second[i][j].exons[k].second >= min_exon_length) //is exon large enough
                        {
                            if (donor && k < it->second[i][j].exons.size()-1)  {//no donor for last exons - reverse order of exons on reverse strand!
			      rv_ss[it->first].push_back(std::make_pair(it->second[i][j].exons[k].second-1,i));
			      //std::cerr << "rv donor: " << it->second[i][j].exons[k].second-1 << std::endl;
                            }
                            if (!donor && k >0) {
			      rv_ss[it->first].push_back(std::make_pair(it->second[i][j].exons[k].first,i));
			      //std::cerr << "rv acceptor: " << it->second[i][j].exons[k].first << std::endl;
                            }
                        }
                    }
                }
            }
        }
    }
    //sort splice sites sites in increasing order
    //second coordinate (gene locus) not crucial for sorting.
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > >::iterator it_ss;
    for(it_ss=fw_ss.begin(); it_ss!=fw_ss.end(); ++it_ss)
    {
        std::sort(it_ss->second.begin(), it_ss->second.end());
    }
    for(it_ss=rv_ss.begin(); it_ss!=rv_ss.end(); ++it_ss)
    {
        std::sort(it_ss->second.begin(), it_ss->second.end());
    }
}

void GtfReader::get_splice_sites(vector<unsigned>& ss, bool donor, unsigned contig_id, unsigned gene, unsigned min_exon_length) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end());
    assert(gene < knownTranscripts.at(contig_id).size());
    
    bool fw = false;
    bool rv = false;
    for(size_t j=0; j<knownTranscripts.at(contig_id).at(gene).size(); j++)
    {
        //std::cerr << "new transcript: " << knownTranscripts.at(contig_id).at(gene)[j].name << std::endl;
        for(size_t k=0; k<knownTranscripts.at(contig_id).at(gene)[j].exons.size(); k++)
        {
            //std::cerr << "exon: " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first << " " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].second << std::endl;
            
            if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first < knownTranscripts.at(contig_id).at(gene)[j].exons[k].second) //forward strand
            {
                if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].second - knownTranscripts.at(contig_id).at(gene)[j].exons[k].first >= min_exon_length) //is exon large enough
                {
                    if (donor && k < knownTranscripts.at(contig_id).at(gene)[j].exons.size()-1) //no donor for last exon
                    {
                        fw = true;
                        ss.push_back(knownTranscripts.at(contig_id).at(gene)[j].exons[k].second);
                        //std::cerr << " fw donor: " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].second << std::endl;
                    }
                    if (!donor && k > 0) //no acceptor for first exon
                    {
                        fw = true;
                        ss.push_back(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first-1);
                        //std::cerr << " fw acceptor: " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first-1 << std::endl;
                    }
                }
            } else //reverse strand
            {
                if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first - knownTranscripts.at(contig_id).at(gene)[j].exons[k].second >= min_exon_length) //is exon large enough
                {
                    if (donor && k < knownTranscripts.at(contig_id).at(gene)[j].exons.size()-1) //no donor for last exons - reverse order of exons on reverse strand!
                    {
                        rv = true;
                        ss.push_back(knownTranscripts.at(contig_id).at(gene)[j].exons[k].second-1);
                        //std::cerr << "rv donor: " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].second-1 << std::endl;
                    }
                    if (!donor && k >0)
                    {
                        rv = true;
                        ss.push_back(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first);
                        //std::cerr << "rv acceptor: " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first << std::endl;
                    }
                }
            }
        }
    }
    assert(!fw || !rv); //gene cannot be both forward and reverse
   
    //sort splice sites in increasing order
    std::sort(ss.begin(), ss.end());
}



//get all exons of given gene in increasing order
void GtfReader::get_exons(vector<std::pair<unsigned,unsigned> >& gene_exons,
                          unsigned contig_id,
                          unsigned gene,
                          unsigned min_exon_length) const
{
  assert(knownTranscripts.find(contig_id) != knownTranscripts.end());
  assert(gene < knownTranscripts.at(contig_id).size());
  for(size_t j=0; j<knownTranscripts.at(contig_id).at(gene).size(); j++)
    {
      for(size_t k=0; k<knownTranscripts.at(contig_id).at(gene)[j].exons.size(); k++)
        {
	  if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first < knownTranscripts.at(contig_id).at(gene)[j].exons[k].second) {
	    if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].second - knownTranscripts.at(contig_id).at(gene)[j].exons[k].first >= min_exon_length)
	      gene_exons.push_back(std::make_pair(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first, knownTranscripts.at(contig_id).at(gene)[j].exons[k].second));
	  } else {
	    if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first - knownTranscripts.at(contig_id).at(gene)[j].exons[k].second >= min_exon_length)
	      gene_exons.push_back(std::make_pair(knownTranscripts.at(contig_id).at(gene)[j].exons[k].second, knownTranscripts.at(contig_id).at(gene)[j].exons[k].first));
	  }
        }
    }
  //sort exons in increasing order
  std::sort(gene_exons.begin(), gene_exons.end());   
}



//get all annotated exons in increasing order
void GtfReader::get_exons(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_exons,
                          std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_exons,
                          unsigned min_exon_length) const
{
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //cerr << "new locus: " << endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //cerr << "number of exons: " << it->second[i][j].exons.size() << endl;
                 for(size_t k=0; k<it->second[i][j].exons.size(); k++)
                 {
                     if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second) {
                         if(it->second[i][j].exons[k].second - it->second[i][j].exons[k].first >= min_exon_length)
                             fw_exons[it->first].push_back(std::make_pair(it->second[i][j].exons[k].first, it->second[i][j].exons[k].second));
                     } else {
                         if(it->second[i][j].exons[k].first - it->second[i][j].exons[k].second >= min_exon_length)
                             rv_exons[it->first].push_back(std::make_pair(it->second[i][j].exons[k].second, it->second[i][j].exons[k].first));
                     }
                 }
            }
        }
    }
    //sort exons in increasing order
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > >::iterator it_exs;
    for(it_exs=fw_exons.begin(); it_exs!=fw_exons.end(); ++it_exs)
    {
        std::sort(it_exs->second.begin(), it_exs->second.end());
    }
    for(it_exs=rv_exons.begin(); it_exs!=rv_exons.end(); ++it_exs)
    {
        std::sort(it_exs->second.begin(), it_exs->second.end());
    }
}


bool GtfReader::is_gene_on_rv(unsigned contig_id, unsigned gene) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end());
    assert(gene < knownTranscripts.at(contig_id).size());
    assert(knownTranscripts.at(contig_id).at(gene).size() > 0); //at least one transcript
    assert(knownTranscripts.at(contig_id).at(gene)[0].exons.size() > 0); //at least one exon
    
    if(knownTranscripts.at(contig_id).at(gene)[0].exons[0].first < knownTranscripts.at(contig_id).at(gene)[0].exons[0].second)
        return false;
    else
        return true;
}

string GtfReader::getGeneName(unsigned contig_id, unsigned gene) const
{
    assert(geneNames.find(contig_id) != geneNames.end());
    assert(gene < geneNames.at(contig_id).size());

    return geneNames.at(contig_id).at(gene);
}

//returns leftmost base of only annotated transcript
//if multiple isoform annotated, take leftmost tss
//note: if gene on reverse strand that corresponds to the end site
unsigned GtfReader::getTss(unsigned contig_id, unsigned gene) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end()); //contig exists
    assert(gene < knownTranscripts.at(contig_id).size()); //gene exists
    //assert(knownTranscripts.at(contig_id).at(gene).size()==1); //exactly one transcript annotated
    
    unsigned leftmost_tss;
    if (knownTranscripts.at(contig_id).at(gene)[0].exons[0].first < knownTranscripts.at(contig_id).at(gene)[0].exons[0].second)
    {
        leftmost_tss = knownTranscripts.at(contig_id).at(gene)[0].exons[0].first;
        for(size_t i = 0; i < knownTranscripts.at(contig_id).at(gene).size(); ++i)
            leftmost_tss = std::min(leftmost_tss, knownTranscripts.at(contig_id).at(gene)[i].exons[0].first);
    }
    else {        
        size_t nof_exons = knownTranscripts.at(contig_id).at(gene)[0].exons.size();
        leftmost_tss = knownTranscripts.at(contig_id).at(gene)[0].exons[nof_exons-1].second;
        for(size_t i = 0; i < knownTranscripts.at(contig_id).at(gene).size(); ++i) {
            nof_exons = knownTranscripts.at(contig_id).at(gene)[i].exons.size();
            leftmost_tss = std::min(leftmost_tss, knownTranscripts.at(contig_id).at(gene)[i].exons[nof_exons-1].second);
        }
    }
    return leftmost_tss;
}

//returns start codon position of first transcript of given gene
unsigned GtfReader::getStartCodonPos(unsigned contig_id, unsigned gene) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end()); //contig exists
    assert(gene < knownTranscripts.at(contig_id).size()); //gene exists

    return knownTranscripts.at(contig_id).at(gene)[0].start_codon;
}

//returns rightmost base of only annotated transcript
//if multiple isoforms annotated, take rightmost tes
//note: if gene on reverse strand that corresponds to the start site
unsigned GtfReader::getTes(unsigned contig_id, unsigned gene) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end()); //contig exists
    assert(gene < knownTranscripts.at(contig_id).size()); //gene exists
    //assert(knownTranscripts.at(contig_id).at(gene).size()==1); //exactly one transcript annotated
    
    unsigned rightmost_tes;
    if (knownTranscripts.at(contig_id).at(gene)[0].exons[0].first < knownTranscripts.at(contig_id).at(gene)[0].exons[0].second)
    {
        size_t nof_exons = knownTranscripts.at(contig_id).at(gene)[0].exons.size();
        rightmost_tes = knownTranscripts.at(contig_id).at(gene)[0].exons[nof_exons-1].second-1;
        for(size_t i = 0; i < knownTranscripts.at(contig_id).at(gene).size(); ++i) {
            nof_exons = knownTranscripts.at(contig_id).at(gene)[i].exons.size();
            rightmost_tes = std::max(rightmost_tes, knownTranscripts.at(contig_id).at(gene)[i].exons[nof_exons-1].second-1);
        }
    }
    else {
        rightmost_tes = knownTranscripts.at(contig_id).at(gene)[0].exons[0].first-1;
        for(size_t i = 0; i < knownTranscripts.at(contig_id).at(gene).size(); ++i)
            rightmost_tes = std::max(rightmost_tes, knownTranscripts.at(contig_id).at(gene)[i].exons[0].first-1);
    }
    return rightmost_tes;
}


//get all introns of given gene in increasing order
void GtfReader::get_introns(vector<std::pair<unsigned,unsigned> >& gene_introns, unsigned contig_id, unsigned gene, unsigned min_intron_length) const
{
    assert(knownTranscripts.find(contig_id) != knownTranscripts.end());
    assert(gene < knownTranscripts.at(contig_id).size());
  for(size_t j=0; j<knownTranscripts.at(contig_id).at(gene).size(); j++)
    {      
      unsigned prev_end;
      for(size_t k=0; k<knownTranscripts.at(contig_id).at(gene)[j].exons.size(); k++) {
	//std::cerr << contigId2Name.at(it->first) << " " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first << "-" << knownTranscripts.at(contig_id).at(gene)[j].exons[k].second << std::endl;
	if(k==0)
	  prev_end = knownTranscripts.at(contig_id).at(gene)[j].exons[k].second;
	else {
	  if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first < knownTranscripts.at(contig_id).at(gene)[j].exons[k].second) {
	    assert(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first >= prev_end); //non-empty intron
	    if(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first - prev_end >= min_intron_length)
	      gene_introns.push_back(std::make_pair(prev_end, knownTranscripts.at(contig_id).at(gene)[j].exons[k].first)); //half open intervals
	    //cerr << "pushed back forward intron: " << prev_end << "-" << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first << endl;
	  }
	  else {
	    assert(prev_end >= knownTranscripts.at(contig_id).at(gene)[j].exons[k].first); //non-empty intron
	    if(prev_end - knownTranscripts.at(contig_id).at(gene)[j].exons[k].first >= min_intron_length)
	      gene_introns.push_back(std::make_pair(knownTranscripts.at(contig_id).at(gene)[j].exons[k].first, prev_end));
	    //cerr << "pushed back reverse intron: " << contigId2Name.at(it->first) << " " << knownTranscripts.at(contig_id).at(gene)[j].exons[k].first << "-" << prev_end << endl;
	  }
	  prev_end = knownTranscripts.at(contig_id).at(gene)[j].exons[k].second;
	}
      }
    }
 
  //sort introns in increasing order
  std::sort(gene_introns.begin(), gene_introns.end());
}


//get all annotated introns in increasing order
void GtfReader::get_introns(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_introns, std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_introns, unsigned min_intron_length)  const
{    
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //cerr << "new locus: " << endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //cerr << "number of exons: " << it->second[i][j].exons.size() << endl;
                unsigned prev_end;
                for(size_t k=0; k<it->second[i][j].exons.size(); k++) {
                    //std::cerr << contigId2Name.at(it->first) << " " << it->second[i][j].exons[k].first << "-" << it->second[i][j].exons[k].second << std::endl;
                    if(k==0)
                        prev_end = it->second[i][j].exons[k].second;
                    else {
                        if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second) {
                            assert(it->second[i][j].exons[k].first >= prev_end); //non-empty intron
                            if(it->second[i][j].exons[k].first - prev_end >= min_intron_length)
                                fw_introns[it->first].push_back(std::make_pair(prev_end, it->second[i][j].exons[k].first)); //half open intervals
                            //cerr << "pushed back forward intron: " << prev_end << "-" << it->second[i][j].exons[k].first << endl;
                        }
                        else {
                            assert(prev_end >= it->second[i][j].exons[k].first); //non-empty intron
                            if(prev_end - it->second[i][j].exons[k].first >= min_intron_length)
                                rv_introns[it->first].push_back(std::make_pair(it->second[i][j].exons[k].first, prev_end));
                            //cerr << "pushed back reverse intron: " << contigId2Name.at(it->first) << " " << it->second[i][j].exons[k].first << "-" << prev_end << endl;
                        }
                        prev_end = it->second[i][j].exons[k].second;
                    }
                }
            }
        }
    }
    //sort introns in increasing order
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > >::iterator it_ints;
    for(it_ints=fw_introns.begin(); it_ints!=fw_introns.end(); ++it_ints)
    {
        std::sort(it_ints->second.begin(), it_ints->second.end());
    }
    for(it_ints=rv_introns.begin(); it_ints!=rv_introns.end(); ++it_ints)
    {
        std::sort(it_ints->second.begin(), it_ints->second.end());
    }

    /*
  std::map<unsigned, vector<std::pair<unsigned,unsigned> > >::const_iterator it_ints;
  for(it_ints=rv_introns.begin(); it_ints!=rv_introns.end(); ++it_ints)
  {
    vector<std::pair<unsigned,unsigned> >::const_iterator it_chrints;
    for(it_chrints=it_ints->second.begin(); it_chrints!=it_ints->second.end(); ++it_chrints)
      cerr << contigId2Name.at(it_ints->first) << " " << it_chrints->first << "-" << it_chrints->second << endl;
  }
  */
}

bool GtfReader::gene_interval_order(const std::pair<std::pair<unsigned,unsigned>,unsigned>& a, const std::pair<std::pair<unsigned,unsigned>,unsigned>& b)
{
    return a.first < b.first;
}

//get all annotated introns in increasing order
void GtfReader::get_introns(std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_introns, 
			    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_introns, 
			    unsigned min_intron_length)  const
{
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
    for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
    {
        for(size_t i=0; i<it->second.size(); i++)
        {
            //cerr << "new locus: " << endl;
            for(size_t j=0; j<it->second[i].size(); j++)
            {
                //std::cerr << "new transcript: " << it->second[i][j].name << std::endl;
                //cerr << "number of exons: " << it->second[i][j].exons.size() << endl;
                unsigned prev_end;
                for(size_t k=0; k<it->second[i][j].exons.size(); k++) {
                    //std::cerr << contigId2Name.at(it->first) << " " << it->second[i][j].exons[k].first << "-" << it->second[i][j].exons[k].second << std::endl;
                    if(k==0)
                        prev_end = it->second[i][j].exons[k].second;
                    else {
                        if(it->second[i][j].exons[k].first < it->second[i][j].exons[k].second) {
                            assert(it->second[i][j].exons[k].first >= prev_end); //non-empty intron
                            if(it->second[i][j].exons[k].first - prev_end >= min_intron_length)
                                fw_introns[it->first].push_back(std::make_pair(std::make_pair(prev_end, it->second[i][j].exons[k].first),i)); //half open intervals
                            //cerr << "pushed back forward intron: " << prev_end << "-" << it->second[i][j].exons[k].first << endl;
                        }
                        else {
                            assert(prev_end >= it->second[i][j].exons[k].first); //non-empty intron
                            if(prev_end - it->second[i][j].exons[k].first >= min_intron_length)
                                rv_introns[it->first].push_back(std::make_pair(std::make_pair(it->second[i][j].exons[k].first, prev_end),i));
                            //cerr << "pushed back reverse intron: " << contigId2Name.at(it->first) << " " << it->second[i][j].exons[k].first << "-" << prev_end << endl;
                        }
                        prev_end = it->second[i][j].exons[k].second;
                    }
                }
            }
        }
    }
    //sort introns in increasing order
    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >::iterator it_ints;
    for(it_ints=fw_introns.begin(); it_ints!=fw_introns.end(); ++it_ints)
    {
        std::sort(it_ints->second.begin(), it_ints->second.end(), gene_interval_order);
    }
    for(it_ints=rv_introns.begin(); it_ints!=rv_introns.end(); ++it_ints)
    {
        std::sort(it_ints->second.begin(), it_ints->second.end(), gene_interval_order);
    }
    
}



void GtfReader::getGenes(std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_genes, std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_genes,
                         std::map<unsigned, vector<string> >& fw_gene_names, std::map<unsigned, vector<string> >& rv_gene_names) const
{
  std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
  for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
  {
    //for each locus
    for(size_t i=0; i<it->second.size(); i++)
    {     
      //for each known transcript of locus
      unsigned min = 0;
      unsigned max = 0;
      bool fw;
      for(size_t j=0; j<it->second[i].size(); j++)
      {
	unsigned min_j, max_j;
	//assuming all exons on same strand, and sorted
	if(it->second[i][j].exons[0].first < it->second[i][j].exons[0].second) { //gene on forward strand
	  assert(j==0 || fw == true); //all transcripts on same strand
	  fw = true;
	  assert(it->second[i][j].exons.back().first < it->second[i][j].exons.back().second);	  
	  min_j = it->second[i][j].exons[0].first;
	  max_j = it->second[i][j].exons.back().second;
	  assert(min_j < max_j);
	} else { //gene on reverse strand
	  assert(j==0 || fw == false); //all transcripts on same strand
	  fw = false;
	  assert(it->second[i][j].exons.back().first > it->second[i][j].exons.back().second);
	  min_j = it->second[i][j].exons.back().second;
	  max_j = it->second[i][j].exons[0].first;
	  assert(min_j < max_j);
	}
	if(j==0 || min_j < min)
	  min = min_j;
	if(j==0 || max_j > max)
	  max = max_j;
      }
      assert(min < max);
        if(fw) {
            fw_genes[it->first].push_back(std::make_pair(min,max));
            fw_gene_names[it->first].push_back(geneNames.at(it->first)[i]);
        }
        else {
            rv_genes[it->first].push_back(std::make_pair(min,max));
            rv_gene_names[it->first].push_back(geneNames.at(it->first)[i]);
        }
    }
  }
  //sort genes in increasing order
  std::map<unsigned, vector<std::pair<unsigned,unsigned> > >::iterator it_genes;
  for(it_genes=fw_genes.begin(); it_genes!=fw_genes.end(); ++it_genes)
  {
    std::sort(it_genes->second.begin(), it_genes->second.end());
  }
  for(it_genes=rv_genes.begin(); it_genes!=rv_genes.end(); ++it_genes)
  {
    std::sort(it_genes->second.begin(), it_genes->second.end());
  }
}


void GtfReader::getGenes(std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
			 std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes) const
{
  std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it;
  for(it=knownTranscripts.begin(); it!=knownTranscripts.end(); ++it)
  {
    //for each locus
    for(size_t i=0; i<it->second.size(); i++)
    {     
      //for each known transcript of locus
      unsigned min = 0;
      unsigned max = 0;
      bool fw;
      for(size_t j=0; j<it->second[i].size(); j++)
      {
	unsigned min_j, max_j;
	//assuming all exons on same strand, and sorted
	if(it->second[i][j].exons[0].first < it->second[i][j].exons[0].second) { //gene on forward strand
	  assert(j==0 || fw == true); //all transcripts on same strand
	  fw = true;
	  assert(it->second[i][j].exons.back().first < it->second[i][j].exons.back().second);	  
	  min_j = it->second[i][j].exons[0].first;
	  max_j = it->second[i][j].exons.back().second;
	  assert(min_j < max_j);
	} else { //gene on reverse strand
	  assert(j==0 || fw == false); //all transcripts on same strand
	  fw = false;
	  assert(it->second[i][j].exons.back().first > it->second[i][j].exons.back().second);
	  min_j = it->second[i][j].exons.back().second;
	  max_j = it->second[i][j].exons[0].first;
	  assert(min_j < max_j);
	}
	if(j==0 || min_j < min)
	  min = min_j;
	if(j==0 || max_j > max)
	  max = max_j;
      }
      assert(min < max);
        if(fw) {
	  fw_genes[it->first].push_back(std::make_pair(std::make_pair(min,max),i));         
        }
        else {
	  rv_genes[it->first].push_back(std::make_pair(std::make_pair(min,max),i));           
        }
    }
  }
  //sort genes in increasing order
  std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>, unsigned> > >::iterator it_genes;
  for(it_genes=fw_genes.begin(); it_genes!=fw_genes.end(); ++it_genes)
  {
    std::sort(it_genes->second.begin(), it_genes->second.end(), gene_interval_order);
  }
  for(it_genes=rv_genes.begin(); it_genes!=rv_genes.end(); ++it_genes)
  {
    std::sort(it_genes->second.begin(), it_genes->second.end(), gene_interval_order);
  }
}
