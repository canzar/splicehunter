/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */



#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <algorithm>
#include <set>

#include "NovelTrans.h"
#include "GtfReader.h"
#include "PacBioBamReader.h"
#include "FastaReader.h"
#include "Parameters.h"


using std::cout;
using std::cerr;
using std::endl;
using std::vector;
using std::string;
using std::set;


NovelTrans::NovelTrans(const string& gtf, const vector<string>& bams, const vector<string>* batch,
                       const vector<string>* sample, const string& fasta) : gtf_reader(gtf)
{    
    vector<string>::const_iterator it_bam = bams.begin();
    for(; it_bam != bams.end(); ++it_bam)
    {
        bam_readers.push_back(new PacBioBamReader(*it_bam));
    }

    this->batch = batch;
    this->sample = sample;
    
    reference_sequences = 0;    
    if(fasta != "")
    {
        //read reference sequence
        std::ifstream reference_istream(fasta.c_str());
        if (reference_istream.fail()) {
            throw std::runtime_error("Error opening file \"" + fasta + "\".");
        }
        reference_sequences = FastaReader::parse(reference_istream).release();
        if (reference_sequences->size() == 0) {
            throw std::runtime_error("Error: references sequences empty.");
        }
    }

    //compute reference stop codons by translating from annotated start codon
    const std::map<unsigned, vector<vector<GtfReader::Transcript> > >& knownTranscripts = gtf_reader.getKnownTranscripts();
    std::map<unsigned, vector<vector<GtfReader::Transcript> > >::const_iterator it_ctg;
    for(it_ctg = knownTranscripts.begin(); it_ctg != knownTranscripts.end(); ++it_ctg)
    {
        string chr = gtf_reader.getContigNames().at(it_ctg->first);
        std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
        it_refseq = reference_sequences->find(chr);
        if(it_refseq == reference_sequences->end()) {
            cerr << "Reference \"" << chr << "\" missing." << endl;
            continue;
        }
        const NamedDnaSequence& ref_sequence = *it_refseq->second;

        //for each locus
        for(size_t i=0; i < it_ctg->second.size(); i++)
        {
            if(i==0) ref_stop_codons[it_ctg->first];
            if(it_ctg->second[i][0].start_codon == -1u) {
                ref_stop_codons.at(it_ctg->first).push_back(-1u);
                continue;
            }
            vector<pair<unsigned,unsigned> > exon_chain;
            bool reverse = false;
            for(size_t k=0; k<it_ctg->second[i][0].exons.size(); k++) //go over all exons in first (and only) annotated transcript
            {
                if(it_ctg->second[i][0].exons[k].first < it_ctg->second[i][0].exons[k].second)
                    exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].first, it_ctg->second[i][0].exons[k].second));
                else {
                    exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].second,  it_ctg->second[i][0].exons[k].first));
                    reverse = true;
                }
            }
            std::sort(exon_chain.begin(), exon_chain.end()); //probably not necessary
            string protseq = "";
            string mrna = "";
            if(!reverse) {
                bool valid_sc = build_mrna_fw(exon_chain, ref_sequence, it_ctg->second[i][0].start_codon, mrna);
                if(valid_sc)
                    protseq = translate(mrna);
                assert( valid_sc || chr == "MT" );
                if(protseq.size()>0) {
                    ref_stop_codons.at(it_ctg->first).push_back( getStopCodonPos(exon_chain, protseq.size(), it_ctg->second[i][0].start_codon) );
                    //cerr << chr << " " << it_ctg->second[i][0].start_codon << endl;
                    //cerr << ref_stop_codons.at(it_ctg->first).back() << endl;
                } else
                    ref_stop_codons.at(it_ctg->first).push_back(-1u); //such that ordering is consistent with knownTranscripts
            } else {
                bool valid_sc = build_mrna_rv(exon_chain, ref_sequence, it_ctg->second[i][0].start_codon, mrna);
                if(valid_sc)
                    protseq = translate(mrna);
                assert( valid_sc || chr == "MT" );
                if(protseq.size()>0) {
                    ref_stop_codons.at(it_ctg->first).push_back( getStopCodonPosRev(exon_chain, protseq.size(), it_ctg->second[i][0].start_codon) );
                    //cerr << chr << " " << it_ctg->second[i][0].start_codon << endl;
                    //cerr << ref_stop_codons.at(it_ctg->first).back() << endl;
                } else
                    ref_stop_codons.at(it_ctg->first).push_back(-1u);
            }
        }
        assert(it_ctg->second.size() == ref_stop_codons.at(it_ctg->first).size());
    }

   
    if(Parameters::nc_anno_file != "")
    {
        // outputs all longest ORF for each gene in input gtf (used for ncRNA in paper)
        GtfReader nc_gtf(Parameters::nc_anno_file);
        
        std::ofstream nc_protseq_file;
        nc_protseq_file.open("nc_protseq.fa");
        unsigned nc_prot_nr = 1;
        const std::map<unsigned, vector<vector<GtfReader::Transcript> > >& knownNCTranscripts = nc_gtf.getKnownTranscripts();
        for(it_ctg = knownNCTranscripts.begin(); it_ctg != knownNCTranscripts.end(); ++it_ctg)
        {
            string chr = nc_gtf.getContigNames().at(it_ctg->first);
            std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
            it_refseq = reference_sequences->find(chr);
            if(it_refseq == reference_sequences->end()) {
                //cerr << "Reference \"" << chr << "\" missing." << endl;
                continue;
            }
            const NamedDnaSequence& ref_sequence = *it_refseq->second;
            for(size_t i=0; i < it_ctg->second.size(); i++)
            {
                vector<pair<unsigned,unsigned> > exon_chain;
                bool reverse = false;
                for(size_t k=0; k<it_ctg->second[i][0].exons.size(); k++) //go over all exons in first (and only) annotated transcript
                {
                    if(it_ctg->second[i][0].exons[k].first < it_ctg->second[i][0].exons[k].second)
                        exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].first, it_ctg->second[i][0].exons[k].second));
                    else {
                        exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].second,  it_ctg->second[i][0].exons[k].first));
                        reverse = true;
                    }
                }
                std::sort(exon_chain.begin(), exon_chain.end()); //probably not necessary
                string mrna = "";
                string protseq = "";
                string longest_protseq = "";
                if(!reverse)
                {
                    for(vector<pair<unsigned,unsigned> >::const_iterator it_virt_iso_ex = exon_chain.begin(); it_virt_iso_ex != exon_chain.end(); ++it_virt_iso_ex)
                    {
                        mrna += ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second);
                    }
                    unsigned i = 0;
                    while(i<mrna.size()-8) //start codon, end codon, at least one aa
                    {
                        if(mrna.substr(i,3)=="ATG") {
                            protseq = translate(mrna.substr(i));
                            if(protseq.size() > longest_protseq.size())
                                longest_protseq = protseq;
                        }
                        i++;
                    }
                } else
                {
                    for(vector<pair<unsigned,unsigned> >::const_reverse_iterator it_virt_iso_ex = exon_chain.rbegin(); it_virt_iso_ex != exon_chain.rend(); ++it_virt_iso_ex)
                    {
                        mrna += getRevCompl(ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second));
                    }
                    unsigned i = 0;
                    while(i<mrna.size()-8) //start codon, end codon, at least one aa
                    {
                        if(mrna.substr(i,3)=="ATG") {
                            protseq = translate(mrna.substr(i));
                            if(protseq.size() > longest_protseq.size())
                                longest_protseq = protseq;
                        }
                        i++;
                    }
                }
                
                if(longest_protseq.size()>0) {
                    std::stringstream ss;
                    ss << ">ncP" << nc_prot_nr;
                    nc_protseq_file << ss.str() << endl;
                    nc_protseq_file << longest_protseq << endl;
                    nc_prot_nr++;
                }
            }
        }
        nc_protseq_file.close();
    }
   
   
    if(Parameters::anno_intron_file != "")
    {
        std::ofstream annoint_file;
        annoint_file.open(Parameters::anno_intron_file);
        
        //output lengths/hexamers of all annotated introns to compare to retained intron lengths
        for(it_ctg = knownTranscripts.begin(); it_ctg != knownTranscripts.end(); ++it_ctg)
        {
            string chr = gtf_reader.getContigNames().at(it_ctg->first);
            std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
            it_refseq = reference_sequences->find(chr);
            if(it_refseq == reference_sequences->end()) {
                //cerr << "Reference \"" << chr << "\" missing." << endl;
                continue;
            }
            const NamedDnaSequence& ref_sequence = *it_refseq->second;
            
            //for each locus
            for(size_t i=0; i < it_ctg->second.size(); i++)
            {
                //cerr << "next locus" << endl;
                vector<pair<unsigned,unsigned> > exon_chain;
                bool reverse = false;
                for(size_t k=0; k<it_ctg->second[i][0].exons.size(); k++) //go over all exons in first (and only) annotated transcript
                {
                    //cerr << "next" << endl;
                    if(it_ctg->second[i][0].exons[k].first < it_ctg->second[i][0].exons[k].second)
                    {
                        exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].first, it_ctg->second[i][0].exons[k].second));
                        //cerr << exon_chain.back().first << " " << exon_chain.back().second << endl;
                    } else {
                        exon_chain.push_back(std::make_pair(it_ctg->second[i][0].exons[k].second,  it_ctg->second[i][0].exons[k].first));
                        reverse = true;
                        //cerr << exon_chain.back().first << " " << exon_chain.back().second << endl;
                    }
                }
                if(exon_chain.size()>1)
                {
                    if(!reverse)
                    {
                        vector<pair<unsigned,unsigned> >::const_iterator it = exon_chain.begin();
                        for(++it; it != exon_chain.end(); ++it) {
                            //cerr <<  chr << " - annotated intron length: " << it->first - (it-1)->second << endl;
                            annoint_file << get_hexamers(std::make_pair((it-1)->second,it->first), ref_sequence, false) << " - " << it->first - (it-1)->second << endl;
                        }
                    } else
                    {
                        vector<pair<unsigned,unsigned> >::const_reverse_iterator it = exon_chain.rbegin();
                        for(++it; it != exon_chain.rend(); ++it) {
                            //cerr <<  chr << " - annotated intron length: " << it->first - (it-1)->second << endl;
                            annoint_file << get_hexamers(std::make_pair((it-1)->second,it->first), ref_sequence, true) << " - " <<  it->first - (it-1)->second << endl;
                        }
                    }
                }
            }
        }
        annoint_file.close();
    }
    
}

NovelTrans::~NovelTrans()
{
    for(size_t i = 0; i<bam_readers.size(); ++i)
    {
        delete bam_readers[i];        
    }
}

bool NovelTrans::interval_order(const std::pair<unsigned, interval_locator_t> i, const std::pair<unsigned, interval_locator_t> j)
{
    return i.first > j.first;
}

bool NovelTrans::ss_order(const std::pair<unsigned, ss_locator_t> i, const std::pair<unsigned, ss_locator_t> j)
{
    return i.first > j.first;
}

bool NovelTrans::readthrough_cmp(const pair<intronchain_cnt_t, std::set<unsigned> >& a, const pair<intronchain_cnt_t, std::set<unsigned> >& b)
{
    return intronchain_cnt_cmp(a.first, b.first);
}

//return true iff two inton chains are equal, ignoring first and last element (tss/tes)
bool NovelTrans::ic_equal(const intronchain_cnt_t&a, const intronchain_cnt_t& b) const
{
    if(a.first.size()!=b.first.size())
        return false;

    //compare two intron chains lexicographically and ignore tss/tes
    intronchain_t::const_iterator it_a = a.first.begin()+1;
    intronchain_t::const_iterator it_b = b.first.begin()+1;
    for (; it_a!=a.first.end()-1 && it_b!=b.first.end()-1; it_a++, it_b++)
    {
        if(*it_a != *it_b)
            return false;
    }

    return true;
}

bool NovelTrans::intronchain_reads_cmp(const intronchain_reads_t& a, const intronchain_reads_t& b)
{
    //compare two intron chains lexicographically and ignore tss/tes
    intronchain_t::const_iterator it_a = a.first.begin();
    intronchain_t::const_iterator it_b = b.first.begin();
    for (; it_a!=a.first.end() && it_b!=b.first.end(); it_a++, it_b++)
    {
        //pick smaller first coordinate
        if (it_a->first < it_b->first) {
            return true;
        }
        else if (it_b->first < it_a->first) {
            return false;
        }

        //pick smaller second coordinate if first coordinates equal
        if (it_a->second < it_b->second) {
            return true;
        }
        else if (it_b->second < it_a->second) {
            return false;
        }
    }
    //if all introns match
    if (it_a!=a.first.end()-1) { //...b<a if a longer
        return false;
    }
    else if (it_b != b.first.end()-1) { //...a<b if b longer
        return true;
    }
    else //intron chains equal
        return false;
}

bool NovelTrans::intronchain_cnt_cmp(const intronchain_cnt_t& a, const intronchain_cnt_t& b)
{
    //compare two intron chains lexicographically and ignore tss/tes
    intronchain_t::const_iterator it_a = a.first.begin();
    intronchain_t::const_iterator it_b = b.first.begin();
    for (; it_a!=a.first.end() && it_b!=b.first.end(); it_a++, it_b++)
    {
        //pick smaller first coordinate
        if (it_a->first < it_b->first) {
            return true;
        }
        else if (it_b->first < it_a->first) {
            return false;
        }
        
        //pick smaller second coordinate if first coordinates equal
        if (it_a->second < it_b->second) {
            return true;
        }
        else if (it_b->second < it_a->second) {
            return false;
        }
    }
    //if all introns match
    if (it_a!=a.first.end()-1) { //...b<a if a longer
        return false;
    }
    else if (it_b != b.first.end()-1) { //...a<b if b longer
        return true;
    }
    else //intron chains equal
        return false;
}





/*
 * Returns the number of novel read introns that are retained in an annotated exon
 */
unsigned NovelTrans::get_introns_in_exons(const vector<std::pair<unsigned,unsigned> >& read_introns,
                                      const vector<std::pair<unsigned,unsigned> >& gene_exons,
                                      const vector<std::pair<unsigned,unsigned> >& gene_introns,
                                      std::map<unsigned, set<std::pair<unsigned,unsigned> > >& intinex) const
{
    unsigned found = 0;

    //for all read introns
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readint;
    for(it_readint=read_introns.begin()+1; it_readint!=read_introns.end()-1; ++it_readint)
    {
        //cerr << "read intron: " << it_readint->first << " " << it_readint->second << endl;
        
        //check whether intron is annotated
        if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint))
            continue;
        
        //look for exons spanning read intron
        unsigned certificate_idx;
        if(is_strictly_contained(gene_exons, *it_readint, certificate_idx))
        {
            /*
            cout << "Intron in exon: " << endl;
            cout << it_readint->first << " " << it_readint->second << endl;
            continue;
            */
            intinex[certificate_idx].insert(*it_readint);

            found++;
        }
    }
    return found;
}


/*
 * Returns the number of novel introns on same primary transcript
 * that skip at least one annotated exon. The simultaneous skipping 
 * of multiple exons of a given intron counts as one exon skipping event.
 * It also returns in skipped_exons for each such intron the sequence of skipped exons.
 */
unsigned NovelTrans::get_exon_skippings(const vector<std::pair<unsigned,unsigned> >& read_introns,
                                        const vector<std::pair<unsigned,unsigned> >& gene_exons,
                                        const vector<std::pair<unsigned,unsigned> >& gene_introns,
                                        vector<vector<unsigned> >& skipped_exons) const
{
    unsigned found = 0;
    skipped_exons.clear();
    //std::map<unsigned, std::map<std::pair<unsigned,unsigned>, unsigned> > novel_exon_skippings;
    
    //for all read introns
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readint;
    for(it_readint=read_introns.begin()+1; it_readint!=read_introns.end()-1; ++it_readint)
    {
        //cerr << "read intron: " << it_readint->first << " " << it_readint->second << endl;
        
        //check whether intron is part of same primary transript as annotated
        if (it_readint->first < gene_exons[0].first || it_readint->second > gene_exons.back().second) {
            continue;
        }
        
        //check whether intron is annotated
        if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint))
            continue;
        
        //look for exons strictly spanned by read intron
        vector<unsigned> certificates;
        contains_strictly(gene_exons, *it_readint, certificates);
        if(certificates.size())
        {
            /*
            cout << "Skipped exon(s): " << endl;
            vector<std::pair<unsigned,unsigned> >::const_iterator it_cert;
            for(it_cert = certificates.begin(); it_cert != certificates.end(); ++it_cert)
            {
                //TODO check whether exon skipped in gene annotation
                
                cout << it_cert->first << " " << it_cert->second << endl;
            }
            */
            found++;
            skipped_exons.push_back(certificates);
        }
    }
    return found;
}


/*
 * chose reference strand from query and make query strand unspecific
 */
bool NovelTrans::get_query_ref_from_strand(const std::pair<unsigned,unsigned>& query,
                                           std::pair<unsigned,unsigned>& strand_unspec_query,
                                           const std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& fw_ref,
                                           const std::map<unsigned, vector<std::pair<unsigned,unsigned> > >& rv_ref,
                                           unsigned ctg,
                                           const vector<std::pair<unsigned,unsigned> >*& reference) const
{
    if (query.first > query.second) //have to look at reverse strand
    {      
        if (rv_ref.find(ctg)==rv_ref.end())
            return false;
        strand_unspec_query.first = query.second;
        strand_unspec_query.second = query.first;
        reference = &(rv_ref.at(ctg));
    } else //look at forward strand
    {        
        if (fw_ref.find(ctg)==fw_ref.end())
            return false;
        strand_unspec_query = query;
        reference = &(fw_ref.at(ctg));
    }
    return true;
}





/*
 * Returns the number of novel read exons that span at least one annotated introns
 * An exon that retains multiple introns counts as one intron retention event
 */
unsigned NovelTrans::get_intron_retentions(vector<std::pair<unsigned,unsigned> >& read_exons,
                                           vector<std::pair<unsigned,unsigned> >& gene_exons,
                                           vector<std::pair<unsigned,unsigned> >& gene_introns,
                                           vector<vector<unsigned> >& retained_introns) const
{
    unsigned found = 0;
    retained_introns.clear();

    //for all read exons
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readex;
    for(it_readex=read_exons.begin(); it_readex!=read_exons.end(); ++it_readex)
    {
        //check whether exon is annotated
        if(std::binary_search(gene_exons.begin(), gene_exons.end(), *it_readex))
            continue;
        
        //look for introns strictly spanned by read exon
        vector<unsigned> certificates;
        contains_strictly(gene_introns, *it_readex, certificates);
        if(certificates.size())
        {
            /*
            cout << "Retained intron(s): " << endl;
            vector<std::pair<unsigned,unsigned> >::const_iterator it_cert;
            for(it_cert = certificates.begin(); it_cert != certificates.end(); ++it_cert)
            {
                //TODO check whether intron retained in gene annotation

                cout << it_cert->first << " " << it_cert->second << endl;
            }
            */
            found++;
            retained_introns.push_back(certificates);
        }
    }
    return found;
}


void NovelTrans::sort_by_occ(std::map<unsigned, std::map<std::pair<unsigned,unsigned>, unsigned> >& novel_intervals) const
{
    unsigned tot_nof_novel_intervals = 0;
    vector<std::pair<unsigned, interval_locator_t> > all_novel_intervals; //number of reads (first) supporting intron/exon (second)
    
    //restructure novel_ints into all_novel_ints to be able to sort by number of supporting reads, over all contigs
    std::map<unsigned, std::map<std::pair<unsigned,unsigned>, unsigned> >::iterator it_contigs = novel_intervals.begin();
    for(; it_contigs != novel_intervals.end(); ++it_contigs)
    {
        std::map<std::pair<unsigned,unsigned>, unsigned>::iterator it_ni = it_contigs->second.begin();
        for(; it_ni != it_contigs->second.end(); ++it_ni)
        {
            tot_nof_novel_intervals += 1;//it_ni->second;
            interval_locator_t il(it_contigs->first, it_ni);
            all_novel_intervals.push_back( std::pair<unsigned, interval_locator_t>(it_ni->second,il) );
        }
    }
    sort(all_novel_intervals.begin(), all_novel_intervals.end(), interval_order);
 
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_genes;
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_genes;
    std::map<unsigned, vector<string> > fw_gene_names;
    std::map<unsigned, vector<string> > rv_gene_names;
    gtf_reader.getGenes(fw_genes, rv_genes, fw_gene_names, rv_gene_names);
    
    //go over all novel ints and output
    std::vector<std::pair<unsigned, interval_locator_t> >::const_iterator it_ni = all_novel_intervals.begin();
    for(; it_ni != all_novel_intervals.end(); ++it_ni)
    {
        //determine gene
        string gene_name = "unknown";
        bool fw = it_ni->second.second->first.first < it_ni->second.second->first.second;
        unsigned certificate_idx;
        if(fw) {
            if(fw_genes.find(it_ni->second.first)!=fw_genes.end())
            {
                if(is_strictly_contained(fw_genes.at(it_ni->second.first), std::make_pair(it_ni->second.second->first.first, it_ni->second.second->first.second), certificate_idx))
                    gene_name = fw_gene_names.at(it_ni->second.first)[certificate_idx];
            }
        }
        else
        {
            if(rv_genes.find(it_ni->second.first)!=rv_genes.end())
            {
                if(is_strictly_contained(rv_genes.at(it_ni->second.first), std::make_pair(it_ni->second.second->first.second, it_ni->second.second->first.first), certificate_idx))
                    gene_name = rv_gene_names.at(it_ni->second.first)[certificate_idx];
            }
        }
        std::cout << "chr" + gtf_reader.getContigNames().at(it_ni->second.first) << "\t" << it_ni->second.second->first.first;
        std::cout << "\t" << it_ni->second.second->first.second << " (" << gene_name << ")\t" << it_ni->first << std::endl;
    }
}

void NovelTrans::sort_by_occ(std::map<unsigned, std::map<unsigned, unsigned> >& novel_ss, std::string ss_type, bool fw_strand) const
{
    unsigned tot_nof_novel_ss = 0;
    vector<std::pair<unsigned, ss_locator_t> > all_novel_ss; //number of reads (first) supporting ss (second)
    
    //restructure novel_ss into all_novel_ss to be able to sort by number of supporting reads, over all contigs
    std::map<unsigned, std::map<unsigned, unsigned> >::iterator it_contigs = novel_ss.begin();
    for(; it_contigs != novel_ss.end(); ++it_contigs)
    {
        std::map<unsigned, unsigned>::iterator it_ni = it_contigs->second.begin();
        for(; it_ni != it_contigs->second.end(); ++it_ni)
        {
            tot_nof_novel_ss += 1;//it_ni->second;
            ss_locator_t il(it_contigs->first, it_ni);
            all_novel_ss.push_back( std::pair<unsigned, ss_locator_t>(it_ni->second,il) );
        }
    }
    sort(all_novel_ss.begin(), all_novel_ss.end(), ss_order);

    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_genes;
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_genes;
    std::map<unsigned, vector<string> > fw_gene_names;
    std::map<unsigned, vector<string> > rv_gene_names;
    gtf_reader.getGenes(fw_genes, rv_genes, fw_gene_names, rv_gene_names);
    
    //go over all novel ss and output
    std::vector<std::pair<unsigned, ss_locator_t> >::const_iterator it_ni = all_novel_ss.begin();
    for(; it_ni != all_novel_ss.end(); ++it_ni)
    {
        //determine gene
        string gene_name = "unknown";
        unsigned certificate_idx;
        if(fw_strand) {
            if(fw_genes.find(it_ni->second.first)!=fw_genes.end())
            {
                if(is_strictly_contained(fw_genes.at(it_ni->second.first), std::make_pair(it_ni->second.second->first, it_ni->second.second->first+1), certificate_idx))
                    gene_name = fw_gene_names.at(it_ni->second.first)[certificate_idx];
            }
        }
        else
        {
            if(rv_genes.find(it_ni->second.first)!=rv_genes.end())
            {
                if(is_strictly_contained(rv_genes.at(it_ni->second.first), std::make_pair(it_ni->second.second->first, it_ni->second.second->first+1), certificate_idx))
                    gene_name = rv_gene_names.at(it_ni->second.first)[certificate_idx];
            }
        }

        std::cout << ss_type << ": chr" + gtf_reader.getContigNames().at(it_ni->second.first) << "\t" << it_ni->second.second->first << " (" << gene_name << ")\t" << it_ni->first << std::endl;
    }
}



/*
 * Returns the number of novel read exons that do not overlap any exon of the assigned gene
 */
unsigned NovelTrans::get_novel_exons(const vector<std::pair<unsigned,unsigned> >& read_exons,
                                     const vector<std::pair<unsigned,unsigned> >& gene_exons,
                                     const vector<std::pair<unsigned,unsigned> >& gene_introns,
                                     bool inclusion) const
{
    unsigned found = 0;

    //for all read exons
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readex;
    for(it_readex=read_exons.begin(); it_readex!=read_exons.end(); ++it_readex)
    {
        //check whether exon is annotated
        if(std::binary_search(gene_exons.begin(), gene_exons.end(), *it_readex))
            continue;

        if( is_exon_novel(gene_exons, *it_readex) )
        {
            //cout << "Novel exon: " << it_readex->first << " " << it_readex->second << endl;
            bool included = false;
            vector<std::pair<unsigned,unsigned> >::const_iterator it_gene_int;
            for(it_gene_int = gene_introns.begin(); it_gene_int != gene_introns.end(); ++it_gene_int)
            {
                if( it_gene_int->first < it_readex->first && it_gene_int->second > it_readex->second //gene intron spans read exons
                    && read_exons.at(0).first < it_gene_int->first && it_gene_int->second < read_exons.back().second ) //gene intron on same primary transcript
                {
                    included = true;
                    break;
                }
            }
            if(inclusion && included)
                found++;
            else if(!inclusion && !included)
                found++;
        }
    }
    return found;
}



/*
 * Returns the number of novel read introns that a) combine known donor/acceptor in a new way (found[0]),
 * b) have (only) a novel donor (found[1]), c) have (only) a novel acceptor (found[2]),
 * d) have both novel donor and acceptor
 */
void NovelTrans::get_novel_introns(const vector<std::pair<unsigned,unsigned> >& read_introns,
                                   const vector<unsigned>* nof_sup,
                                   const vector<std::pair<unsigned,unsigned> >& gene_introns,
                                   const vector<std::pair<unsigned,unsigned> >& gene_exons,
                                   const vector<unsigned>& gene_donors,
                                   const vector<unsigned>& gene_acc,
                                   unsigned gene_tss,
                                   unsigned gene_tes,
                                   bool gene_on_rv,
                                   //std::multiset<pair<unsigned,unsigned> >* novel_introns,
                                   //std::multiset<pair<unsigned,unsigned> >* anno_introns,
                                   set<pair<unsigned,unsigned> >* novel_introns,
                                   set<pair<unsigned,unsigned> >* anno_introns,
                                   vector<unsigned>& found) const
{
    assert(found.size()==4);
    found.assign(4,0);
    //get annotated donor and acceptor sites
    //donor: 5' end of intron (first base in intron)
    //acceptor: 3' end of intron (last base in intron)
    
    //for all read introns
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readint;
    for(it_readint=read_introns.begin()+1; it_readint!=read_introns.end()-1; ++it_readint)
    {
        //cerr << "read intron: " << it_readint->first << " " << it_readint->second << endl;

        //get dimer
        //string dimer = get_dimer(*it_readint, ref_sequence, gene_on_rv);

        //check whether intron is annotated
        if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint)) {
            anno_introns->insert(*it_readint);
            continue;
        } else
        {
            //uncomment if introns counted by multiplicity
            //vector<unsigned>::const_iterator it = nof_sup->begin();
            //for(; it!=nof_sup->end(); ++it) {
            //    for(size_t i=0; i<*it; ++i)
                    novel_introns->insert(*it_readint);
            //}
        }

        bool has_novel_donor = false;
        bool has_novel_acc = false;
        //determine annotated donor/acceptor sites on same chr/strand
        if (gene_on_rv) //have to look at reverse strand
        {
            //donor must not be a ss in reference
            has_novel_donor = !binary_search(gene_donors.begin(), gene_donors.end(), (it_readint->second)-1);
            //5' exon must overlap exon in reference (it will probably also be on the same primary RNA)
            has_novel_donor = has_novel_donor && !is_exon_novel(gene_exons, std::make_pair(it_readint->second, (it_readint+1)->first));
            //probably redundant: donor must lie on same primary transcript
            has_novel_donor = has_novel_donor && gene_tss < (it_readint->second)-1 &&  (it_readint->second)-1 < gene_tes;
            
            //acceptor must not be a ss in reference
            has_novel_acc = !binary_search(gene_acc.begin(), gene_acc.end(), it_readint->first);
            //3' exon must overlap exon in reference (it will probably also be on the same primary RNA)
            has_novel_acc = has_novel_acc && !is_exon_novel(gene_exons, std::make_pair((it_readint-1)->second, it_readint->first));
            //probably redundant: acceptor must lie on same primary transcript
            has_novel_acc = has_novel_acc && gene_tss < it_readint->first && it_readint->first < gene_tes;
        } else //look at forward strand
        {
            //donor must not be a ss in reference
            has_novel_donor = !binary_search(gene_donors.begin(), gene_donors.end(), it_readint->first);
            //5' exon must overlap exon in reference (it will probably also be on the same primary RNA)
            has_novel_donor = has_novel_donor && !is_exon_novel(gene_exons, std::make_pair((it_readint-1)->second, it_readint->first));
            //probably redundant: donor must lie on same primary transcript
            has_novel_donor = has_novel_donor && gene_tss < it_readint->first && it_readint->first  < gene_tes;
            
            //acceptor must not be a ss in reference
            has_novel_acc = !binary_search(gene_acc.begin(), gene_acc.end(), (it_readint->second)-1);
            //3' exon must overlap exon in reference (it will probably also be on the same primary RNA)
            has_novel_acc = has_novel_acc && !is_exon_novel(gene_exons, std::make_pair(it_readint->second, (it_readint+1)->first));
            //probably redundant: acceptor must lie on same primary transcript
            has_novel_acc = has_novel_acc && gene_tss < (it_readint->second)-1 &&  (it_readint->second)-1 < gene_tes;
        }
        
        if (!has_novel_acc && !has_novel_donor) { //assuming a single reference transcript, they have to come from the same primary transcript
            //cout << "Novel intron structure: ";
            //cout << it_readint->first << " " << it_readint->second << endl;
            found.at(0)++;
        }
        unsigned read_donor = gene_on_rv ? (it_readint->second)-1 : it_readint->first;
        if ( has_novel_donor && !has_novel_acc ) {
             //&& gene_tss < read_donor && read_donor < gene_tes ) {
            //cout << "Intron with novel donor site: ";
            //cout << it_readint->first << " " << it_readint->second << endl;
            found.at(1)++;
        }
        unsigned read_acc = gene_on_rv ? it_readint->first : (it_readint->second)-1;
        if ( !has_novel_donor && has_novel_acc ) {
             //&& gene_tss < read_acc && read_acc < gene_tes ) {
            //cout << "Intron with novel acceptor site: ";
            //cout << it_readint->first << " " << it_readint->second << endl;
            found.at(2)++;
        }
        if ( has_novel_donor && has_novel_acc ) {
             //&& gene_tss < read_donor && read_donor < gene_tes
             //&& gene_tss < read_acc && read_acc < gene_tes ) {
            //cout << "Intron with novel acceptor and novel donor site: ";
            //cout << it_readint->first << " " << it_readint->second << endl;
            vector<pair<unsigned,unsigned> > curr_read_intron;
            curr_read_intron.push_back(read_introns.front()); //get_introns_in_exons assumes tss/tes as first/last element
            curr_read_intron.push_back(*it_readint);
            curr_read_intron.push_back(read_introns.back());
            std::map<unsigned, set<std::pair<unsigned,unsigned> > > intinex; //not used.
            if( get_introns_in_exons(curr_read_intron, gene_exons, gene_introns, intinex) == 0) {
                found.at(3)++;
            }
        }
    }
}



int NovelTrans::resolve_ambiguity_by_overlap(std::set<unsigned>& ambig_genes,
                                             unsigned contig_id,
                                             std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& ctg_genes,
                                             std::pair<unsigned,unsigned>& alg_range,
                                             vector<std::pair<unsigned,unsigned> >& read_exons) const
{        
  vector<std::pair<unsigned,unsigned> > all_overlap_genes;
  get_all_overlap(contig_id, ctg_genes, alg_range, read_exons, all_overlap_genes);
  
  //get genes with largest and second largest overlap among intron matching genes
  unsigned largest_gene;
  unsigned max_overlap=0;
  unsigned second_max_overlap=0;
  vector<std::pair<unsigned,unsigned> >::const_iterator it; 
  for(it=all_overlap_genes.begin(); it!=all_overlap_genes.end(); it++)
  {    
    if(ambig_genes.find(it->first) != ambig_genes.end())
    {
      if(it->second >= max_overlap) {
	second_max_overlap = max_overlap;
	max_overlap = it->second;
	largest_gene = it->first;
      } else if(it->second > second_max_overlap) {
	second_max_overlap = it->second;	
      }
    }
  }	  
  if(max_overlap > Parameters::ambig_res_thr * second_max_overlap) {
    assert(max_overlap > 0);    
    return largest_gene; //could also be antisense
  }
  else {    
    return -1; //ignore read?
  }
}

int NovelTrans::resolve_overlap_ambiguity(vector<std::pair<unsigned,unsigned> >& overlap_genes) const
{
  unsigned max_overlap = 0;
  unsigned second_max_overlap = 0;
  unsigned best_gene;

  vector<std::pair<unsigned,unsigned> >::const_iterator it = overlap_genes.begin();
  for(; it != overlap_genes.end(); ++it)
  {
    if(it->second >= max_overlap) {
      second_max_overlap = max_overlap;
      max_overlap = it->second;
      best_gene = it->first;     
    } else if(it->second > second_max_overlap)
      second_max_overlap = it->second;
  }
  assert(max_overlap > 0);
  if(max_overlap > Parameters::ambig_res_thr * second_max_overlap) {
    //cerr << max_overlap << " > " << Parameters::ambig_res_thr * second_max_overlap << endl;
    //cerr << "returning best gene " << best_gene << endl;   
    return best_gene;
  }
  else {
    //cerr << max_overlap << " <= " << Parameters::ambig_res_thr * second_max_overlap << endl;
    return -1;
  }
}

bool NovelTrans::get_max_hit_gene(std::set<unsigned>& intron_match_genes, std::map<unsigned, unsigned>& nof_occ, unsigned& best_gene) const
{ 
  unsigned max_nof_introns = 0;
  bool best_uniq;
  std::set<unsigned>::const_iterator it = intron_match_genes.begin();
  for(; it!= intron_match_genes.end(); ++it) {
    if(nof_occ.at(*it) > max_nof_introns) {
      max_nof_introns = nof_occ.at(*it);
      best_gene = *it;
      best_uniq = true;
    } 
    else if(nof_occ.at(*it) == max_nof_introns) 
      best_uniq = false;
  }
  assert(max_nof_introns > 0);
  return best_uniq;
}

int NovelTrans::assign_gene_by_intron(vector<std::pair<unsigned,unsigned> >& read_introns,
                                      std::pair<unsigned,unsigned>& alg_range,
                                      vector<std::pair<unsigned,unsigned> >& read_exons,
                                      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
                                      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes,
                                      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_introns,
                                      std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_introns,
                                      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_donor,
                                      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_donor,
                                      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_acc,
                                      std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_acc,
                                      unsigned contig_id,
                                      std::set<unsigned>& multi_match_genes) const
{
    assert(!read_introns.empty());
    
    std::set<unsigned> intron_match_fw_genes;
    std::set<unsigned> intron_match_rv_genes;
    std::map<unsigned,unsigned> nof_occ_fw;
    std::map<unsigned,unsigned> nof_occ_rv;
    
    //for all read introns
    vector<std::pair<unsigned,unsigned> >::const_iterator it_readint;
    for(it_readint=read_introns.begin(); it_readint!=read_introns.end(); ++it_readint)
    {
        //cerr << "read intron: " << it_readint->first << " " << it_readint->second << endl;
        
        vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator lower_intron;
        
        //check whether read intron annotated on genes on fw strand
        if(fw_introns.find(contig_id)!=fw_introns.end())
        {
            lower_intron = std::lower_bound(fw_introns.at(contig_id).begin(), fw_introns.at(contig_id).end(), std::make_pair(*it_readint,0), GtfReader::gene_interval_order);
            while(lower_intron != fw_introns.at(contig_id).end() && lower_intron->first == *it_readint)
            {
                //cerr << "lower intron: " << lower_intron->first.first << " " << lower_intron->first.second << endl;
                
                //increment occurrence counter if necessary
                if(nof_occ_fw.find(lower_intron->second) != nof_occ_fw.end()) {
                    nof_occ_fw.at(lower_intron->second) = nof_occ_fw.at(lower_intron->second) + 1;
                } else {
                    nof_occ_fw[lower_intron->second] = 1;
                }
                
                intron_match_fw_genes.insert(lower_intron->second);
                lower_intron++;
            }
        }
        
        //check whether read intron annotated on genes on rv strand
        if(rv_introns.find(contig_id)!=rv_introns.end())
        {
            lower_intron = std::lower_bound(rv_introns.at(contig_id).begin(), rv_introns.at(contig_id).end(), std::make_pair(*it_readint,0), GtfReader::gene_interval_order);
            while(lower_intron != rv_introns.at(contig_id).end() && lower_intron->first == *it_readint)
            {
                //cerr << "lower intron: " << lower_intron->first.first << " " << lower_intron->first.second << endl;
                
                //increment occurrence counter if necessary
                if(nof_occ_rv.find(lower_intron->second) != nof_occ_rv.end()) {
                    nof_occ_rv.at(lower_intron->second) = nof_occ_rv.at(lower_intron->second) + 1;
                } else {
                    nof_occ_rv[lower_intron->second] = 1;
                }
                
                intron_match_rv_genes.insert(lower_intron->second);
                lower_intron++;
            }
        }
    }
    
    //now let's analyse the results
    //----------------------------
    
    if(intron_match_fw_genes.empty() && intron_match_rv_genes.empty()) //no intron matches annotation, look for splice sites
    {
        std::set<unsigned> ss_match_fw_genes;
        std::set<unsigned> ss_match_rv_genes;
        std::map<unsigned,unsigned> nof_occ_ss_fw;
        std::map<unsigned,unsigned> nof_occ_ss_rv;
        
        //for all read introns
        for(it_readint=read_introns.begin(); it_readint!=read_introns.end(); ++it_readint)
        {
            vector<std::pair<unsigned,unsigned> >::const_iterator lower_ss;
            //check whether 'left' splice site in read matches donor on forward or acceptor on reverse strand
            if(fw_donor.find(contig_id)!=fw_donor.end())
            {
                lower_ss = std::lower_bound(fw_donor.at(contig_id).begin(), fw_donor.at(contig_id).end(), std::make_pair(it_readint->first,unsigned(0)));
                while(lower_ss != fw_donor.at(contig_id).end() && lower_ss->first == it_readint->first) {
                    //cerr << "fw donor: " << lower_ss->first << endl;
                    
                    //increment occurrence counter if necessary
                    if(nof_occ_ss_fw.find(lower_ss->second) != nof_occ_ss_fw.end()) {
                        nof_occ_ss_fw.at(lower_ss->second) = nof_occ_ss_fw.at(lower_ss->second) + 1;
                    } else {
                        nof_occ_ss_fw[lower_ss->second] = 1;
                    }
                    
                    ss_match_fw_genes.insert(lower_ss->second);
                    lower_ss++;
                }
            }
            if(rv_acc.find(contig_id)!=rv_acc.end())
            {
                lower_ss = std::lower_bound(rv_acc.at(contig_id).begin(), rv_acc.at(contig_id).end(), std::make_pair(it_readint->first,unsigned(0)));
                while(lower_ss != rv_acc.at(contig_id).end() && lower_ss->first == it_readint->first) {
                    //cerr << "rv acc: " << lower_ss->first << endl;
                    
                    //increment occurrence counter if necessary
                    if(nof_occ_ss_rv.find(lower_ss->second) != nof_occ_ss_rv.end()) {
                        nof_occ_ss_rv.at(lower_ss->second) = nof_occ_ss_rv.at(lower_ss->second) + 1;
                    } else {
                        nof_occ_ss_rv[lower_ss->second] = 1;
                    }
                    
                    ss_match_rv_genes.insert(lower_ss->second);
                    lower_ss++;
                }
            }
            
            //check whether 'right' splice site in read matches acceptor on forward or donor on reverse strand
            if(fw_acc.find(contig_id)!=fw_acc.end())
            {
                lower_ss = std::lower_bound(fw_acc.at(contig_id).begin(), fw_acc.at(contig_id).end(), std::make_pair((it_readint->second)-1,unsigned(0)));
                while(lower_ss != fw_acc.at(contig_id).end() && lower_ss->first == (it_readint->second)-1) {
                    //cerr << "fw acc: " << lower_ss->first << endl;
                    
                    //increment occurrence counter if necessary
                    if(nof_occ_ss_fw.find(lower_ss->second) != nof_occ_ss_fw.end()) {
                        nof_occ_ss_fw.at(lower_ss->second) = nof_occ_ss_fw.at(lower_ss->second) + 1;
                    } else {
                        nof_occ_ss_fw[lower_ss->second] = 1;
                    }
                    
                    ss_match_fw_genes.insert(lower_ss->second);
                    lower_ss++;
                }
            }
            if(rv_donor.find(contig_id)!=rv_donor.end())
            {
                lower_ss = std::lower_bound(rv_donor.at(contig_id).begin(), rv_donor.at(contig_id).end(), std::make_pair((it_readint->second)-1,unsigned(0)));
                while(lower_ss != rv_donor.at(contig_id).end() && lower_ss->first == (it_readint->second)-1) {
                    //cerr << "rv donor: " << lower_ss->first << endl;
                    
                    //increment occurrence counter if necessary
                    if(nof_occ_ss_rv.find(lower_ss->second) != nof_occ_ss_rv.end()) {
                        nof_occ_ss_rv.at(lower_ss->second) = nof_occ_ss_rv.at(lower_ss->second) + 1;
                    } else {
                        nof_occ_ss_rv[lower_ss->second] = 1;
                    }
                    
                    ss_match_rv_genes.insert(lower_ss->second);
                    lower_ss++;
                }
            }
        }
        
        if(ss_match_fw_genes.empty() && ss_match_rv_genes.empty())
            return -2; //treat as read without intron
        
        else if(!ss_match_fw_genes.empty() && !ss_match_rv_genes.empty())
            assert(false);
        
        else { //matches ss on only one strand
            
            //determine case - which strand non-empty
            std::set<unsigned>* ss_match_genes;
            std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >* genes;
            if(ss_match_rv_genes.empty())
            {
                assert(!ss_match_fw_genes.empty());
                //return if unique
                if(ss_match_fw_genes.size()==1) {
                    return *(ss_match_fw_genes.begin());
                }               

                return -6;

                //return gene with highest number of matching splice sites if unique
                unsigned best_gene;
                if( get_max_hit_gene(ss_match_fw_genes, nof_occ_ss_fw, best_gene) ) {
                    assert(nof_occ_ss_fw.at(best_gene)>1);
                    //cerr << nof_occ_ss_fw.at(best_gene) << endl;
                    return best_gene;
                }
                
                ss_match_genes = &ss_match_fw_genes;
                assert(fw_genes.find(contig_id)!=fw_genes.end());
                genes = &(fw_genes);
            } else
            {
                assert(ss_match_fw_genes.empty());
                assert(!ss_match_rv_genes.empty());
                //return if unique
                if(ss_match_rv_genes.size()==1) {
                    return *(ss_match_rv_genes.begin());
                }

                return -6;
                
                //return gene with highest number of matching splice sites if unique
                unsigned best_gene;
                if( get_max_hit_gene(ss_match_rv_genes, nof_occ_ss_rv, best_gene) ) {
                    assert(nof_occ_ss_rv.at(best_gene)>1);
                    //cerr << nof_occ_ss_rv.at(best_gene) << endl;
                    return best_gene;
                }
                
                ss_match_genes = &ss_match_rv_genes;
                assert(rv_genes.find(contig_id)!=rv_genes.end());
                genes = &(rv_genes);
            }
            
            int resolved_gene = resolve_ambiguity_by_overlap(*ss_match_genes, contig_id, *genes, alg_range, read_exons);
            return resolved_gene;
        }
    } 
    else if( (!intron_match_fw_genes.empty() && intron_match_rv_genes.empty()) || 
            (intron_match_fw_genes.empty() && !intron_match_rv_genes.empty()) ) //matches only annotated intron(s) on one strand
    {
        //determine case - which strand non-empty
        std::set<unsigned>* intron_match_genes;
        std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >* genes;
        if(intron_match_rv_genes.empty()) 
        {
            //return if unique
            if(intron_match_fw_genes.size()==1) {	
                return *(intron_match_fw_genes.begin());
            }
            
            //return all fw matching genes
            std::set<unsigned>::const_iterator it_genes = intron_match_fw_genes.begin();
            for(; it_genes != intron_match_fw_genes.end(); ++it_genes)
                multi_match_genes.insert(*it_genes);
            return -3;
            
            /*
             //return gene with highest number of matching introns if unique
             unsigned best_gene;
             if( get_max_hit_gene(intron_match_fw_genes, nof_occ_fw, best_gene) ) {
             assert(nof_occ_fw.at(best_gene)>1);
             //cerr << nof_occ_fw.at(best_gene) << endl;
             return best_gene;
             }
             */
            
            
            intron_match_genes = &intron_match_fw_genes;
            assert(fw_genes.find(contig_id)!=fw_genes.end());
            genes = &(fw_genes);
        } else 
        {
            assert(intron_match_fw_genes.empty());
            //return if unique
            if(intron_match_rv_genes.size()==1) {	
                return *(intron_match_rv_genes.begin());
            }    
            
            //return all rv matching genes
            std::set<unsigned>::const_iterator it_genes = intron_match_rv_genes.begin();
            for(; it_genes != intron_match_rv_genes.end(); ++it_genes)
                multi_match_genes.insert(*it_genes);
            return -4;
            
            /*
             //return gene with highest number of matching introns if unique
             unsigned best_gene;
             if( get_max_hit_gene(intron_match_rv_genes, nof_occ_rv, best_gene) ) {
             assert(nof_occ_rv.at(best_gene)>1);
             //cerr << nof_occ_rv.at(best_gene) << endl;
             return best_gene;
             }
             */
            
            intron_match_genes = &intron_match_rv_genes;
            assert(rv_genes.find(contig_id)!=rv_genes.end());
            genes = &(rv_genes);
        }
        
        return resolve_ambiguity_by_overlap(*intron_match_genes, contig_id, *genes, alg_range, read_exons);
    }
    else if(!intron_match_fw_genes.empty() && !intron_match_rv_genes.empty()) //matches introns on both strands
    {               
        //return all matching genes
        std::set<unsigned>::const_iterator it_genes = intron_match_fw_genes.begin();
        for(; it_genes != intron_match_fw_genes.end(); ++it_genes)
            multi_match_genes.insert(*it_genes);
        for(it_genes = intron_match_rv_genes.begin(); it_genes != intron_match_rv_genes.end(); ++it_genes)
            multi_match_genes.insert(*it_genes);
        return -5;
        //assert(false);
    }
}


int NovelTrans::assign_gene_by_overlap(std::pair<unsigned,unsigned>& alg_range,
                                       vector<std::pair<unsigned,unsigned> >& read_exons,
                                       std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& fw_genes,
                                       std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& rv_genes,
                                       unsigned contig_id,
                                       bool orig_fw_strand) const
{
    
    // std::set<unsigned> set_overlap_fw_genes;
    // std::set<unsigned> set_overlap_rv_genes;
    
    vector<std::pair<unsigned,unsigned> > overlap_fw_genes;
    vector<std::pair<unsigned,unsigned> > overlap_rv_genes;
    
    //overlapping genes on forward strand
    if(fw_genes.find(contig_id)!=fw_genes.end())
        get_all_overlap(contig_id, fw_genes, alg_range, read_exons, overlap_fw_genes);
        //get_all_overlap(fw_genes.at(contig_id), alg_range, overlap_fw_genes);
    
    //overlapping genes on reverse strand
    if(rv_genes.find(contig_id)!=rv_genes.end())
        get_all_overlap(contig_id, rv_genes, alg_range, read_exons, overlap_rv_genes);
        //get_all_overlap(rv_genes.at(contig_id), alg_range, overlap_rv_genes);
    
    if(overlap_fw_genes.empty() && overlap_rv_genes.empty()) //novel gene
        return -2;
    else if( (overlap_fw_genes.empty() && !overlap_rv_genes.empty()) ||  //overlap gene(s) on only one strand
            (!overlap_fw_genes.empty() && overlap_rv_genes.empty()) )
    {
        //determine case - which strand non-empty
        vector<std::pair<unsigned,unsigned> >* overlap_genes;
        vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >* genes;
        if(overlap_rv_genes.empty())
        {
            //return if unique
            if(overlap_fw_genes.size()==1) {
                //cerr << "overlap on fw unique" << endl;
                return overlap_fw_genes.begin()->first;
            }
            
            overlap_genes = &overlap_fw_genes;
        } else
        {
            assert(overlap_fw_genes.empty());
            //return if unique
            if(overlap_rv_genes.size()==1) {
                //cerr << "overlap on rv unique" << endl;
                return overlap_rv_genes.begin()->first;
            }
            
            overlap_genes = &overlap_rv_genes;
        }
        
        return resolve_overlap_ambiguity(*overlap_genes);  //largest overlap must be > Parameters::ambig_res_thr * second largest overlap
    }
    else if( !overlap_fw_genes.empty() && !overlap_rv_genes.empty() ) //overlap gene(s) on both strands
    {
        int resolved_fw_gene = resolve_overlap_ambiguity(overlap_fw_genes);
        int resolved_rv_gene = resolve_overlap_ambiguity(overlap_rv_genes);
        
        if(resolved_fw_gene == -1 && resolved_rv_gene == -1)
            return -1;
        else if(resolved_fw_gene != -1 && resolved_rv_gene != -1)
        {
            int max_overlap_fw = 0;
            vector<std::pair<unsigned,unsigned> >::const_iterator it = overlap_fw_genes.begin();
            for(; it!=overlap_fw_genes.end(); ++it) {
                if(it->first==resolved_fw_gene)
                {
                    max_overlap_fw = it->second;
                    break;
                }
            }
            assert(max_overlap_fw > 0);
            
            int max_overlap_rv = 0;
            for(it=overlap_rv_genes.begin(); it!=overlap_rv_genes.end(); ++it) {
                if(it->first==resolved_rv_gene)
                {
                    max_overlap_rv = it->second;
                    break;
                }
            }
            assert(max_overlap_rv > 0);
            
            
            if (orig_fw_strand) {
                if (max_overlap_fw >= Parameters::min_overlap_same_strand * max_overlap_rv) {
                    return resolved_fw_gene;
                }
                if (max_overlap_rv > Parameters::min_overlap_diff_strand * max_overlap_fw) {
                    return resolved_rv_gene;
                }
            } else {
                if (max_overlap_rv >= Parameters::min_overlap_same_strand * max_overlap_fw) {
                    return resolved_rv_gene;
                }
                if (max_overlap_fw > Parameters::min_overlap_diff_strand * max_overlap_rv) {
                    return resolved_fw_gene;
                }
            }
            
            return -1;
/*
            if(max_overlap_fw > 1.5 * max_overlap_rv) {
                //cerr << "resolved both strands by overlap" << endl;
                return resolved_fw_gene;
            }
            else if(max_overlap_rv > 1.5 * max_overlap_fw) {
                //cerr << "resolved both strands by overlap" << endl;
                return resolved_rv_gene;
            }
            else
                return -1;
*/
        } else //resolved gene on only one strand, still ambiguous on other
        {
            vector<std::pair<unsigned,unsigned> >* overlap_res_genes;
            vector<std::pair<unsigned,unsigned> >* overlap_notres_genes;
            int resolved_gene;
            bool orig_res_strand;
            if(resolved_fw_gene != -1)
            {
                assert(resolved_rv_gene == -1);
                overlap_res_genes = &overlap_fw_genes;
                resolved_gene = resolved_fw_gene;
                overlap_notres_genes = &overlap_rv_genes;
                orig_res_strand = orig_fw_strand;
            } else if(resolved_rv_gene != -1)
            {
                assert(resolved_fw_gene == -1);
                overlap_res_genes = &overlap_rv_genes;
                resolved_gene = resolved_rv_gene;
                overlap_notres_genes = &overlap_fw_genes;
                orig_res_strand = !orig_fw_strand;
            }
            
            int max_overlap_res = 0;
            vector<std::pair<unsigned,unsigned> >::const_iterator it = overlap_res_genes->begin();
            for(; it!=overlap_res_genes->end(); ++it) {
                if(it->first==resolved_gene)
                {
                    max_overlap_res = it->second;
                    break;
                }
            }
            assert(max_overlap_res > 0);
            
            int max_overlap_notres = 0;   
            for(it=overlap_notres_genes->begin(); it!=overlap_notres_genes->end(); ++it) {
                if(it->second > max_overlap_notres)	
                    max_overlap_notres = it->second;	        
            }
            assert(max_overlap_notres > 0);
            
            if (orig_res_strand && max_overlap_res > Parameters::min_overlap_same_strand * max_overlap_notres) {
                return resolved_gene;
            } else if(!orig_res_strand && max_overlap_res > Parameters::min_overlap_diff_strand * max_overlap_notres) {
                return resolved_gene;
            }
            else
                return -1;
                
/*
            if(max_overlap_res > 1.5 * max_overlap_notres)
                return resolved_gene;
            else
                return -1;    
*/
        }
    } else
        assert(false); //should have covered all cases
}	

bool NovelTrans::pair_pair_order(const pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> >& i,
                                 const pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> >& j)
{
    return i.first < j.first;
}

void NovelTrans::mergeIntervals(vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >& input_intervals,
                                vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > >& merged_intervals,
                                int nof_bams) const
{
    std::sort(input_intervals.begin(), input_intervals.end(), pair_pair_order);
    std::pair<unsigned,unsigned> current(0,0);
    vector<unsigned>* nof_sup_reads = new vector<unsigned>(nof_bams,0);
    vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >::const_iterator it_r = input_intervals.begin();
    for(; it_r != input_intervals.end(); ++it_r)
    {
        //first interval defines cluster boundaries
        if (current.first == 0 and current.second == 0) {
            current.first = it_r->first.first;
            current.second = it_r->first.second;
            nof_sup_reads->at(it_r->second.first) = it_r->second.second;
        }
        //interval extends cluster to the right
        else if (it_r->first.first < current.second && it_r->first.second > current.second) {
            current.second = it_r->first.second;
            nof_sup_reads->at(it_r->second.first) += it_r->second.second;
        }
        //interval contained in cluster
        else if (it_r->first.first < current.second)
            nof_sup_reads->at(it_r->second.first) += it_r->second.second;
        //interval lies outside of cluster
        else {
            merged_intervals.push_back(std::make_pair(current,nof_sup_reads));
            current.first = it_r->first.first;
            current.second = it_r->first.second;
            nof_sup_reads = new vector<unsigned>(nof_bams,0);
            nof_sup_reads->at(it_r->second.first) = it_r->second.second;
        }
    }
    merged_intervals.push_back(std::make_pair(current,nof_sup_reads));
}

void NovelTrans::reads2isoforms(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t,isoforms_t> > >& isoforms_per_gene,
                                std::map<unsigned, std::map<unsigned, std::pair<isoforms_t,isoforms_t> > >& intret_chain_per_gene,
                                bool flonly,
                                std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* isoform_reads_per_gene) const
{
    const std::map<string, unsigned>& contigName2Id = gtf_reader.getContigIds();
    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > > fw_genes;
    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > > rv_genes;
    gtf_reader.getGenes(fw_genes, rv_genes);
    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > > fw_introns;
    std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > > rv_introns;
    gtf_reader.get_introns(fw_introns, rv_introns);
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_donor;
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_donor;
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > fw_acc;
    std::map<unsigned, vector<std::pair<unsigned,unsigned> > > rv_acc;
    gtf_reader.get_splice_sites(fw_donor, rv_donor, true);
    gtf_reader.get_splice_sites(fw_acc, rv_acc, false);
    
    int nof_reads_with_introns = 0;
    int nof_ccsfl_reads_with_introns = 0;
    vector<int> nof_introns_histo(5,0);
    vector<int> nof_ccsfl_introns_histo(5,0);
    int nof_reads_without_introns = 0;
    int nof_ccsfl_reads_without_introns = 0;
    int ambiguous_intron = 0;
    int no_match_intron = 0;
    int assigned_by_intron = 0;
    int ambiguous_overlap = 0;
    int novel_gene = 0;
    int assigned_by_overlap = 0;

    std::map<unsigned, isoforms_t> novel_fw_isoforms_w_introns;
    std::map<unsigned, isoform_reads_t> novel_fw_isoform_reads_w_introns;
    std::map<unsigned, isoforms_t> novel_rv_isoforms_w_introns;
    std::map<unsigned, isoform_reads_t> novel_rv_isoform_reads_w_introns;
    std::map<unsigned, vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > > novel_fw_genes_no_introns;
    std::map<unsigned, vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > > novel_rv_genes_no_introns;
    std::map<unsigned, std::map<unsigned, std::pair<vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >,vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > > > > interval_per_gene;
    //std::map<unsigned, isoforms_t> novel_fw_readthr;
    //std::map<unsigned, isoforms_t> novel_rv_readthr;
    std::map<unsigned, readthroughs_t> novel_fw_readthr;
    std::map<unsigned, readthroughs_t> novel_rv_readthr;
    
    std::ofstream ambig_reads;
    ambig_reads.open("ambiguous.txt");
    
    std::ofstream cert_file;
    if(isoform_reads_per_gene != NULL)
        cert_file.open(Parameters::cert_file);
    
    std::streambuf* buf;
    std::ofstream iso_file;
    if(Parameters::iso_file != "") {
        iso_file.open(Parameters::iso_file);
        buf = iso_file.rdbuf();
    } else
    {
        buf = std::cout.rdbuf();
    }
    std::ostream out(buf);

    //go over all bams
    for(size_t i=0; i<bam_readers.size(); ++i)
    {
        //go through all mapped reads
        while ( bam_readers[i]->hasNext() )
        {
            //ignore read if unmapped
            if(bam_readers[i]->isUnmapped())
            {
                bam_readers[i]->advance();
                continue;
            }
        
            if( bam_readers[i]->isMultiMap() ||
                bam_readers[i]->isTranslocMap() )
            {
                bam_readers[i]->advance();
                continue;
            }

            size_t readlength = bam_readers[i]->getReadLength();
            size_t nof_aln = bam_readers[i]->getNofAlnBases();
            size_t nof_id_aln = bam_readers[i]->getNofIdentAlnBases(reference_sequences);
            float maplength = float(nof_aln)/float(readlength);
            float mapqual = float(nof_id_aln)/ float(nof_aln);
            if(maplength < Parameters::maplength || mapqual < Parameters::mapqual) {
                bam_readers[i]->advance();
                continue;
            }

            //extract number of FL reads from read id
            string readname = bam_readers[i]->getReadName();
            size_t cluster_start = readname.find_first_of("/");
            size_t cluster_end = readname.find_last_of("/");
            string clusterinfo = readname.substr(cluster_start+1,cluster_end-cluster_start-1);
            std::stringstream ss;
            std::stringstream ss_nonfl;
            unsigned nof_fl;
            unsigned nof_nonfl = 0;
            if(clusterinfo.front()!='f') {                
                ss << clusterinfo;  
            } else {                
                size_t fl_end = clusterinfo.find_first_of("p");
                ss << clusterinfo.substr(1,fl_end-1);
                ss_nonfl << clusterinfo.substr(fl_end+1);
                ss_nonfl >> nof_nonfl;
            }            
            ss >> nof_fl;

            if(!flonly)
                nof_fl += nof_nonfl;
            
            //determine chromosome/strand/gene
            //--------------------------------
            bool orig_fw_strand; //does read originate from forward strand
            if(bam_readers[i]->isReverseStrand())
                orig_fw_strand = false;
            else
                orig_fw_strand = true;
            
            //get contig id to which read maps
            unsigned contig_id;
            std::string refname = bam_readers[i]->getRefName();            
            contig_id = contigName2Id.at(refname);
       
            //assign read to gene if possible
            //-------------------------------
            int gene = 0;
            vector<std::pair<unsigned,unsigned> > read_introns;
            vector<std::pair<unsigned,unsigned> > read_exons;
            bam_readers[i]->getIntrons(read_introns);
            bam_readers[i]->getExons(read_exons);
            std::pair<unsigned,unsigned> alg_range(bam_readers[i]->getFirstAlgRefBase(), bam_readers[i]->getLastAlgRefBase()+1);
            assert(alg_range.second > alg_range.first);
            
            /*if(refname=="I") {
                cerr << "read: " << refname << " " << readname << endl;
                cerr << alg_range.first << " " << alg_range.second << endl;
                vector<std::pair<unsigned,unsigned> >::iterator re_it = read_exons.begin();
                for(; re_it != read_exons.end(); ++re_it)
                    cerr << re_it->first << " " << re_it->second << endl;
            }*/
            

            vector<std::pair<unsigned,unsigned> > read_isoform; //tss, intronchain, tes
            read_isoform.push_back(std::make_pair(alg_range.first,alg_range.first));
            read_isoform.insert(read_isoform.end(), read_introns.begin(), read_introns.end());
            read_isoform.push_back(std::make_pair(alg_range.second, alg_range.second));
            
            //first by looking at the read's introns
            std::set<unsigned> multi_match_genes;
            if(!read_introns.empty()) {
                gene = assign_gene_by_intron(read_introns, alg_range, read_exons, fw_genes, rv_genes, fw_introns, rv_introns, fw_donor, rv_donor, fw_acc, rv_acc, contig_id, multi_match_genes);
                nof_reads_with_introns++;
                nof_ccsfl_reads_with_introns += nof_fl;
                nof_introns_histo.at(std::min(5,(int)read_introns.size())-1)++;
                nof_ccsfl_introns_histo.at(std::min(5,(int)read_introns.size())-1) += nof_fl;
                if(gene == -1)
                    ambiguous_intron++;
                else if(gene == -2)
                    no_match_intron++;
                else
                    assigned_by_intron++;
            }
            if(read_introns.empty() || gene == -2) //-2 means neither intron nor ss matched annotation
            {
                gene = assign_gene_by_overlap(alg_range, read_exons, fw_genes, rv_genes, contig_id, orig_fw_strand);
                if(read_introns.empty()) {
                    nof_reads_without_introns++;
                    nof_ccsfl_reads_without_introns += nof_fl;
                }
                if(gene==-1)
                    ambiguous_overlap++;
                else if(gene==-2)
                    novel_gene++;
                else
                    assigned_by_overlap++;
            }
            
            //ignoring certain types of reads
            if (gene == -6)
            {
                ambig_reads << bam_readers[i]->getReadName() << " matches multiple splice sites. Ignoring read." << endl;
                bam_readers[i]->advance();
                continue;
            }

            if(gene==-2) //store novel genes, matched by intron chain or by overlap
            {
               if(!read_introns.empty())
                {
                    //which chr/strand are we looking at
                    isoforms_t* current_isoforms;
                    isoform_reads_t* current_isoform_reads;
                    if (orig_fw_strand)
                    {
                        //check whether map for chr/strand exists yet and create if necessary
                        if (novel_fw_isoforms_w_introns.find(contig_id) == novel_fw_isoforms_w_introns.end()) {                           
                            novel_fw_isoforms_w_introns[contig_id] = isoforms_t(intronchain_cnt_cmp);
                            if(isoform_reads_per_gene != NULL) {
                                 assert(novel_fw_isoform_reads_w_introns.find(contig_id) == novel_fw_isoform_reads_w_introns.end());
                                 novel_fw_isoform_reads_w_introns[contig_id] = isoform_reads_t(intronchain_reads_cmp);
                            }
                        }
                        current_isoforms = &(novel_fw_isoforms_w_introns.at(contig_id));
                        if(isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(novel_fw_isoform_reads_w_introns.at(contig_id));
                    }
                    else
                    {
                        //check whether map for chr/strand exists yet and create if necessary
                        if (novel_rv_isoforms_w_introns.find(contig_id) == novel_rv_isoforms_w_introns.end()) {                            
                            novel_rv_isoforms_w_introns[contig_id] = isoforms_t(intronchain_cnt_cmp);
                            if(isoform_reads_per_gene != NULL) {
                                assert(novel_rv_isoform_reads_w_introns.find(contig_id) == novel_rv_isoform_reads_w_introns.end());
                                novel_rv_isoform_reads_w_introns[contig_id] = isoform_reads_t(intronchain_reads_cmp);
                            }
                        }
                        current_isoforms = &(novel_rv_isoforms_w_introns.at(contig_id));
                        if(isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(novel_rv_isoform_reads_w_introns.at(contig_id));
                    }

                    //insert intron chain as new isoform or increase counter
                    intronchain_cnt_t iso2insert(read_isoform, NULL);                  
                    isoforms_t::iterator it_iso = current_isoforms->find(iso2insert);
                    intronchain_reads_t isoreads2insert;
                    if(isoform_reads_per_gene != NULL) {
                        isoreads2insert.first = read_isoform;
                        isoreads2insert.second = NULL;
                    }
                    if (it_iso != current_isoforms->end()) {
                        it_iso->second->at(i) = it_iso->second->at(i) + nof_fl;
                        if(isoform_reads_per_gene != NULL) {
                            isoform_reads_t::iterator it_isor = current_isoform_reads->find(isoreads2insert);
                            assert(it_isor != current_isoform_reads->end());
                            it_isor->second->at(i).push_back(readname);
                        }
                    } else {
                        iso2insert.second = new vector<unsigned>(bam_readers.size(),0);
                        iso2insert.second->at(i) = nof_fl;
                        current_isoforms->insert(iso2insert);
                        if(isoform_reads_per_gene != NULL) {
                            isoreads2insert.second = new vector<vector<string> >(bam_readers.size());
                            isoreads2insert.second->at(i).push_back(readname);
                            current_isoform_reads->insert(isoreads2insert);
                        }
                    }
                }
               else
               {
                    if (orig_fw_strand)
                    {
                        if (novel_fw_genes_no_introns.find(contig_id) == novel_fw_genes_no_introns.end()) {
                            novel_fw_genes_no_introns[contig_id];
                        }
                        novel_fw_genes_no_introns.at(contig_id).push_back( std::make_pair(alg_range, std::make_pair(i,nof_fl)) );
                    }
                    else
                    {
                        if (novel_rv_genes_no_introns.find(contig_id) == novel_rv_genes_no_introns.end()) {
                            novel_rv_genes_no_introns[contig_id];
                        }
                        novel_rv_genes_no_introns.at(contig_id).push_back(std::make_pair(alg_range, std::make_pair(i,nof_fl)));
                    }
               }
            }
            //store read throughs, matched by intron chain
            else if (gene==-3 || gene==-4)
            {
                assert(!read_introns.empty());
                readthroughs_t* current_isoforms;
                if(gene==-3)
                {
                    if (novel_fw_readthr.find(contig_id) == novel_fw_readthr.end()) {
                        novel_fw_readthr[contig_id] = readthroughs_t(readthrough_cmp);
                    }
                    current_isoforms = &(novel_fw_readthr.at(contig_id));
                }
                else if(gene==-4)
                {
                    if (novel_rv_readthr.find(contig_id) == novel_rv_readthr.end()) {
                        novel_rv_readthr[contig_id] = readthroughs_t(readthrough_cmp);
                    }
                    current_isoforms = &(novel_rv_readthr.at(contig_id));
                }
                
                intronchain_cnt_t iso2insert(read_isoform, NULL);
                std::set<unsigned> rt_genes;
                readthroughs_t::iterator it_iso = current_isoforms->find(make_pair(iso2insert, rt_genes));
                if (it_iso != current_isoforms->end()) {
                    assert(it_iso->second ==  multi_match_genes);
                    it_iso->first.second->at(i) = it_iso->first.second->at(i) + nof_fl;
                } else {
                    iso2insert.second = new vector<unsigned>(bam_readers.size(),0);
                    iso2insert.second->at(i) = nof_fl;
                    current_isoforms->insert(std::make_pair(iso2insert, multi_match_genes));
                }
            }            
            //generate output if read could not be assigned to gene
            else if (gene<0)
            {
                string chr;
                if(refname.substr(0,3) == "chr")
                    chr = refname;
                else
                    chr = "chr"+refname;                
                
                ambig_reads << endl << chr << "\t" << alg_range.first << "\t" << bam_readers[i]->getReadName();
                if(orig_fw_strand)
                    ambig_reads << " (+)";
                else
                    ambig_reads << " (-)";
                
                ambig_reads << " [batch " << batch->at(i) << ", sample " << sample->at(i) << "]\t";
                
                if(gene == -1) {
                    ambig_reads << " ambiguous\t";
                    ambig_reads << nof_fl;
                }
                
                else if(gene == -5) //matches introns of multiple genes
                {
                    assert(multi_match_genes.size()>1);
                    
                    std::set<unsigned>::const_iterator it_gene;
                    for(it_gene = multi_match_genes.begin(); it_gene!=multi_match_genes.end(); ++it_gene) {
                        ambig_reads << gtf_reader.getGeneName(contig_id, *it_gene);
                        if(gtf_reader.is_gene_on_rv(contig_id, *it_gene))
                            ambig_reads << " (-)";
                        else
                            ambig_reads << " (+)";
                        if(next(it_gene,1) != multi_match_genes.end())
                            ambig_reads << ", ";
                    }
                    ambig_reads << endl << "-------------------------------------------------------------------------" << endl;
                    ambig_reads << "fused RNA molecules." << endl;
                }
            }       
            //if read has successfully been assigned to gene, cluster by intron chain
            else if(gene >= 0)
            {                
                vector<std::pair<unsigned,unsigned> > read_good_introns;
                bool is_forward_read = true; //true for isoseq reads

                assert(reference_sequences != 0);
                vector<std::pair<unsigned, unsigned> > gene_introns;
                gtf_reader.get_introns(gene_introns, contig_id, gene);
                bool intron_not_rescued = bam_readers[i]->getGoodIntrons(read_good_introns, reference_sequences, is_forward_read, Parameters::ss_window, Parameters::ss_snap_window, gene_introns);
                
                //ignore read if unmapped
                if(intron_not_rescued)
                {
                    bam_readers[i]->advance();
                    continue;
                }
                
                //cerr << "number of introns: " << read_introns.size() << endl;
                //cerr << "number of good introns " << read_good_introns.size() << endl;
                
                //cluster single exon reads by sequence of intron retentions
                if (read_good_introns.empty())
                {                                    
                    //get spanned gene introns
                    vector<std::pair<unsigned,unsigned> > spanned_introns;
                    spanned_introns.push_back(std::make_pair(alg_range.first,alg_range.first));
                    vector<std::pair<unsigned,unsigned> >::const_iterator it_gi = gene_introns.begin();
                    for (; it_gi != gene_introns.end(); ++it_gi) {
                        if (alg_range.first<it_gi->first && it_gi->second<alg_range.second) {
                            spanned_introns.push_back(*it_gi);
                        }
                    }
                    spanned_introns.push_back(std::make_pair(alg_range.second, alg_range.second));
                    
                    //singe exon reads are treated as intervals; intervals need to be merged into clusters
                    //before creating a corresponding isoforms
                    if (spanned_introns.size()==2) {
                        //check whether interval map for chr and gene exists yet and create if necessary
                        if(interval_per_gene.find(contig_id) == interval_per_gene.end()) {
                            interval_per_gene[contig_id];
                        }
                        assert(interval_per_gene.find(contig_id) != interval_per_gene.end());
                        if (interval_per_gene.at(contig_id).find(gene) == interval_per_gene.at(contig_id).end()) {
                            interval_per_gene.at(contig_id)[gene];
                        }
                        assert(interval_per_gene.at(contig_id).find(gene) != interval_per_gene.at(contig_id).end());
                        
                        //sense or antisense
                        vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >* current_intervals;
                        if (orig_fw_strand == gtf_reader.is_gene_on_rv(contig_id, gene)) {
                            //cerr << "antisense" << endl;
                            current_intervals = &(interval_per_gene.at(contig_id).at(gene).second); //antisense isoforms
                        }
                        else {
                            //cerr << "sense" << endl;
                            current_intervals = &(interval_per_gene.at(contig_id).at(gene).first);
                        }
                        current_intervals->push_back( std::make_pair(alg_range, std::make_pair(i,nof_fl)) );
                    } else
                    {
                        //if (spanned_introns.size()>2)
                        //{
                        //check whether map for chr or gene exist yet and create if necessary
                        if (intret_chain_per_gene.find(contig_id) == intret_chain_per_gene.end()) {
                            intret_chain_per_gene[contig_id];
                        }
                        assert(intret_chain_per_gene.find(contig_id) != intret_chain_per_gene.end());
                        if (intret_chain_per_gene.at(contig_id).find(gene) == intret_chain_per_gene.at(contig_id).end()) {
                            intret_chain_per_gene.at(contig_id)[gene] = std::make_pair(isoforms_t(intronchain_cnt_cmp), isoforms_t(intronchain_cnt_cmp));
                        }
                        assert(intret_chain_per_gene.at(contig_id).find(gene) != intret_chain_per_gene.at(contig_id).end());
                        
                        //insert intron chain as new isoform or increase counter
                        isoforms_t* current_chains;
                        if (orig_fw_strand == gtf_reader.is_gene_on_rv(contig_id, gene)) {
                            //cerr << "antisense" << endl;
                            current_chains = &(intret_chain_per_gene.at(contig_id).at(gene).second); //antisense isoforms
                        }
                        else {
                            //cerr << "sense" << endl;
                            current_chains = &(intret_chain_per_gene.at(contig_id).at(gene).first);
                        }
                        
                        intronchain_cnt_t chain2insert(spanned_introns, NULL);
                        isoforms_t::iterator it_chain = current_chains->find(chain2insert);
                        if (it_chain != current_chains->end()) {
                            it_chain->second->at(i) = it_chain->second->at(i) + nof_fl;
                        } else {
                            chain2insert.second = new vector<unsigned>(bam_readers.size(),0);
                            chain2insert.second->at(i) = nof_fl;
                            current_chains->insert(chain2insert);
                        }
                    }
                    //}
                } else //read has good introns
                {
                    vector<std::pair<unsigned,unsigned> > read_good_isoform; //tss, intronchain, tes
                    read_good_isoform.push_back(std::make_pair(alg_range.first,alg_range.first));
                    read_good_isoform.insert(read_good_isoform.end(), read_good_introns.begin(), read_good_introns.end());
                    read_good_isoform.push_back(std::make_pair(alg_range.second, alg_range.second));
                    //cerr << alg_range.first << " " << read_good_introns.begin()->first << " " << read_good_introns.begin()->second << " " << alg_range.second << endl;
                    
                    /*if(refname=="I") {
                        cerr << "read good intron: " << endl;
                        cerr << alg_range.first << " " << alg_range.second << endl;
                        vector<std::pair<unsigned,unsigned> >::iterator re_it = read_good_introns.begin();
                        for(; re_it != read_good_introns.end(); ++re_it)
                            cerr << re_it->first << " " << re_it->second << endl;
                    }*/
                    
                    //check whether map for chr or gene exist yet and create if necessary
                    if (isoforms_per_gene.find(contig_id) == isoforms_per_gene.end()) {                        
                        isoforms_per_gene[contig_id];
                        if (isoform_reads_per_gene != NULL) {
                            assert(isoform_reads_per_gene->find(contig_id) == isoform_reads_per_gene->end());
                            (*isoform_reads_per_gene)[contig_id];
                        }
                    }
                    assert(isoforms_per_gene.find(contig_id) != isoforms_per_gene.end());
                    assert(isoform_reads_per_gene == NULL || isoform_reads_per_gene->find(contig_id) != isoform_reads_per_gene->end());
                    if (isoforms_per_gene.at(contig_id).find(gene) == isoforms_per_gene.at(contig_id).end()) {                        
                        isoforms_per_gene.at(contig_id)[gene] = std::make_pair(isoforms_t(intronchain_cnt_cmp), isoforms_t(intronchain_cnt_cmp));
                        if (isoform_reads_per_gene != NULL) {
                            assert(isoform_reads_per_gene->at(contig_id).find(gene) == isoform_reads_per_gene->at(contig_id).end());
                            isoform_reads_per_gene->at(contig_id)[gene] = std::make_pair(isoform_reads_t(intronchain_reads_cmp), isoform_reads_t(intronchain_reads_cmp));
                        }
                    }
                    assert(isoforms_per_gene.at(contig_id).find(gene) != isoforms_per_gene.at(contig_id).end());
                    assert(isoform_reads_per_gene == NULL || isoform_reads_per_gene->at(contig_id).find(gene) != isoform_reads_per_gene->at(contig_id).end());
                    
                    //insert intron chain as new isoform or increase counter
                    isoforms_t* current_isoforms;
                    isoform_reads_t* current_isoform_reads;
                    if (orig_fw_strand == gtf_reader.is_gene_on_rv(contig_id, gene)) {
                        current_isoforms = &(isoforms_per_gene.at(contig_id).at(gene).second); //antisense isoforms
                        if (isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(isoform_reads_per_gene->at(contig_id).at(gene).second);
                    }
                    else {
                        current_isoforms = &(isoforms_per_gene.at(contig_id).at(gene).first);
                        if (isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(isoform_reads_per_gene->at(contig_id).at(gene).first);
                    }
                    
                    intronchain_cnt_t iso2insert(read_good_isoform, NULL);
                    intronchain_reads_t isoreads2insert;
                    if (isoform_reads_per_gene != NULL) {
                        isoreads2insert.first = read_good_isoform;
                        isoreads2insert.second = NULL;
                    }
                    isoforms_t::iterator it_iso = current_isoforms->find(iso2insert);
                    if (it_iso != current_isoforms->end()) {                       
                        it_iso->second->at(i) = it_iso->second->at(i) + nof_fl;
                        if (isoform_reads_per_gene != NULL) {
                            isoform_reads_t::iterator it_isor = current_isoform_reads->find(isoreads2insert);
                            assert(it_isor != current_isoform_reads->end());
                            it_isor->second->at(i).push_back(readname);
                        }
                    } else {
                        iso2insert.second = new vector<unsigned>(bam_readers.size(),0);
                        iso2insert.second->at(i) = nof_fl;
                        current_isoforms->insert(iso2insert);
                        if (isoform_reads_per_gene != NULL) {
                            isoreads2insert.second = new vector<vector<string> >(bam_readers.size());
                            isoreads2insert.second->at(i).push_back(readname);
                            current_isoform_reads->insert(isoreads2insert);
                        }
                    }
                }
            }
            
            bam_readers[i]->advance();
        }
    }
    

    //cluster single exon reads for all genes
    std::map<unsigned, std::map<unsigned, std::pair<vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >,vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > > > >::iterator it_ctg = interval_per_gene.begin();
    std::map<unsigned, std::pair<vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > >,vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > > >::iterator it_gene;
    for (; it_ctg != interval_per_gene.end(); ++it_ctg)
    {
        it_gene = it_ctg->second.begin();
        for (; it_gene != it_ctg->second.end(); ++it_gene)
        {
            for (int sense = 0; sense<2; ++sense)
            {
                //create sense isoforms from merged sense/antisense intervals
                vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > > merged_se_iso;
                if (sense==0 && !it_gene->second.first.empty())
                    mergeIntervals(it_gene->second.first, merged_se_iso, bam_readers.size());
                else if (!it_gene->second.second.empty())
                    mergeIntervals(it_gene->second.second, merged_se_iso, bam_readers.size());
                
                vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > >::const_iterator it_mg = merged_se_iso.begin();
                for(; it_mg != merged_se_iso.end(); ++it_mg)
                {
                    //check whether map for chr or gene exist yet and create if necessary
                    if (intret_chain_per_gene.find(it_ctg->first) == intret_chain_per_gene.end()) {
                        intret_chain_per_gene[it_ctg->first];
                    }
                    assert(intret_chain_per_gene.find(it_ctg->first) != intret_chain_per_gene.end());
                    if (intret_chain_per_gene.at(it_ctg->first).find(it_gene->first) == intret_chain_per_gene.at(it_ctg->first).end()) {
                        intret_chain_per_gene.at(it_ctg->first)[it_gene->first] = std::make_pair(isoforms_t(intronchain_cnt_cmp), isoforms_t(intronchain_cnt_cmp));
                    }
                    assert(intret_chain_per_gene.at(it_ctg->first).find(it_gene->first) != intret_chain_per_gene.at(it_ctg->first).end());
                    
                    //insert intron chain as new isoform or increase counter
                    isoforms_t* current_chains;
                    if (sense==0) {
                        current_chains = &(intret_chain_per_gene.at(it_ctg->first).at(it_gene->first).first);
                    } else
                        current_chains = &(intret_chain_per_gene.at(it_ctg->first).at(it_gene->first).second); //antisense isoforms
                    
                    
                    vector<std::pair<unsigned,unsigned> > spanned_introns;
                    spanned_introns.push_back(std::make_pair(it_mg->first.first, it_mg->first.first));
                    spanned_introns.push_back(std::make_pair(it_mg->first.second, it_mg->first.second));
                    
                    intronchain_cnt_t chain2insert(spanned_introns, NULL);
                    isoforms_t::iterator it_chain = current_chains->find(chain2insert);
                    if (it_chain != current_chains->end()) {
                        for (int i=0; i<bam_readers.size(); ++i) {
                            it_chain->second->at(i) = it_chain->second->at(i) + it_mg->second->at(i);
                        }
                    } else {
                        chain2insert.second = it_mg->second;
                        current_chains->insert(chain2insert);
                    }
                    
                }
            }
        }
    }

    
/*
     //debugging isoform grouping of reads
    std::map<unsigned, std::map<unsigned, isoforms_t> >::iterator it_ctg = isoforms_per_gene.begin();
    for (; it_ctg != isoforms_per_gene.end(); ++it_ctg) {
        std::map<unsigned, isoforms_t>::iterator it_gene = it_ctg->second.begin();
        for (; it_gene != it_ctg->second.end(); ++it_gene) {
            isoforms_t::iterator it_iso = it_gene->second.begin();
            for (; it_iso != it_gene->second.end(); ++it_iso) {
                cout << it_ctg->first << " " << it_gene->first << " " << it_iso->second << endl;
            }
        }
    }
*/
    
    print_header(out);
   
    std::ofstream protseq_file;
    unsigned prot_nr = 1;
    protseq_file.open(Parameters::protseq_file);
    
    std::ofstream novel_rna_file;
    novel_rna_file.open(Parameters::novel_rna_file);

    //unify and output novel genes without introns
    int ng_cnt = 1;
    std::map<unsigned, vector<pair<pair<unsigned,unsigned>, pair<unsigned,unsigned> > > >::iterator it_nr = novel_fw_genes_no_introns.begin();
    for(; it_nr != novel_fw_genes_no_introns.end(); ++it_nr)
    {
        //get reference sequence for translation
        string chr = gtf_reader.getContigNames().at(it_nr->first);
        std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
        it_refseq = reference_sequences->find(chr);
        if(it_refseq == reference_sequences->end()) {
            //cerr << "Reference \"" << chr << "\" missing." << endl;
            continue;
        }
        const NamedDnaSequence& ref_sequence = *it_refseq->second;
        
        vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > > merged_genes;
        mergeIntervals(it_nr->second, merged_genes, bam_readers.size());

        //print merged genes on current chromosome
        intronchain_t exons;
        vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > >::const_iterator it_mg = merged_genes.begin();
        for(; it_mg != merged_genes.end(); ++it_mg)
        {
            iso_features isof;
            std::stringstream ss;
            ss << ng_cnt;
            isof.gene_name = "NG_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_nr->first);
            isof.gene_strand = true;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_mg->second;
            exons.clear();
            exons.push_back(it_mg->first);
            isof.exons = exons;

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_nr->first) << "\t";
            //cout << "Novel gene NG_" << ng_cnt << "\t (+)";
            //cout << " (";
            //for (size_t ii=0; ii<it_mg->second->size(); ++ii) {
            //    cout << it_mg->second->at(ii);
            //    if (ii<it_mg->second->size()-1)
            //        cout << ",";
            //}
            //cout << "): " << endl;
            
            vector<std::pair<unsigned,unsigned> > interval_isoform;
            interval_isoform.push_back(std::make_pair(it_mg->first.first, it_mg->first.first));
            interval_isoform.push_back(std::make_pair(it_mg->first.second, it_mg->first.second));
            intronchain_cnt_t iso2insert(interval_isoform, it_mg->second);
            
            isoforms_t novel_ic(intronchain_cnt_cmp);
            novel_ic.clear();
            novel_ic.insert(iso2insert);
            set<unsigned> no_orf;
            no_orf.clear();
            print_max_orf(chr, novel_ic, true, false, ref_sequence, prot_nr, protseq_file, novel_rna_file, no_orf, ng_cnt);
            
            if (no_orf.find(ng_cnt) != no_orf.end()) {
                isof.annot.push_back("no ORF");
            }
            print_iso(isof, out);
            
            ng_cnt++;
        }
    }
    it_nr = novel_rv_genes_no_introns.begin();
    for(; it_nr != novel_rv_genes_no_introns.end(); ++it_nr)
    {
        //get reference sequence for translation
        string chr = gtf_reader.getContigNames().at(it_nr->first);
        std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
        it_refseq = reference_sequences->find(chr);
        if(it_refseq == reference_sequences->end()) {
            //cerr << "Reference \"" << chr << "\" missing." << endl;
            continue;
        }
        const NamedDnaSequence& ref_sequence = *it_refseq->second;
        
        vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > > merged_genes;
        mergeIntervals(it_nr->second, merged_genes, bam_readers.size());

        //print merged genes on current chromosome
        intronchain_t exons;
        vector<pair<pair<unsigned,unsigned>, vector<unsigned>* > >::const_iterator it_mg = merged_genes.begin();
        for(; it_mg != merged_genes.end(); ++it_mg)
        {
            iso_features isof;
            std::stringstream ss;
            ss << ng_cnt;
            isof.gene_name = "NG_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_nr->first);
            isof.gene_strand = false;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_mg->second;
            exons.clear();
            exons.push_back(it_mg->first);
            isof.exons = exons;

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_nr->first) << "\t";
            //cout << "Novel gene NG_" << ng_cnt << "\t (-)";
            //cout << " (";
            //for (size_t ii=0; ii<it_mg->second->size(); ++ii) {
            //    cout << it_mg->second->at(ii);
            //    if (ii<it_mg->second->size()-1)
            //        cout << ",";
            //}
            //cout << "): " << endl;
            
            //print_max_orf(chr, novel_ic, false, false, ref_sequence, prot_nr, protseq_file, no_orf);
            
            vector<std::pair<unsigned,unsigned> > interval_isoform;
            interval_isoform.push_back(std::make_pair(it_mg->first.first, it_mg->first.first));
            interval_isoform.push_back(std::make_pair(it_mg->first.second, it_mg->first.second));
            intronchain_cnt_t iso2insert(interval_isoform, it_mg->second);
            
            isoforms_t novel_ic(intronchain_cnt_cmp);
            novel_ic.clear();
            novel_ic.insert(iso2insert);
            set<unsigned> no_orf;
            no_orf.clear();
            print_max_orf(chr, novel_ic, true, true, ref_sequence, prot_nr, protseq_file, novel_rna_file, no_orf, ng_cnt);
            
            if (no_orf.find(ng_cnt) != no_orf.end()) {
                isof.annot.push_back("no ORF");
            }
            print_iso(isof, out);

            ng_cnt++;
        }
    }

    //output novel genes with introns
    ng_cnt = 1;
    isoforms_t novel_ic(intronchain_cnt_cmp);
    vector<iso_features> isos2print;
    std::map<unsigned,unsigned> restore_cnt0;

    std::map<unsigned, isoforms_t>::const_iterator it_ng = novel_fw_isoforms_w_introns.begin();
    for(; it_ng != novel_fw_isoforms_w_introns.end(); ++it_ng)
    {
        novel_ic.clear();
        isos2print.clear();
        restore_cnt0.clear();

        isoforms_t merged_isoforms(intronchain_cnt_cmp);
        isoform_reads_t merged_isoform_reads(intronchain_reads_cmp);
        if(isoform_reads_per_gene != NULL) {
            assert(novel_fw_isoform_reads_w_introns.find(it_ng->first) != novel_fw_isoform_reads_w_introns.end());
            mergeTssTes(it_ng->second, merged_isoforms, &(novel_fw_isoform_reads_w_introns.at(it_ng->first)), &merged_isoform_reads);
        } else
            mergeTssTes(it_ng->second, merged_isoforms);
        isoforms_t::const_iterator it_iso = merged_isoforms.begin();
        for(; it_iso != merged_isoforms.end(); ++it_iso)
        {
            iso_features isof;
            std::stringstream ss;
            ss << ng_cnt;
            isof.gene_name = "NG_I_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_ng->first);
            isof.gene_strand = true;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_iso->second;

            intronchain_t exon_chain;
            get_ic_exons(it_iso->first, exon_chain);

            isof.exons = exon_chain;

            if(isoform_reads_per_gene != NULL) {
                intronchain_reads_t isoreadskey(it_iso->first, NULL);
                isoform_reads_t::iterator it_isor = merged_isoform_reads.find(isoreadskey);
                assert(it_isor != merged_isoform_reads.end());
                isof.sup_reads = it_isor->second;
            }

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_ng->first) << "\t";
            //cout << "Novel gene NG_I_" << ng_cnt << "\t (+)";
            
            //cout << " (";
            //for (size_t ii=0; ii<bam_readers.size(); ++ii) {
            //    cout << it_iso->second->at(ii);
            //    if (ii<bam_readers.size()-1)
            //        cout << ",";
            //}
            //cout << "): " << endl;

            isos2print.push_back(isof);
            //print_iso(isof);
            assert(restore_cnt0.find(ng_cnt)==restore_cnt0.end());
            restore_cnt0[ng_cnt] = it_iso->second->front();
            it_iso->second->front() = ng_cnt; //dirty! this field used to have different meaning (that's why it has to be printed before)
            novel_ic.insert(*it_iso);
            
            ng_cnt++;
        }

        //translate isoforms
        set<unsigned> no_orf;
        no_orf.clear();
        if (!novel_ic.empty())
        {
            //get reference sequence
            string chr = gtf_reader.getContigNames().at(it_ng->first);
            std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
            it_refseq = reference_sequences->find(chr);
            if(it_refseq == reference_sequences->end()) {
                //cerr << "Reference \"" << chr << "\" missing." << endl;
                continue;
            }
            const NamedDnaSequence& ref_sequence = *it_refseq->second;

            print_max_orf(chr, novel_ic, false, false, ref_sequence, prot_nr, protseq_file, novel_rna_file, no_orf);
        } //if !novel_ic.empty()

        //print isoforms and potentially indicate translation status
        vector<iso_features>::iterator it_i2p = isos2print.begin();
        for (; it_i2p != isos2print.end(); ++it_i2p) {
            unsigned iso_nr = it_i2p->nof_sup->front();
            assert(restore_cnt0.find(iso_nr)!=restore_cnt0.end());
            it_i2p->nof_sup->front() = restore_cnt0.at(iso_nr);
            if (no_orf.find(iso_nr) != no_orf.end()) {
                it_i2p->annot.push_back("no ORF");
            }
            if(isoform_reads_per_gene != NULL)
                print_iso(*it_i2p, out, &cert_file);
            else
                print_iso(*it_i2p, out);
        }
    }

    it_ng = novel_rv_isoforms_w_introns.begin();
    for(; it_ng != novel_rv_isoforms_w_introns.end(); ++it_ng)
    {
        novel_ic.clear();
        isos2print.clear();
        restore_cnt0.clear();

        isoforms_t merged_isoforms(intronchain_cnt_cmp);
        isoform_reads_t merged_isoform_reads(intronchain_reads_cmp);
        if(isoform_reads_per_gene != NULL) {
            assert(novel_rv_isoform_reads_w_introns.find(it_ng->first) != novel_rv_isoform_reads_w_introns.end());
            mergeTssTes(it_ng->second, merged_isoforms, &(novel_rv_isoform_reads_w_introns.at(it_ng->first)), &merged_isoform_reads);
        } else
            mergeTssTes(it_ng->second, merged_isoforms);
        isoforms_t::const_iterator it_iso = merged_isoforms.begin();
        for(; it_iso != merged_isoforms.end(); ++it_iso)
        {
            iso_features isof;
            std::stringstream ss;
            ss << ng_cnt;
            isof.gene_name = "NG_I_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_ng->first);
            isof.gene_strand = false;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_iso->second;

            intronchain_t exon_chain;
            get_ic_exons(it_iso->first, exon_chain);

            isof.exons = exon_chain;

            if(isoform_reads_per_gene != NULL) {
                intronchain_reads_t isoreadskey(it_iso->first, NULL);
                isoform_reads_t::iterator it_isor = merged_isoform_reads.find(isoreadskey);
                assert(it_isor != merged_isoform_reads.end());
                isof.sup_reads = it_isor->second;
            }

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_ng->first) << "\t";
            //cout << "Novel gene NG_I_" << ng_cnt << "\t (-)";
            
            //cout << " (";
            //for (size_t ii=0; ii<bam_readers.size(); ++ii) {
            //    cout << it_iso->second->at(ii);
            //    if (ii<bam_readers.size()-1)
            //        cout << ",";
            //}
            //cout << "): " << endl;            

            isos2print.push_back(isof);
            //print_iso(isof);
            assert(restore_cnt0.find(ng_cnt)==restore_cnt0.end());
            restore_cnt0[ng_cnt] = it_iso->second->front();
            it_iso->second->front() = ng_cnt; //dirty! this field used to have different meaning (that's why it has to be printed before)
            novel_ic.insert(*it_iso);
            
            ng_cnt++;
        }

        //translate isoforms
        set<unsigned> no_orf;
        no_orf.clear();
        if (!novel_ic.empty())
        {
            //get reference sequence
            string chr = gtf_reader.getContigNames().at(it_ng->first);
            std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
            it_refseq = reference_sequences->find(chr);
            if(it_refseq == reference_sequences->end()) {
                //cerr << "Reference \"" << chr << "\" missing." << endl;
                continue;
            }
            const NamedDnaSequence& ref_sequence = *it_refseq->second;

            print_max_orf(chr, novel_ic, false, true, ref_sequence, prot_nr, protseq_file, novel_rna_file, no_orf);
        } //if !novel_ic.empty()

        //print isoforms and potentially indicate translation status
        vector<iso_features>::iterator it_i2p = isos2print.begin();
        for (; it_i2p != isos2print.end(); ++it_i2p) {
            unsigned iso_nr = it_i2p->nof_sup->front();
            assert(restore_cnt0.find(iso_nr)!=restore_cnt0.end());
            it_i2p->nof_sup->front() = restore_cnt0.at(iso_nr);
            if (no_orf.find(iso_nr) != no_orf.end()) {
                it_i2p->annot.push_back("no ORF");
            }
            if(isoform_reads_per_gene != NULL)
                print_iso(*it_i2p, out, &cert_file);
            else
                print_iso(*it_i2p, out);
        }
    }

    //output read throughs
    int rt_cnt = 1;
    std::map<unsigned, readthroughs_t>::const_iterator it_nrt = novel_fw_readthr.begin();
    for(; it_nrt != novel_fw_readthr.end(); ++it_nrt)
    {
        readthroughs_t merged_readthroughs(readthrough_cmp);
        mergeTssTes(it_nrt->second, merged_readthroughs);
        readthroughs_t::const_iterator it_iso = merged_readthroughs.begin();
        for(; it_iso != merged_readthroughs.end(); ++it_iso)
        {
            iso_features isof;
            std::stringstream ss;
            ss << rt_cnt;
            isof.gene_name = "RT_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_nrt->first);
            isof.gene_strand = true;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_iso->first.second;

            intronchain_t exon_chain;
            get_ic_exons(it_iso->first.first, exon_chain);

            isof.exons = exon_chain;
            
            std::set<unsigned>::const_iterator it_g = it_iso->second.begin();
            for (; it_g != it_iso->second.end(); ++it_g) {
                isof.annot.push_back(gtf_reader.getGeneName(it_nrt->first, *it_g));
            }

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_nrt->first) << "\t";
            //cout << "Read through RT_" << rt_cnt << "\t (+)";
            rt_cnt++;
            //cout << " (";
            //for (size_t ii=0; ii<bam_readers.size(); ++ii) {
            //    cout << it_iso->second->at(ii);
            //    if (ii<bam_readers.size()-1)
            //        cout << ",";
            //}
            //cout << "): " << endl;
            print_iso(isof, out);
        }
    }
    it_nrt = novel_rv_readthr.begin();
    for(; it_nrt != novel_rv_readthr.end(); ++it_nrt)
    {
        readthroughs_t merged_readthroughs(readthrough_cmp);
        mergeTssTes(it_nrt->second, merged_readthroughs);
        readthroughs_t::const_iterator it_iso = merged_readthroughs.begin();
        for(; it_iso != merged_readthroughs.end(); ++it_iso)
        {
            iso_features isof;
            std::stringstream ss;
            ss << rt_cnt;
            isof.gene_name = "RT_" + ss.str();
            isof.chr = gtf_reader.getContigNames().at(it_nrt->first);
            isof.gene_strand = false;
            isof.iso_id = "";
            isof.iso_strand = true;
            isof.nof_sup = it_iso->first.second;

            intronchain_t exon_chain;
            get_ic_exons(it_iso->first.first, exon_chain);

            isof.exons = exon_chain;
            
            std::set<unsigned>::const_iterator it_g = it_iso->second.begin();
            for (; it_g != it_iso->second.end(); ++it_g) {
                isof.annot.push_back(gtf_reader.getGeneName(it_nrt->first, *it_g));
            }

            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_nrt->first) << "\t";
            //cout << "Read through RT_" << rt_cnt << "\t (-)";
            rt_cnt++;
            //cout << " (";
            //for (size_t ii=0; ii<bam_readers.size(); ++ii) {
            //    cout << it_iso->second->at(ii);
            //   if (ii<bam_readers.size()-1)
            //      cout << ",";
            //}
            //cout << "): " << endl;
            print_iso(isof, out);
        }
    }

    protseq_file.close();
    if(isoform_reads_per_gene != NULL)
        cert_file.close();

    cerr << "number of isoseq reads with introns: " << nof_reads_with_introns << endl;
    cerr << "number of fl ccs reads with introns: " << nof_ccsfl_reads_with_introns << endl;
    cerr << "number of isoseq reads without introns: " << nof_reads_without_introns << endl;
    cerr << "number of fl ccs reads without introns: " << nof_ccsfl_reads_without_introns << endl;
    cerr << "number of isoseq reads with 1, 2, 3, 4, 5+ introns: ";
    cerr << nof_introns_histo.at(0) << "," << nof_introns_histo.at(1) << "," << nof_introns_histo.at(2) << "," << nof_introns_histo.at(3) << "," << nof_introns_histo.at(4) << endl;
    cerr << "number of fl ccs reads with 1, 2, 3, 4, 5+ introns: ";
    cerr << nof_ccsfl_introns_histo.at(0) << "," << nof_ccsfl_introns_histo.at(1) << "," << nof_ccsfl_introns_histo.at(2) << "," << nof_ccsfl_introns_histo.at(3) << "," << nof_ccsfl_introns_histo.at(4) << endl;
    cerr << "ambiguous introns/ss: " << ambiguous_intron << endl;
    cerr << "no intron/ss matches: " << no_match_intron << endl;
    cerr << "successfully assigned by intron/ss: " << assigned_by_intron << endl;
    cerr << "ambiguous overlap: " << ambiguous_overlap << endl;
    cerr << "successfully assigned by overlap: " << assigned_by_overlap << endl;
    cerr << "novel genes: " << novel_gene << endl;
}


void NovelTrans::print_header(std::ostream& out) const
{
    out << "Gene name";
    out << "\t";
    out << "Contig";
    out << "\t";
    out << "Gene strand";
    out << "\t";
    out << "Isoform ID";
    out << "\t";
    out << "Isoform strand";
    out << "\t";
    out << "Exon start(s)";
    out << "\t";
    out << "Exon end(s)";
    out << "\t";
    for(size_t i=0; i<bam_readers.size(); ++i) {
        out << batch->at(i) << "_" << sample->at(i);
        if(i<bam_readers.size()-1)
            out << ",";
    }
    out << "\t";
    out << "Splicing";
    out << "\t";
    out << "Number of events";
    out << endl;
}


void NovelTrans::print_iso(iso_features& isof, std::ostream& out, std::ofstream* cert_file) const
{
    out << isof.gene_name << "\t";
    out << isof.chr << "\t";
    if(isof.gene_strand)
        out << "+\t";
    else
        out << "-\t";
    out << isof.iso_id << "\t";

    if(isof.iso_id == "")
        out << "\t";
    else {
        if(isof.iso_strand)
            out << "+\t";
        else
            out << "-\t";
    }

    if(!isof.exons.empty())
    {
        intronchain_t::const_iterator it_ex = isof.exons.begin();
        for(; it_ex != isof.exons.end(); ++it_ex) {
            out << it_ex->first;
            if (it_ex+1 != isof.exons.end())
                out << ",";
        }
        out << "\t";
        it_ex = isof.exons.begin();
        for(; it_ex != isof.exons.end(); ++it_ex) {
            out << it_ex->second;
            if (it_ex+1 != isof.exons.end())
                out << ",";
        }
        out << "\t";
    } else
    {
        out << "\t\t";
    }

    for (size_t i=0; i<isof.nof_sup->size(); ++i) {
        out << isof.nof_sup->at(i);
        if (i < isof.nof_sup->size()-1)
            out << ",";
    }
    out << "\t";

    if(isof.iso_id != "" && isof.iso_strand != isof.gene_strand) {
        out << "antisense";
        if(!isof.annot.empty())
            out << ", ";
    }

    for(size_t i=0; i<isof.annot.size(); ++i) {
        out << isof.annot.at(i);
        if(i < isof.annot.size()-1)
            out << ", ";
    }
    out << "\t";

    assert(isof.annot.size() >= isof.nof_annot.size()); //no numbers if no AS but truncated or novel TSS/TES; potentially translation status without numbers
    for(size_t i=0; i<isof.nof_annot.size(); ++i) {
        out << isof.nof_annot.at(i);
        if(i < isof.nof_annot.size()-1)
            out << ", ";
    }
    out << endl;

    if(cert_file != NULL)
    {
        if(isof.iso_id != "")
            *cert_file << isof.iso_id << ": ";
        else
            *cert_file << isof.gene_name << ": ";
        for (size_t i=0; i<isof.nof_sup->size(); ++i)
        {
            vector<string>::iterator it_r = isof.sup_reads->at(i).begin();
            for(; it_r != isof.sup_reads->at(i).end(); ++it_r) {
                *cert_file << *it_r;
                if(it_r + 1 != isof.sup_reads->at(i).end()) //was not last one
                    *cert_file << ",";
            }
            if(i < isof.nof_sup->size()-1)
                *cert_file << " - ";
        }
        *cert_file << endl;
    }

    //delete isof.nof_sup;
}


//return all half-open intervals between provided (half-open) intervals
void NovelTrans::get_ic_exons(const intronchain_t& ic, intronchain_t& exon_chain) const
{
    exon_chain.clear();
    if(ic.size()<2)
        return;

    intronchain_t::const_iterator it = ic.begin();
    unsigned prev_intron_end = it->second;
    for(++it; it!=ic.end(); ++it)
    {
        exon_chain.push_back(std::make_pair(prev_intron_end, it->first));
        prev_intron_end = it->second;
    }
}


void NovelTrans::cluster_by_ic(const readthroughs_t& isoforms, vector<vector<pair<intronchain_cnt_t, set<unsigned> > > >& iso_clusters) const
{
    readthroughs_t::iterator it = isoforms.begin();
    for(; it!=isoforms.end(); ++it)
    {
        bool new_cluster = true;
        vector<vector<pair<intronchain_cnt_t, set<unsigned> > > >::iterator it_cluster = iso_clusters.begin();
        for(; it_cluster!=iso_clusters.end(); ++it_cluster)
        {
            if(ic_equal(it_cluster->front().first,it->first)) {
                assert(it_cluster->front().second == it->second);
                new_cluster = false;
                it_cluster->push_back(*it);
                break;
            }
        }
        if(new_cluster) {
            vector<pair<intronchain_cnt_t, set<unsigned> > > new_cluster;
            new_cluster.push_back(*it);
            iso_clusters.push_back(new_cluster);
        }
    }
}


//first clusters by intron chain, then merges tss/tes in 50 bp steps
//annotated tss/tes ignored
void NovelTrans::mergeTssTes(const readthroughs_t& isoforms,
                             readthroughs_t& merged_isoforms) const
{
    vector<unsigned> tss, tes;
    vector<vector<pair<intronchain_cnt_t, set<unsigned> > > >iso_clusters;
    cluster_by_ic(isoforms, iso_clusters);

//    //debugging output
//    cerr << "before: " << endl;
//    readthroughs_t::iterator it = isoforms.begin();
//    for(; it!=isoforms.end(); ++it) {
//        for(size_t x=0; x<it->first.first.size(); ++x) {
//            cerr << it->first.first.at(x).first << "-" << it->first.first.at(x).second << " ";
//        }
//        cerr << endl;
//    }

//    cerr << endl << "after: " << endl;
//    vector<vector<pair<intronchain_cnt_t, set<unsigned> > > >::iterator it_clust = iso_clusters.begin();
//    for(; it_clust!=iso_clusters.end(); ++it_clust) {
//        cerr << "next" << endl;
//        vector<pair<intronchain_cnt_t, set<unsigned> > >::iterator it_c = it_clust->begin();
//        for(; it_c!=it_clust->end(); ++it_c)
//        {
//            for(size_t x=0; x<it_c->first.first.size(); ++x) {
//                cerr << it_c->first.first.at(x).first << "-" << it_c->first.first.at(x).second << " ";
//            }
//            cerr << endl;
//        }
//    }

    vector<vector<pair<intronchain_cnt_t, set<unsigned> > > >::iterator it_clusters = iso_clusters.begin();
    for(; it_clusters!=iso_clusters.end(); ++it_clusters)
    {
        tss.clear();
        tes.clear();

        vector<pair<intronchain_cnt_t, set<unsigned> > >::iterator it_iso = it_clusters->begin();
        for(; it_iso!=it_clusters->end(); ++it_iso)
        {
            tss.push_back(it_iso->first.first.front().first);
            tes.push_back(it_iso->first.first.back().first);
        }
        assert(tss.size()==tes.size());

        if(tss.empty())
            return;

        sort(tss.begin(), tss.end());
        sort(tes.begin(), tes.end());

        std::map<unsigned, unsigned> newtss; //maps tss to leftmost tss in same group
        std::map<unsigned, unsigned> newtes; //maps tes to rightmost tes in same group

        //compute newtss mapping
        //cerr << "compute tss mapping" << endl;
        unsigned prev = tss.front();
        unsigned leftmost = prev;
        newtss[leftmost] = leftmost;  //leftmost maps to itself
        vector<unsigned>::const_iterator it_tss = tss.begin()+1;
        for (; it_tss!=tss.end(); ++it_tss) {
            if (*it_tss <= prev + 50) {
                newtss[*it_tss]=leftmost;
            } else
            {
                leftmost = *it_tss;
                newtss[leftmost]=leftmost;
            }
            prev = *it_tss;
        }

        //compute newtes mapping
        //cerr << "compute tes mapping" << endl;
        prev = tes.back();
        unsigned rightmost = prev;
        newtes[rightmost] = rightmost;  //leftmost maps to itself
        vector<unsigned>::const_reverse_iterator it_tes = tes.rbegin()+1;
        for (; it_tes!=tes.rend(); ++it_tes) {
            if (*it_tes >= prev - 50) {
                newtes[*it_tes]=rightmost;
            } else
            {
                rightmost = *it_tes;
                newtes[rightmost]=rightmost;
            }
            prev = *it_tes;
        }

        //cerr << "filling..." << endl;
        it_iso = it_clusters->begin();
        for(; it_iso != it_clusters->end(); ++it_iso)
        {
            unsigned old_tss = it_iso->first.first.front().first;
            unsigned snapped_tss = newtss.at(old_tss);
            unsigned old_tes = it_iso->first.first.back().first;
            unsigned snapped_tes = newtes.at(old_tes);

            if(old_tss < snapped_tss) { //check for conflict with intron (or tes if no intron)
                if ( it_iso->first.first.at(1).first <= snapped_tss ) { //new tss must be left oft first element (intron or tes)
                    snapped_tss = old_tss;
                }
            }
            if(old_tes > snapped_tes) { //check for conflict with intron (or tss if no intron)
                if ( (it_iso->first.first.end()-2)->second >= snapped_tes) { //new tss must be left oft first element (intron or tes)
                    snapped_tes = old_tes;
                }
            }

            intronchain_t chain2insert;      
            chain2insert.push_back(std::make_pair(snapped_tss,snapped_tss));
            chain2insert.insert(chain2insert.end(), it_iso->first.first.begin()+1, it_iso->first.first.end()-1);
            chain2insert.push_back(std::make_pair(snapped_tes,snapped_tes));

            intronchain_cnt_t iso2insert(chain2insert, NULL);
            std::set<unsigned> rt_genes;
            readthroughs_t::iterator it = merged_isoforms.find(make_pair(iso2insert, rt_genes));
            if(it != merged_isoforms.end())
            {
                assert(it->second == it_iso->second);
                for(size_t i = 0; i<it_iso->first.second->size(); ++i) {
                    it->first.second->at(i) = it->first.second->at(i) + it_iso->first.second->at(i);
                }
            } else {
                iso2insert.second = new vector<unsigned>(it_iso->first.second->size(),0);
                for(size_t i = 0; i<it_iso->first.second->size(); ++i)
                    iso2insert.second->at(i) = it_iso->first.second->at(i);
                merged_isoforms.insert(std::make_pair(iso2insert,it_iso->second));
            }
        }
    }


//    //debug output
//    cerr << endl << "old transcripts: " << endl;

//    readthroughs_t::iterator it_iso = isoforms.begin();
//    for(; it_iso != isoforms.end(); ++it_iso)
//    {
//        for(size_t i = 0; i<it_iso->first.first.size(); ++i)
//            cerr << it_iso->first.first.at(i).first << "-" << it_iso->first.first.at(i).second << ", ";
//        cerr << endl;
//        for(size_t i = 0; i<it_iso->first.second->size(); ++i)
//            cerr << it_iso->first.second->at(i) << ", ";
//        cerr << endl;
//    }

//    //sense could have been empty
//    cerr << "new transcripts: " << endl;
//    it_iso = merged_isoforms.begin();
//    for(; it_iso != merged_isoforms.end(); ++it_iso)
//    {
//        for(size_t i = 0; i<it_iso->first.first.size(); ++i)
//            cerr << it_iso->first.first.at(i).first << "-" << it_iso->first.first.at(i).second << ", ";
//        cerr << endl;
//        for(size_t i = 0; i<it_iso->first.second->size(); ++i)
//            cerr << it_iso->first.second->at(i) << ", ";
//        cerr << endl;
//    }
}

//void NovelTrans::snap2tsstes(const isoforms_t& isoforms, unsigned tss, unsigned tes,
//                             isoforms_t& snapped_isoforms) const
//{
//    isoforms_t::iterator it_iso = isoforms.begin();
//    for(; it_iso != isoforms.end(); ++it_iso)
//    {
//        bool snap_tss = false;
//        if ( (it_iso->first.front().first < tss && it_iso->first.front().first+50 >= tss) || //left of tss
//             (it_iso->first.front().first > tss && tss+50 >= it_iso->first.front().first) )  //right of tss
//        {
//            snap_tss = true;
//        }
//        bool snap_tes = false;
//        if ( (it_iso->first.back().first > tes && tes+50 >= it_iso->first.back().first) || //right of tes
//             (it_iso->first.back().first < tes && it_iso->first.back().first+50 >= tes ) )  //left of tes
//        {
//            snap_tes = true;
//        }
//        if (snap_tss && snap_tes)
//        {
//            intronchain_t chain2insert;
//            chain2insert.push_back(std::make_pair(tss,tss));
//            chain2insert.insert(chain2insert.end(), it_iso->first.begin()+1, it_iso->first.end()-1);
//            chain2insert.push_back(std::make_pair(tes,tes));
//            
//            intronchain_cnt_t iso2insert(chain2insert, NULL);
//            isoforms_t::iterator it = snapped_isoforms.find(iso2insert);
//            if(it != snapped_isoforms.end())
//            {
//                for(size_t i = 0; i<it_iso->second->size(); ++i) {
//                    it->second->at(i) = it->second->at(i) + it_iso->second->at(i);
//                }
//            } else {
//                iso2insert.second = new vector<unsigned>(it_iso->second->size(),0);
//                for(size_t i = 0; i<it_iso->second->size(); ++i)
//                    iso2insert.second->at(i) = it_iso->second->at(i);
//                snapped_isoforms.insert(iso2insert);
//            }
//   
//        } else
//            snapped_isoforms.insert(*it_iso);
//    }
//}


//first clusters by intron chain, then merges tss/tes in 50 bp steps
//annotated tss/tes ignored
void NovelTrans::mergeTssTes(const isoforms_t& isoforms,
                             isoforms_t& merged_isoforms,
                             const isoform_reads_t* isoform_reads,
                             isoform_reads_t* merged_isoform_reads) const
{
    vector<unsigned> tss, tes;
    vector<vector<intronchain_cnt_t> > iso_clusters;
    vector<vector<intronchain_reads_t> > isoread_clusters;
    if(isoform_reads != NULL)
        cluster_by_ic(isoforms, iso_clusters, isoform_reads, &isoread_clusters);
    else
        cluster_by_ic(isoforms, iso_clusters);

/*
    //debugging output
    cerr << "before: " << endl;
    isoforms_t::iterator it = isoforms.begin();
    for(; it!=isoforms.end(); ++it) {
        for(size_t x=0; x<it->first.size(); ++x) {
            cerr << it->first.at(x).first << "-" << it->first.at(x).second << " ";
        }
        cerr << endl;
    }

    cerr << endl << "after: " << endl;
    vector<vector<intronchain_cnt_t> >::iterator it_dclusters = iso_clusters.begin();
    for(; it_dclusters!=iso_clusters.end(); ++it_dclusters) {
        cerr << "next" << endl;
        vector<intronchain_cnt_t>::iterator it_c = it_dclusters->begin();
        for(; it_c!=it_dclusters->end(); ++it_c)
        {
            for(size_t x=0; x<it_c->first.size(); ++x) {
                cerr << it_c->first.at(x).first << "-" << it_c->first.at(x).second << " ";
            }
            cerr << endl;
        }
    }
*/

    vector<vector<intronchain_cnt_t> >::iterator it_clusters = iso_clusters.begin();
    //it_clusters = iso_clusters.begin();
    for(; it_clusters!=iso_clusters.end(); ++it_clusters)
    {
        tss.clear();
        tes.clear();

        vector<intronchain_cnt_t>::iterator it_iso = it_clusters->begin();
        for(; it_iso!=it_clusters->end(); ++it_iso)
        {
            tss.push_back(it_iso->first.front().first);
            tes.push_back(it_iso->first.back().first);
        }
        assert(tss.size()==tes.size());

        if(tss.empty())
            return;

        sort(tss.begin(), tss.end());
        sort(tes.begin(), tes.end());

        std::map<unsigned, unsigned> newtss; //maps tss to leftmost tss in same group
        std::map<unsigned, unsigned> newtes; //maps tes to rightmost tes in same group

        //compute newtss mapping
        //cerr << "compute tss mapping" << endl;
        unsigned prev = tss.front();
        unsigned leftmost = prev;
        newtss[leftmost] = leftmost;  //leftmost maps to itself
        vector<unsigned>::const_iterator it_tss = tss.begin()+1;
        for (; it_tss!=tss.end(); ++it_tss) {
            if (*it_tss <= prev + 50) {
                newtss[*it_tss]=leftmost;
            } else
            {
                leftmost = *it_tss;
                newtss[leftmost]=leftmost;
            }
            prev = *it_tss;
        }

        //compute newtes mapping
        //cerr << "compute tes mapping" << endl;
        prev = tes.back();
        unsigned rightmost = prev;
        newtes[rightmost] = rightmost;  //rightmost maps to itself
        vector<unsigned>::const_reverse_iterator it_tes = tes.rbegin()+1;
        for (; it_tes!=tes.rend(); ++it_tes) {
            if (*it_tes + 50 >= prev) {
                newtes[*it_tes]=rightmost;
            } else
            {
                rightmost = *it_tes;
                newtes[rightmost]=rightmost;
            }
            prev = *it_tes;
        }

        //cerr << "filling..." << endl;
        it_iso = it_clusters->begin();
        for(; it_iso != it_clusters->end(); ++it_iso)
        {
            unsigned old_tss = it_iso->first.front().first;
            unsigned snapped_tss = newtss.at(old_tss);
            unsigned old_tes = it_iso->first.back().first;
            unsigned snapped_tes = newtes.at(old_tes);

            if(old_tss < snapped_tss) { //check for conflict with intron (or tes if no intron)
                if ( it_iso->first.at(1).first <= snapped_tss ) { //new tss must be left oft first element (intron or tes)
                    snapped_tss = old_tss;
                }
            }
            if(old_tes > snapped_tes) { //check for conflict with intron (or tss if no intron)
                if ( (it_iso->first.end()-2)->second >= snapped_tes) { //new tss must be left oft first element (intron or tes)
                    snapped_tes = old_tes;
                }
            }

            intronchain_t chain2insert;
            chain2insert.push_back(std::make_pair(snapped_tss,snapped_tss));
            chain2insert.insert(chain2insert.end(), it_iso->first.begin()+1, it_iso->first.end()-1);
            chain2insert.push_back(std::make_pair(snapped_tes,snapped_tes));

            intronchain_cnt_t iso2insert(chain2insert, NULL);           
            isoforms_t::iterator it = merged_isoforms.find(iso2insert);
            if(it != merged_isoforms.end())
            {                
                for(size_t i = 0; i<it_iso->second->size(); ++i) {
                    it->second->at(i) = it->second->at(i) + it_iso->second->at(i);
                }
                if(isoform_reads != NULL)
                {
                    unsigned cluster_pos = it_clusters - iso_clusters.begin();
                    unsigned iso_pos = it_iso - it_clusters->begin();
                    intronchain_reads_t isoreads2insert(chain2insert, NULL);
                    isoform_reads_t::iterator it_r = merged_isoform_reads->find(isoreads2insert);
                    assert(it_r != merged_isoform_reads->end());
                    for(size_t i = 0; i<it_iso->second->size(); ++i) {
                        vector<string>& reads_i = it_r->second->at(i);
                        vector<string> current_i = isoread_clusters.at(cluster_pos).at(iso_pos).second->at(i);
                        reads_i.insert(reads_i.end(), current_i.begin(), current_i.end());
                    }
                }
            } else {
                iso2insert.second = new vector<unsigned>(it_iso->second->size(),0);
                for(size_t i = 0; i<it_iso->second->size(); ++i)
                    iso2insert.second->at(i) = it_iso->second->at(i);
                merged_isoforms.insert(iso2insert);

                if(isoform_reads != NULL)
                {
                    unsigned cluster_pos = it_clusters - iso_clusters.begin();
                    unsigned iso_pos = it_iso - it_clusters->begin();
                    intronchain_reads_t isoreads2insert(chain2insert, NULL);
                    isoreads2insert.second = new vector<vector<string> >(it_iso->second->size());
                    for(size_t i = 0; i<it_iso->second->size(); ++i)
                    {
                        isoreads2insert.second->at(i) = isoread_clusters.at(cluster_pos).at(iso_pos).second->at(i);
                    }
                    merged_isoform_reads->insert(isoreads2insert);
                }
            }
        }
    }

//    //debug output
//    cerr << endl << "old transcripts: " << endl;
//    isoforms_t::iterator it_iso = isoforms.begin();
//    for(; it_iso != isoforms.end(); ++it_iso)
//    {
//        for(size_t i = 0; i<it_iso->first.size(); ++i)
//            cerr << it_iso->first.at(i).first << "-" << it_iso->first.at(i).second << ", ";
//        cerr << endl;
//        for(size_t i = 0; i<it_iso->second->size(); ++i)
//            cerr << it_iso->second->at(i) << ", ";
//        cerr << endl;
//    }

//    //sense could have been empty
//    if (!merged_isoforms.empty())
//    {
//        cerr << "new transcripts: " << endl;
//        it_iso = merged_isoforms.begin();
//        for (; it_iso != merged_isoforms.end() ; ++it_iso)
//        {
//            for(size_t i = 0; i<it_iso->first.size(); ++i)
//                cerr << it_iso->first.at(i).first << "-" << it_iso->first.at(i).second << ", ";
//            cerr << endl;
//            for(size_t i = 0; i<it_iso->second->size(); ++i)
//                cerr << it_iso->second->at(i) << ", ";
//            cerr << endl;
//        }
//    }

}

void NovelTrans::cluster_by_ic(const isoforms_t& isoforms, vector<vector<intronchain_cnt_t> >& iso_clusters,
                               const isoform_reads_t* isoform_reads, vector<vector<intronchain_reads_t> >* isoread_clusters) const
{
    isoforms_t::iterator it = isoforms.begin();
    for(; it!=isoforms.end(); ++it)
    {
        bool new_cluster = true;
        if(it->first.size()>2) { //single exon reads are not changed (assume clustered before)
            vector<vector<intronchain_cnt_t> >::iterator it_cluster = iso_clusters.begin();
            for(; it_cluster!=iso_clusters.end(); ++it_cluster)
            {
                if(ic_equal(it_cluster->front(),*it)) {
                    new_cluster = false;
                    it_cluster->push_back(*it);

                    //for keeping track of reads assigned to isoforms
                    if(isoform_reads != NULL) {
                        intronchain_reads_t isoreadskey(it->first, NULL);
                        isoform_reads_t::iterator it_isor = isoform_reads->find(isoreadskey);
                        assert(it_isor != isoform_reads->end());
                        unsigned pos = it_cluster - iso_clusters.begin();
                        assert(isoread_clusters->size()>pos);
                        isoread_clusters->at(pos).push_back(*it_isor);
                    }
                    break;
                }
            }
        }
        if(new_cluster) {
            vector<intronchain_cnt_t> new_cluster;
            new_cluster.push_back(*it);
            iso_clusters.push_back(new_cluster);

            //for keeping track of reads assigned to isoforms
            if(isoform_reads != NULL)
            {
                intronchain_reads_t isoreadskey(it->first, NULL);
                isoform_reads_t::iterator it_isor = isoform_reads->find(isoreadskey);
                assert(it_isor != isoform_reads->end());
                vector<intronchain_reads_t> new_isoread_cluster;
                new_isoread_cluster.push_back(*it_isor);
                isoread_clusters->push_back(new_isoread_cluster);
            }
        }
    }
}

//first clusters by intron chain
//then snaps isoform start and end sites to annotated tss/tes (within 50bp), but does not jump over annotated intron
//then merges remaining start/end sites in 50 bp steps
void NovelTrans::mergeTssTes(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& isoforms_per_gene,
                             std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& merged_isoforms_per_gene,
                             int snap_dist, int window_size,
                             std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* isoform_reads_per_gene,
                             std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t,isoform_reads_t> > >* merged_isoform_reads_per_gene) const
{
    vector<unsigned> tss, tes;
    std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >::iterator it_ctg = isoforms_per_gene.begin();
    for (; it_ctg != isoforms_per_gene.end(); ++it_ctg)
    {
        std::map<unsigned, std::pair<isoforms_t, isoforms_t> >::iterator it_gene = it_ctg->second.begin();
        for (; it_gene != it_ctg->second.end(); ++it_gene)
        {
            unsigned ctg = it_ctg->first;
            unsigned gene = it_gene->first;
            
            //for now assume only on transcript annotated per gene
            unsigned gene_tss = gtf_reader.getTss(ctg, gene);
            unsigned gene_tes = gtf_reader.getTes(ctg, gene);

            //cerr << "YYY " << ctg << " " << gene_tss << " " << gene_tes << endl;
            
            for (int sense = 0; sense<2; ++sense)
            {
                vector<vector<intronchain_cnt_t> > iso_clusters;
                vector<vector<intronchain_reads_t> > isoread_clusters;
                if (sense==0) {
                    if(isoform_reads_per_gene != NULL)
                        cluster_by_ic(it_gene->second.first, iso_clusters, &(isoform_reads_per_gene->at(ctg).at(gene).first), &isoread_clusters);
                    else
                        cluster_by_ic(it_gene->second.first, iso_clusters);
                }
                else {
                    if(isoform_reads_per_gene != NULL)
                        cluster_by_ic(it_gene->second.second, iso_clusters, &(isoform_reads_per_gene->at(ctg).at(gene).second), &isoread_clusters);
                    else
                        cluster_by_ic(it_gene->second.second, iso_clusters);
                }

                /*
                //debugging output
                isoforms_t* isos;
                cerr << "before: (" << ctg << "," << gene << ")" << endl;
                if (sense==0) {
                    isos = &(it_gene->second.first);
                } else
                    isos = &(it_gene->second.second);
                isoforms_t::iterator it = isos->begin();
                for(; it!=isos->end(); ++it) {
                    for(size_t x=0; x<it->first.size(); ++x) {
                        cerr << it->first.at(x).first << "-" << it->first.at(x).second << " ";
                    }
                    cerr << endl;
                }

                cerr << endl << "after: " << endl;
                vector<vector<intronchain_cnt_t> >::iterator it_clusterss = iso_clusters.begin();
                for(; it_clusterss!=iso_clusters.end(); ++it_clusterss) {
                    cerr << "next" << endl;
                    vector<intronchain_cnt_t>::iterator it_c = it_clusterss->begin();
                    for(; it_c!=it_clusterss->end(); ++it_c)
                    {
                        for(size_t x=0; x<it_c->first.size(); ++x) {
                            cerr << it_c->first.at(x).first << "-" << it_c->first.at(x).second << " ";
                        }
                        cerr << endl;
                    }
                }
                */

                vector<vector<intronchain_cnt_t> >::iterator it_clusters = iso_clusters.begin();
                //it_clusters = iso_clusters.begin();
                for(; it_clusters!=iso_clusters.end(); ++it_clusters)
                {
                    tss.clear();
                    tes.clear();
                    
                    vector<intronchain_cnt_t>::iterator it_iso = it_clusters->begin();
                    for(; it_iso!=it_clusters->end(); ++it_iso)
                    {
                        tss.push_back(it_iso->first.front().first);
                        tes.push_back(it_iso->first.back().first);
                    }
                    assert(tss.size()==tes.size());
                    
                    if(tss.empty())
                        continue;
                    
                    sort(tss.begin(), tss.end());
                    sort(tes.begin(), tes.end());
                    
                    std::map<unsigned, unsigned> newtss; //maps tss to annotated tss or leftmost tss in same group
                    std::map<unsigned, unsigned> newtes; //maps tes to annotated tes or rightmost tes in same group
                    
                    //compute newtss mapping
                    //cerr << "compute tss mapping" << endl;

                    //first snap all start sites to closeby annotated tss
                    vector<unsigned>::const_iterator it_tss;
                    if(snap_dist >= 0)
                    {
                        it_tss = tss.begin();
                        for (; it_tss!=tss.end(); ++it_tss) {
                            if ( std::max(*it_tss,gene_tss) - std::min(*it_tss,gene_tss) <= snap_dist ) {
                                newtss[*it_tss]=gene_tss;
                                //cerr << "snapped " << *it_tss << " to " << gene_tss << endl;
                            }
                        }
                    }

                    //now start from first start site not yet assigned
                    it_tss = tss.begin();
                    while (it_tss!=tss.end() && newtss.find(*it_tss)!=newtss.end()) {
                        it_tss++;
                    }
                    if (it_tss!=tss.end())
                    {
                        unsigned prev = *it_tss;
                        unsigned leftmost = prev;
                        newtss[leftmost] = leftmost;  //leftmost maps to itself
                        it_tss++;
                        for (; it_tss!=tss.end(); ++it_tss)
                        {
                            //skip start sites already assigned to annotated tss
                            if (newtss.find(*it_tss)!=newtss.end()) {
                                continue;
                            }
                            if(window_size < 0)
                                newtss[*it_tss]=leftmost;
                            else {
                                if (*it_tss <= prev + window_size) {
                                    newtss[*it_tss]=leftmost;
                                } else
                                {
                                    leftmost = *it_tss;
                                    newtss[leftmost]=leftmost;
                                }
                            }
                            prev = *it_tss;
                        }
                    }
                    
                    //compute newtes mapping
                    //cerr << "compute tes mapping" << endl;
                    
                    //first snap all end sites to closeby annotated tes
                    vector<unsigned>::const_reverse_iterator it_tes;
                    if(snap_dist >= 0)
                    {
                        it_tes = tes.rbegin();
                        for (; it_tes!=tes.rend(); ++it_tes) {
                            if ( std::max(*it_tes,gene_tes+1) - std::min(*it_tes,gene_tes+1) <= snap_dist ) {
                                newtes[*it_tes]=gene_tes+1; //gene_tes is inclusive
                                //cerr << "snapped " << *it_tes << " to " << gene_tes+1 << endl;
                            }
                        }
                    }
                    //now start from first end site (from right) not yet assigned
                    it_tes = tes.rbegin();
                    while (it_tes!=tes.rend() && newtes.find(*it_tes)!=newtes.end()) {
                        it_tes++;
                    }
                    if (it_tes!=tes.rend())
                    {
                        unsigned prev = *it_tes;
                        unsigned rightmost = prev;
                        newtes[rightmost] = rightmost;  //leftmost maps to itself
                        it_tes++;
                        for (; it_tes!=tes.rend(); ++it_tes)
                        {
                            //skip end sites already assigned to annotated tes
                            if (newtes.find(*it_tes)!=newtes.end()) {
                                continue;
                            }
                            if(window_size < 0)
                                newtes[*it_tes]=rightmost;
                            else {
                                if (*it_tes + window_size >= prev) {
                                    newtes[*it_tes]=rightmost;
                                } else
                                {
                                    rightmost = *it_tes;
                                    newtes[rightmost]=rightmost;
                                }
                            }
                            prev = *it_tes;
                        }
                    }
                    
                    //now fill isoforms with new tss/tes
                    if (merged_isoforms_per_gene.find(ctg) == merged_isoforms_per_gene.end()) {
                        merged_isoforms_per_gene[ctg];
                        if(isoform_reads_per_gene != NULL) {
                            assert(merged_isoform_reads_per_gene->find(ctg) == merged_isoform_reads_per_gene->end());
                            (*merged_isoform_reads_per_gene)[ctg];
                        }
                    }
                    if (merged_isoforms_per_gene.at(ctg).find(gene) == merged_isoforms_per_gene.at(ctg).end()) { //gene entry could exists because of sense/antisense
                        merged_isoforms_per_gene.at(ctg)[gene] = std::make_pair(isoforms_t(intronchain_cnt_cmp), isoforms_t(intronchain_cnt_cmp));
                        if(isoform_reads_per_gene != NULL) {
                            assert(merged_isoform_reads_per_gene->at(ctg).find(gene) == merged_isoform_reads_per_gene->at(ctg).end());
                            merged_isoform_reads_per_gene->at(ctg)[gene] = std::make_pair(isoform_reads_t(intronchain_reads_cmp), isoform_reads_t(intronchain_reads_cmp));
                        }
                    }
                    
                    isoforms_t* current_isoforms;
                    isoform_reads_t* current_isoform_reads;
                    if (sense == 0) {
                        current_isoforms = &(merged_isoforms_per_gene.at(ctg).at(gene).first);
                        if(isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(merged_isoform_reads_per_gene->at(ctg).at(gene).first);
                    }
                    else if(sense == 1) {
                        current_isoforms = &(merged_isoforms_per_gene.at(ctg).at(gene).second);
                        if(isoform_reads_per_gene != NULL)
                            current_isoform_reads = &(merged_isoform_reads_per_gene->at(ctg).at(gene).second);
                    }
                    
                    //cerr << "filling..." << endl;
                    it_iso = it_clusters->begin();
                    for(; it_iso!=it_clusters->end(); ++it_iso)
                    {
                        unsigned old_tss = it_iso->first.front().first;
                        unsigned snapped_tss = newtss.at(old_tss);
                        unsigned old_tes = it_iso->first.back().first;
                        unsigned snapped_tes = newtes.at(old_tes);
                        
                        if(old_tss < snapped_tss) { //check for conflict with intron (or tes if no intron)
                            if ( it_iso->first.at(1).first <= snapped_tss ) { //new tss must be left oft first element (intron or tes)
                                //cerr << "conflict tss: " << ctg << " " << it_iso->first.at(1).first << " " << snapped_tss << " " << old_tss << endl;
                                snapped_tss = old_tss;
                            }
                        }
                        
                        if(old_tes > snapped_tes) { //check for conflict with intron (or tss if no intron)
                            if ( (it_iso->first.end()-2)->second >= snapped_tes) { //new tss must be left oft first element (intron or tes)
                                //cerr << "conflict tes: " << ctg << " " << (it_iso->first.end()-2)->second << " " <<  snapped_tes << " " << old_tes << endl;
                                snapped_tes = old_tes;
                            }
                        }
                        
                        intronchain_t chain2insert;
                        chain2insert.push_back(std::make_pair(snapped_tss,snapped_tss));
                        chain2insert.insert(chain2insert.end(), it_iso->first.begin()+1, it_iso->first.end()-1);
                        chain2insert.push_back(std::make_pair(snapped_tes,snapped_tes));
                        
                        intronchain_cnt_t iso2insert(chain2insert, NULL);
                        isoforms_t::iterator it = current_isoforms->find(iso2insert);
                        if(it != current_isoforms->end())
                        {
                            for(size_t i = 0; i<it_iso->second->size(); ++i) {
                                it->second->at(i) = it->second->at(i) + it_iso->second->at(i);
                            }
                            if(isoform_reads_per_gene != NULL)
                            {
                                unsigned cluster_pos = it_clusters - iso_clusters.begin();
                                unsigned iso_pos = it_iso - it_clusters->begin();
                                intronchain_reads_t isoreads2insert(chain2insert, NULL);
                                isoform_reads_t::iterator it_r = current_isoform_reads->find(isoreads2insert);
                                assert(it_r != current_isoform_reads->end());
                                for(size_t i = 0; i<it_iso->second->size(); ++i) {
                                    vector<string>& reads_i = it_r->second->at(i);
                                    vector<string> current_i = isoread_clusters.at(cluster_pos).at(iso_pos).second->at(i);
                                    reads_i.insert(reads_i.end(), current_i.begin(), current_i.end());
                                }
                            }
                        } else {
                            iso2insert.second = new vector<unsigned>(it_iso->second->size(),0);
                            for(size_t i = 0; i<it_iso->second->size(); ++i)
                                iso2insert.second->at(i) = it_iso->second->at(i);
                            current_isoforms->insert(iso2insert);

                            if(isoform_reads_per_gene != NULL)
                            {
                                unsigned cluster_pos = it_clusters - iso_clusters.begin();
                                unsigned iso_pos = it_iso - it_clusters->begin();
                                intronchain_reads_t isoreads2insert(chain2insert, NULL);
                                isoreads2insert.second = new vector<vector<string> >(it_iso->second->size());
                                for(size_t i = 0; i<it_iso->second->size(); ++i)
                                {
                                    isoreads2insert.second->at(i) = isoread_clusters.at(cluster_pos).at(iso_pos).second->at(i);
                                }
                                current_isoform_reads->insert(isoreads2insert);
                            }
                        }
                    }
                }
                
//                //debug output
//                cerr << endl << "gene tss: " << gene_tss << endl;
//                cerr << endl << "gene tes: " << gene_tes << endl;
//                cerr << endl << "old transcripts: " << endl;
//                
//                isoforms_t::iterator it_iso;
//                if (sense==0) {
//                    it_iso = it_gene->second.first.begin();
//                }
//                else {
//                    it_iso = it_gene->second.second.begin();
//                    cerr << "antisense" << endl;
//                }
//                for (; (sense==0 && it_iso != it_gene->second.first.end()) ||
//                     (sense==1 && it_iso != it_gene->second.second.end()) ; ++it_iso)
//                {
//                    for(size_t i = 0; i<it_iso->first.size(); ++i)
//                        cerr << it_iso->first.at(i).first << "-" << it_iso->first.at(i).second << ", ";
//                    cerr << endl;
//                    for(size_t i = 0; i<it_iso->second->size(); ++i)
//                        cerr << it_iso->second->at(i) << ", ";
//                    cerr << endl;
//                }
//                
//                //sense could have been empty
//                if (merged_isoforms_per_gene.find(ctg) != merged_isoforms_per_gene.end() &&
//                    merged_isoforms_per_gene.at(ctg).find(gene) != merged_isoforms_per_gene.at(ctg).end())
//                {
//                    cerr << "new transcripts: " << endl;
//                    if (sense==0) {
//                        it_iso = merged_isoforms_per_gene.at(ctg).at(gene).first.begin();
//                    }
//                    else
//                        it_iso = merged_isoforms_per_gene.at(ctg).at(gene).second.begin();
//                    for (; (sense==0 && it_iso != merged_isoforms_per_gene.at(ctg).at(gene).first.end()) ||
//                         (sense==1 && it_iso != merged_isoforms_per_gene.at(ctg).at(gene).second.end()) ; ++it_iso)
//                    {
//                        for(size_t i = 0; i<it_iso->first.size(); ++i)
//                            cerr << it_iso->first.at(i).first << "-" << it_iso->first.at(i).second << ", ";
//                        cerr << endl;
//                        for(size_t i = 0; i<it_iso->second->size(); ++i)
//                            cerr << it_iso->second->at(i) << ", ";
//                        cerr << endl;
//                    }
//                }
                
            }
        }
    }
}


void NovelTrans::all_events_per_isoform(std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >& isoforms_per_gene,
                                        std::map<unsigned, std::map<unsigned, std::pair<isoforms_t,isoforms_t> > >& intret_chain_per_gene,
                                        std::map<unsigned, std::map<unsigned, std::pair<isoform_reads_t, isoform_reads_t> > >* isoform_reads_per_gene) const
{
    set<string> novel_dimers;
    set<string> anno_dimers;
    std::map<string, unsigned> novel_dimer_occ;
    std::map<string, unsigned> anno_dimer_occ;
    
    //std::multiset<pair<unsigned,unsigned> > novel_fw_introns;
    //std::multiset<pair<unsigned,unsigned> > novel_rv_introns;
    //std::multiset<pair<unsigned,unsigned> > anno_fw_introns;
    //std::multiset<pair<unsigned,unsigned> > anno_rv_introns;
    set<pair<unsigned,unsigned> > novel_fw_introns;
    set<pair<unsigned,unsigned> > novel_rv_introns;
    set<pair<unsigned,unsigned> > anno_fw_introns;
    set<pair<unsigned,unsigned> > anno_rv_introns;
    
    std::ofstream as_rna_file;
    as_rna_file.open(Parameters::as_rna_file);
    std::ofstream protseq_file;
    protseq_file.open(Parameters::protseq_file, std::fstream::out | std::fstream::app);
    std::ofstream hexamer_file;
    if(Parameters::hexamer_file != "")
    {
        hexamer_file.open(Parameters::hexamer_file);
    }
    std::ofstream pwcount_file;
    if(Parameters::pwcount_file != "")
    {
        pwcount_file.open(Parameters::pwcount_file);
    }
    std::ofstream retint_length_file;
    if(Parameters::retint_length_file != "")
    {
        retint_length_file.open(Parameters::retint_length_file);
    }
    std::ofstream cert_file;
    if(isoform_reads_per_gene != NULL)
    cert_file.open(Parameters::cert_file, std::fstream::out | std::fstream::app);
    
    std::streambuf* buf;
    std::ofstream iso_file;
    if(Parameters::iso_file != "") {
        iso_file.open(Parameters::iso_file, std::fstream::out | std::fstream::app);
        buf = iso_file.rdbuf();
    } else
    {
        buf = std::cout.rdbuf();
    }
    std::ostream out(buf);
    
    vector<iso_features> isos2print;
    //isoforms_t intronchain_cluster(intronchain_cnt_cmp);
    isoforms_t novel_ic(intronchain_cnt_cmp);
    isoforms_t anno_ic(intronchain_cnt_cmp);
    std::map<unsigned,unsigned> restore_cnt0;
    unsigned iso_nr = 1;
    unsigned prot_nr = 1;
    std::map<unsigned, std::map<unsigned, std::pair<isoforms_t, isoforms_t> > >::iterator it_ctg = isoforms_per_gene.begin();
    std::map<unsigned, std::pair<isoforms_t, isoforms_t> >::iterator it_gene;
    for (; it_ctg != isoforms_per_gene.end(); ++it_ctg)
    {
        string chr = gtf_reader.getContigNames().at(it_ctg->first);
        
        //needed later to translate into protein
        std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
        it_refseq = reference_sequences->find(chr);
        if(it_refseq == reference_sequences->end()) {
            //cerr << "Reference \"" << chr << "\" missing." << endl;
            continue;
        }
        const NamedDnaSequence& ref_sequence = *it_refseq->second;
        
        novel_fw_introns.clear();
        novel_rv_introns.clear();
        anno_fw_introns.clear();
        anno_rv_introns.clear();
        
        it_gene = it_ctg->second.begin();
        for (; it_gene != it_ctg->second.end(); ++it_gene)
        {
            iso_features isof;
            isof.gene_name = gtf_reader.getGeneName(it_ctg->first, it_gene->first);
            isof.chr = chr;
            
            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_ctg->first) << "\t";
            //cout << gtf_reader.getGeneName(it_ctg->first, it_gene->first);
            if(gtf_reader.is_gene_on_rv(it_ctg->first, it_gene->first)) {
                //cout << " (-)" << endl;
                isof.gene_strand = false;
            }
            else {
                //cout << " (+)" << endl;
                isof.gene_strand = true;
            }
            //cout << "-------------------------------------------------------------------------" << endl;
            
            unsigned gene_tss = gtf_reader.getTss(it_ctg->first, it_gene->first);
            unsigned gene_tes = gtf_reader.getTes(it_ctg->first, it_gene->first);
            
            vector<std::pair<unsigned, unsigned> > gene_exons;
            gtf_reader.get_exons(gene_exons, it_ctg->first, it_gene->first);
            
            vector<std::pair<unsigned, unsigned> > gene_introns;
            gtf_reader.get_introns(gene_introns, it_ctg->first, it_gene->first);
            
            
            //needed for analyzing associated events within isoforms
            vector<unsigned> cnt_skipped(gene_exons.size(), 0); //total number of reads skipping exon (independent of second exon)
            vector<unsigned> cnt_incl(gene_exons.size(), 0);    //total number of reads including exon (independent of second exon)
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_skipped; //exon i skipped and j included
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_incl;    //exon i included and j skipped
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_skipped;  //both exons i and j skipped
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_incl;     //both exons i and j included
            
            vector<unsigned> cnt_retained(gene_introns.size(), 0);  //total number of reads retained intron (independent of second intron)
            vector<unsigned> cnt_spliced(gene_introns.size(), 0);   //total number of reads splicing intron (independent of second intron)
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_retained; //intron i retained and j spliced
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_spliced;  //intron i spliced and j retained
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_retained; //both introns i and j retained
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_spliced;  //both introns i and j spliced
            
            vector<unsigned> cnt_retained_ex(gene_exons.size(), 0);  //total number of reads spanning exon (independent of second exon)
            vector<unsigned> cnt_spliced_ex(gene_exons.size(), 0);   //total number of reads containing novel intron (independent of second exon)
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_retained_ex; //exon i spanned and exon j contains novel intron
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_spliced_ex;  //exon i contains novel intron and exon j spanned
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_retained_ex; //both exons i and j spanned
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_spliced_ex;  //both exons i and j contain novel intron
            std::map<unsigned, std::set<pair<unsigned,unsigned> > > introns_per_exon;
            
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_annot_comb; //exon i spanned and intron j retained
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_i_novel_comb;  //exon i contains novel intron and intron j spliced
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_novel_comb; //exons i contains novel intron and intron j retained
            std::map<std::pair<unsigned, unsigned>, unsigned> cnt_pair_annot_comb;  //exons i spanned intron j spliced
            
            if(Parameters::pwcount_file != "")
            {
                //store all novel introns in exons seen for this gene
                //needed for pairwise counting of introns in exons events
                isoforms_t::iterator it_iso_pre;
                it_iso_pre = it_gene->second.first.begin();
                for(; it_iso_pre != it_gene->second.first.end(); ++it_iso_pre)
                {
                    //get introns in exons
                    unsigned found;
                    std::map<unsigned, set<pair<unsigned,unsigned> > > intinex;
                    found = get_introns_in_exons(it_iso_pre->first, gene_exons, gene_introns, intinex);
                    std::map<unsigned, set<pair<unsigned,unsigned> > >::const_iterator it_intinex = intinex.begin();
                    for(; it_intinex != intinex.end(); ++it_intinex)
                    {
                        set<pair<unsigned,unsigned> >::const_iterator it_int = it_intinex->second.begin();
                        for(; it_int != it_intinex->second.end(); ++it_int)
                            introns_per_exon[it_intinex->first].insert(*it_int);
                    }
                }
            }
            
            for (int sense = 0; sense<2; ++sense)
            {
                //intronchain_cluster.clear();
                isos2print.clear();
                novel_ic.clear();
                anno_ic.clear();
                restore_cnt0.clear();
                isoforms_t::iterator it_iso;
                isoform_reads_t* current_isoform_reads;
                if (sense==0) {
                    it_iso = it_gene->second.first.begin();
                    if (isoform_reads_per_gene != NULL)
                        current_isoform_reads = &(isoform_reads_per_gene->at(it_ctg->first).at(it_gene->first).first);
                }
                else {
                    it_iso = it_gene->second.second.begin();
                    if (isoform_reads_per_gene != NULL)
                        current_isoform_reads = &(isoform_reads_per_gene->at(it_ctg->first).at(it_gene->first).second);
                }
                for (; (sense==0 && it_iso != it_gene->second.first.end()) ||
                     (sense==1 && it_iso != it_gene->second.second.end()) ; ++it_iso, ++iso_nr)
                {
                    //if (iso_nr > 1)
                    //    cout << endl;
                    
                    bool iso_printed = false;
                    std::stringstream ss;
                    ss << iso_nr;
                    isof.iso_id = "Iso_" + ss.str();
                    //cout << "Isoform " << iso_nr;
                    
                    if (sense==1) { //antisense
                        isof.iso_strand = !isof.gene_strand;
                    } else
                        isof.iso_strand = isof.gene_strand;
                    
                    isof.nof_sup = it_iso->second;
                    //cout << " (";
                    //for (size_t ii=0; ii<it_iso->second->size(); ++ii) {
                    //    cout << it_iso->second->at(ii);
                    //    if (ii<it_iso->second->size()-1)
                    //        cout << ",";
                    //}
                    //cout << "): " << endl;
                    
                    /*cout << " [batch " << batch->at(i) << ", sample " << sample->at(i) << "] ";
                     if(orig_fw_strand)
                     cout << " (+)\t";
                     else
                     cout << " (-)\t";
                     */
                    
                    //cout << endl;
                    
                    intronchain_t exon_chain;
                    get_ic_exons(it_iso->first, exon_chain);
                    
                    isof.exons = exon_chain;
                    
                    if(isoform_reads_per_gene != NULL) {
                        intronchain_reads_t isoreadskey(it_iso->first, NULL);
                        isoform_reads_t::iterator it_isor = current_isoform_reads->find(isoreadskey);
                        assert(it_isor != current_isoform_reads->end());
                        isof.sup_reads = it_isor->second;
                    }
                    
                    /*
                     cout << "Read exons: ";
                     vector<std::pair<unsigned,unsigned> >::const_iterator it_rex = exon_chain.begin();
                     for(; it_rex != exon_chain.end(); it_rex++)
                     {
                     cout << "[" << it_rex->first << "," << it_rex->second << "]";
                     if (it_rex+1 != exon_chain.end())
                     cout << " - ";
                     }
                     cout << endl;
                     */
                    
                    /*
                     intronchain_t::const_iterator it_ic = it_iso->first.begin();
                     for(; it_ic != it_iso->first.end(); ++it_ic) {
                     cout << "[" << it_ic->first << "," << it_ic->second << "]";
                     if (it_ic+1 != it_iso->first.end())
                     cout << " - ";
                     }
                     
                     cout << endl;
                     */
                    
                    isof.annot.clear();
                    isof.nof_annot.clear();
                    
                    if (sense==0)
                    {
                        //isof.annot.push_back("relevant");
                        bool any_splicing = false;
                        unsigned found;
                        
                        //keep only intron with canonical splice sites
                        bool gene_on_rv = gtf_reader.is_gene_on_rv(it_ctg->first, it_gene->first);
                        
                        if(Parameters::hexamer_file != "")
                        {
                            //generating and printing consensus vs non-consensus introns
                            vector<std::pair<unsigned, unsigned> > canonical_read_introns;
                            canonical_read_introns.push_back(it_iso->first.front());
                            filter_by_dimers(it_iso->first, canonical_read_introns, ref_sequence, gene_on_rv);
                            canonical_read_introns.push_back(it_iso->first.back());
                            
                            vector<std::pair<unsigned, unsigned> > noncanonical_read_introns;
                            noncanonical_read_introns.push_back(it_iso->first.front());
                            filter_inv_by_dimers(it_iso->first, noncanonical_read_introns, ref_sequence, gene_on_rv);
                            noncanonical_read_introns.push_back(it_iso->first.back());
                            
                            vector<std::pair<unsigned,unsigned> >::const_iterator it_readint;
                            for(it_readint=it_iso->first.begin()+1; it_readint!=it_iso->first.end()-1; ++it_readint)
                            {
                                //is intron novel?
                                if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint))
                                {
                                    vector<unsigned>::const_iterator it = isof.nof_sup->begin();
                                    for(; it!=isof.nof_sup->end(); ++it) {
                                        for(size_t i=0; i<*it; ++i) {
                                            //cerr << "AI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << it_readint->second - it_readint->first << endl;
                                            if(!gene_on_rv)
                                                hexamer_file << "AI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, false) << endl;
                                            else
                                                hexamer_file << "AI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, true) << endl;
                                        }
                                    }
                                    continue;
                                }
                                vector<unsigned>::const_iterator it = isof.nof_sup->begin();
                                for(; it!=isof.nof_sup->end(); ++it) {
                                    for(size_t i=0; i<*it; ++i) {
                                        //cerr << "NI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << it_readint->second - it_readint->first << endl;
                                        if(!gene_on_rv) {
                                            int x = 0;
                                            hexamer_file << "NI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, false) << endl;
                                        } else
                                            hexamer_file << "NI " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, true) << endl;
                                    }
                                }
                            }
                            for(it_readint=canonical_read_introns.begin()+1; it_readint!=canonical_read_introns.end()-1; ++it_readint)
                            {
                                //is intron novel?
                                if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint))
                                    continue;
                                vector<unsigned>::const_iterator it = isof.nof_sup->begin();
                                for(; it!=isof.nof_sup->end(); ++it) {
                                    for(size_t i=0; i<*it; ++i) {
                                        //cerr << "NIC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << it_readint->second - it_readint->first << endl;
                                        if(!gene_on_rv) {
                                            int x = 0;
                                            hexamer_file << "NIC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, false) << endl;
                                        } else
                                            hexamer_file << "NIC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, true) << endl;
                                    }
                                }
                            }
                            for(it_readint=noncanonical_read_introns.begin()+1; it_readint!=noncanonical_read_introns.end()-1; ++it_readint)
                            {
                                //is intron novel?
                                if(std::binary_search(gene_introns.begin(), gene_introns.end(), *it_readint))
                                    continue;
                                string dimer;
                                if(!gene_on_rv)
                                    dimer = get_dimer(*it_readint, ref_sequence, false);
                                else
                                    dimer = get_dimer(*it_readint, ref_sequence, true);
                                vector<unsigned>::const_iterator it = isof.nof_sup->begin();
                                for(; it!=isof.nof_sup->end(); ++it) {
                                    for(size_t i=0; i<*it; ++i) {
                                        //cerr << "NINC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << it_readint->second - it_readint->first << " " << dimer << endl;
                                        if(!gene_on_rv) {
                                            hexamer_file << "NINC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, false) << endl;
                                        } else
                                            hexamer_file << "NINC " << isof.chr << " " << it_readint->first << " " << it_readint->second << " " << get_hexamers(*it_readint, ref_sequence, true) << endl;
                                    }
                                }
                            }
                        }
                        
                        //get exon skippings
                        vector<vector<unsigned> > skipped_exons;
                        found = get_exon_skippings(it_iso->first, gene_exons, gene_introns, skipped_exons);
                        //found = get_exon_skippings(canonical_read_introns, gene_exons, gene_introns);
                        assert(found==skipped_exons.size());
                        if(found>0) {
                            //isof.annot.push_back("exon skipping [" + std::to_string(found) + "]");
                            isof.annot.push_back("exon skipping");
                            isof.nof_annot.push_back(found);
                            
                            /*
                             //output skipped exons for comparison with previous study (Bitton)
                             vector<vector<unsigned> >::const_iterator it = skipped_exons.begin();
                             for(; it != skipped_exons.end(); ++it)
                             {
                             cerr << "skipped exons: ";
                             if(isof.gene_strand = true)
                             {
                             vector<unsigned>::const_iterator it_ex = it->begin();
                             for(; it_ex != it->end(); ++it_ex) {
                             cerr << isof.gene_name << ".1:" << "exon:" << (*it_ex)+1;
                             if(it_ex + 1 != it->end())
                             cerr << ",";
                             }
                             } else {
                             vector<unsigned>::const_reverse_iterator it_ex = it->rbegin();
                             for(; it_ex != it->rend(); ++it_ex) {
                             cerr << isof.gene_name << ".1:" << "exon:" << gene_exons.size()-(*it_ex);
                             if(it_ex + 1 != it->rend())
                             cerr << ",";
                             }
                             }
                             cerr << endl;
                             }*/
                            
                            
                        }
                        any_splicing = any_splicing || found;
                        
                        
                        unsigned tot_sup = 0;
                        if(Parameters::pwcount_file != "")
                        {
                            //collect skipped and retained exons
                            //add up total read support over all time points
                            vector<unsigned>::const_iterator it = isof.nof_sup->begin();
                            for(; it!=isof.nof_sup->end(); ++it)
                                tot_sup += *it;
                            if (isof.exons.size()>=2)
                            {
                                //first put all skipped exons in set
                                set<unsigned> se;
                                vector<vector<unsigned> >::const_iterator it_si = skipped_exons.begin();
                                for(; it_si != skipped_exons.end(); ++it_si)
                                {
                                    vector<unsigned>::const_iterator it_se = it_si->begin();
                                    for(; it_se != it_si->end(); ++it_se)
                                    {
                                        se.insert(*it_se);
                                    }
                                }
                                //count skipped/included (pairs of) exons
                                for(unsigned i=0; i<gene_exons.size(); ++i)
                                {
                                    if (isof.exons.begin()->second >= gene_exons[i].first ||
                                        isof.exons.back().first <= gene_exons[i].second )
                                        continue; //exon i not enclosed by first and last exons of read
                                    if (se.find(i) != se.end())
                                        cnt_skipped[i] += tot_sup;
                                    else
                                        cnt_incl[i] += tot_sup;
                                    for(unsigned j=i+1; j<gene_exons.size(); ++j)
                                    {
                                        if (isof.exons.begin()->second >= gene_exons[j].first ||
                                            isof.exons.back().first <= gene_exons[j].second )
                                            continue; //exon j not enclosed by first and last exons of read
                                        if (se.find(i) != se.end())
                                        {
                                            if (se.find(j) != se.end() ) { //both skipped
                                                if (cnt_pair_skipped.find(std::make_pair(i,j)) != cnt_pair_skipped.end()) {
                                                    cnt_pair_skipped.at(std::make_pair(i,j)) += tot_sup;
                                                } else
                                                    cnt_pair_skipped[std::make_pair(i,j)] = tot_sup;
                                            } else {
                                                if (cnt_i_skipped.find(std::make_pair(i,j)) != cnt_i_skipped.end()) {
                                                    cnt_i_skipped.at(std::make_pair(i,j)) += tot_sup;
                                                } else
                                                    cnt_i_skipped[std::make_pair(i,j)] = tot_sup;
                                            }
                                        } else {
                                            if (se.find(j) != se.end() ) {
                                                if (cnt_i_incl.find(std::make_pair(i,j)) != cnt_i_incl.end()) {
                                                    cnt_i_incl.at(std::make_pair(i,j)) += tot_sup;
                                                } else
                                                    cnt_i_incl[std::make_pair(i,j)] = tot_sup;
                                            } else {
                                                if (cnt_pair_incl.find(std::make_pair(i,j)) != cnt_pair_incl.end()) {
                                                    cnt_pair_incl.at(std::make_pair(i,j)) += tot_sup;
                                                } else
                                                    cnt_pair_incl[std::make_pair(i,j)] = tot_sup;
                                            }
                                        }
                                        
                                    }
                                }
                            }
                        }
                        
                        //get intron retentions
                        vector<vector<unsigned> > retained_introns;
                        found = get_intron_retentions(exon_chain, gene_exons, gene_introns, retained_introns);
                        if(found>0) {
                            //isof.annot.push_back("intron retention [" + std::to_string(found) + "]");
                            isof.annot.push_back("intron retention");
                            isof.nof_annot.push_back(found);
                            
                            if(Parameters::retint_length_file != "")
                            {
                                vector<unsigned>::const_iterator itt = isof.nof_sup->begin();
                                for(; itt!=isof.nof_sup->end(); ++itt) {
                                    for(size_t i=0; i<*itt; ++i) {
                                        //output retained intron lengths to check for bias
                                        vector<vector<unsigned> >::const_iterator it = retained_introns.begin();
                                        for(; it != retained_introns.end(); ++it)
                                        {
                                            retint_length_file << "retained intron: ";
                                            vector<unsigned>::const_iterator it_ex = it->begin();
                                            for(; it_ex != it->end(); ++it_ex) {
                                                retint_length_file << isof.gene_name << ":" << "intron:[" << gene_introns.at(*it_ex).first << "," << gene_introns.at(*it_ex).second << "] - ";
                                                assert(gene_introns.at(*it_ex).second > gene_introns.at(*it_ex).first);
                                                retint_length_file << gene_introns.at(*it_ex).second - gene_introns.at(*it_ex).first << endl;
                                            }
                                        }
                                    }
                                }
                            }
                            
                            
                        }
                        any_splicing = any_splicing || found;
                        
                        
                        set<unsigned> ri;
                        if(Parameters::pwcount_file != "")
                        {
                            //collect spliced and retained introns
                            if (isof.exons.size()>=2) //>=3 would prevent counting of independent retentions/splicings
                            {
                                //first put all retained introns in set
                                vector<vector<unsigned> >::const_iterator it_ri = retained_introns.begin();
                                for(; it_ri != retained_introns.end(); ++it_ri)
                                {
                                    vector<unsigned>::const_iterator it_i = it_ri->begin();
                                    for(; it_i != it_ri->end(); ++it_i)
                                    {
                                        ri.insert(*it_i);
                                    }
                                }
                                count_pw_introns( gene_introns, std::make_pair(isof.exons.begin()->first,isof.exons.back().second), tot_sup, ri, cnt_retained, cnt_spliced,
                                                 cnt_i_retained, cnt_i_spliced, cnt_pair_retained, cnt_pair_spliced );
                            }
                        }
                        
                        //get introns in exons
                        std::map<unsigned, set<pair<unsigned,unsigned> > > intinex;
                        found = get_introns_in_exons(it_iso->first, gene_exons, gene_introns, intinex);
                        //found = get_introns_in_exons(canonical_read_introns, gene_exons, gene_introns);
                        if(found>0) {
                            //isof.annot.push_back("intron in exon [" + std::to_string(found) + "]");
                            isof.annot.push_back("intron in exon");
                            isof.nof_annot.push_back(found);
                        }
                        any_splicing = any_splicing || found;
                        
                        
                        if(Parameters::pwcount_file != "")
                        {
                            //count (pairwise) novel introns in exons
                            //first put all exons with novel introns in set
                            set<unsigned> ie;
                            std::map<unsigned, set<pair<unsigned,unsigned> > >::const_iterator it_intinex = intinex.begin();
                            for(; it_intinex != intinex.end(); ++it_intinex)
                            {
                                ie.insert(it_intinex->first);
                            }
                            count_pw_intinex( gene_exons, exon_chain, introns_per_exon, tot_sup, ie, cnt_retained_ex, cnt_spliced_ex,
                                             cnt_i_retained_ex, cnt_i_spliced_ex, cnt_pair_retained_ex, cnt_pair_spliced_ex );
                            
                            
                            //now let's count combinations of novel introns in exons and intron retentions
                            if (isof.exons.size()>=2) //only combinations are counted, singletons from previous calls
                                count_pw_combi( gene_exons, gene_introns, exon_chain, introns_per_exon, tot_sup, ie, ri,
                                               cnt_i_novel_comb, cnt_i_annot_comb, cnt_pair_novel_comb, cnt_pair_annot_comb  );
                        }
                        
                        
                        //get novel introns
                        vector<unsigned> vfound(4,0);
                        vector<unsigned> gene_donors;
                        gtf_reader.get_splice_sites(gene_donors, true, it_ctg->first, it_gene->first);
                        vector<unsigned> gene_acc;
                        gtf_reader.get_splice_sites(gene_acc, false, it_ctg->first, it_gene->first);
                        //std::multiset<pair<unsigned,unsigned> >* novel_introns; //!!!!! don't forget to uncomment loop in get_novel_introns!!!!
                        //std::multiset<pair<unsigned,unsigned> >* anno_introns;  //!!!!! don't forget to uncomment loop in get_novel_introns!!!!
                        set<pair<unsigned,unsigned> >* novel_introns;
                        set<pair<unsigned,unsigned> >* anno_introns;
                        if(gene_on_rv)
                        {
                            novel_introns = &novel_rv_introns;
                            anno_introns = &anno_rv_introns;
                        } else
                        {
                            novel_introns = &novel_fw_introns;
                            anno_introns = &anno_fw_introns;
                        }
                        get_novel_introns(it_iso->first, it_iso->second, gene_introns, gene_exons, gene_donors, gene_acc, gene_tss, gene_tes, gene_on_rv,
                                          novel_introns, anno_introns, vfound);
                        //get_novel_introns(canonical_read_introns, gene_introns, gene_donors, gene_acc, gene_tss, gene_tes, gene_on_rv, vfound);
                        /*
                         if(vfound.at(0)>0) {
                         //isof.annot.push_back("novel intron structure [" + std::to_string(vfound.at(0)) + "]");
                         isof.annot.push_back("novel intron structure");
                         isof.nof_annot.push_back(vfound.at(0));
                         }
                         */
                        if(vfound.at(1)>0) {
                            //isof.annot.push_back("novel donor [" + std::to_string(vfound.at(1)) + "]");
                            isof.annot.push_back("novel donor");
                            isof.nof_annot.push_back(vfound.at(1));
                        }
                        if(vfound.at(2)>0) {
                            //isof.annot.push_back("novel acceptor [" + std::to_string(vfound.at(2)) + "]");
                            isof.annot.push_back("novel acceptor");
                            isof.nof_annot.push_back(vfound.at(2));
                        }
                        if(vfound.at(3)>0) {
                            //isof.annot.push_back("novel donor/acceptor [" + std::to_string(vfound.at(3)) + "]");
                            isof.annot.push_back("novel donor/acceptor");
                            isof.nof_annot.push_back(vfound.at(3));
                        }
                        any_splicing = any_splicing || vfound.at(1) || vfound.at(2) || vfound.at(3);
                        
                        //get_novel_exons (not included)
                        found = get_novel_exons(exon_chain, gene_exons, gene_introns, false);
                        if(found>0) {
                            //isof.annot.push_back("novel exon [" + std::to_string(found) + "]");
                            isof.annot.push_back("novel exon");
                            isof.nof_annot.push_back(found);
                        }
                        any_splicing = any_splicing || found;
                        
                        //get_novel_exons (inside)
                        found = get_novel_exons(exon_chain, gene_exons, gene_introns, true);
                        if(found>0) {
                            //isof.annot.push_back("exon inclusion [" + std::to_string(found) + "]");
                            isof.annot.push_back("exon inclusion");
                            isof.nof_annot.push_back(found);
                        }
                        any_splicing = any_splicing || found;
                        
                        if (!any_splicing)
                        {
                            //cerr << "no splicing: " << it_iso->first.front().first << " " << gene_tss << " " << it_iso->first.back().first << " " << gene_tes << endl;
                            if(it_iso->first.front().first == gene_tss &&
                               it_iso->first.back().first == gene_tes+1) //gene_tes is inclusive
                            {
                                assert(isof.annot.empty() && isof.nof_annot.empty());
                                isof.annot.push_back("annotated");
                            } else
                            {
                                if( isof.gene_strand && (gene_tss < it_iso->first.front().first) ||
                                   !isof.gene_strand && (gene_tes+1 > it_iso->first.back().first) )
                                    isof.annot.push_back("truncated 5'");
                                
                                if( isof.gene_strand && (gene_tes+1 > it_iso->first.back().first) ||
                                   !isof.gene_strand && (gene_tss < it_iso->first.front().first) )
                                    isof.annot.push_back("truncated 3'");
                                
                                if( isof.gene_strand && (gene_tss > it_iso->first.front().first) ||
                                   !isof.gene_strand && (gene_tes+1 < it_iso->first.back().first) )
                                    isof.annot.push_back("novel TSS");
                                
                                if(isof.gene_strand && (gene_tes+1 < it_iso->first.back().first) ||
                                   !isof.gene_strand && (gene_tss > it_iso->first.front().first) )
                                    isof.annot.push_back("novel TES");
                                /*
                                 //at least one side truncated
                                 if(gene_tss < it_iso->first.front().first ||
                                 gene_tes+1 > it_iso->first.back().first )
                                 {
                                 isof.annot.push_back("truncated/annotated");
                                 }
                                 //at least one side extends beyond annotated tss/tes
                                 if( gene_tss > it_iso->first.front().first ||
                                 gene_tes+1 < it_iso->first.back().first )
                                 {
                                 isof.annot.push_back("novel TSS/TES");
                                 }
                                 */
                            }
                            isos2print.push_back(isof);
                            iso_printed = true;
                            assert(restore_cnt0.find(iso_nr)==restore_cnt0.end());
                            restore_cnt0[iso_nr] = it_iso->second->front();
                            it_iso->second->front() = iso_nr; //dirty! this field used to have different meaning (that's why it has to be printed before)
                            anno_ic.insert(*it_iso);
                        } else
                        {
                            //isof.annot.push_back("novel_spliced");
                            //remember isoform as relevant for translation
                            isos2print.push_back(isof);
                            //print_iso(isof);
                            iso_printed = true;
                            assert(restore_cnt0.find(iso_nr)==restore_cnt0.end());
                            restore_cnt0[iso_nr] = it_iso->second->front();
                            it_iso->second->front() = iso_nr; //dirty! this field used to have different meaning (that's why it has to be printed before)
                            novel_ic.insert(*it_iso);
                            
                            /*
                             //cluster isoforms by internal intron structure for later translation
                             assert(it_iso->first.size()>2); //must have at least one real intron, besides tss/tes
                             vector<std::pair<unsigned,unsigned> > iso_introns;
                             iso_introns.insert(iso_introns.end(), it_iso->first.begin()+1, it_iso->first.end()-1);
                             
                             intronchain_cnt_t iso2insert(iso_introns, NULL);
                             isoforms_t::iterator it_ic = intronchain_cluster.find(iso2insert);
                             if (it_ic != intronchain_cluster.end()) {
                             for(size_t i = 0; i<it_iso->second->size(); ++i) {
                             it_ic->second->at(i) = it_ic->second->at(i) + it_iso->second->at(i);
                             }
                             } else {
                             iso2insert.second = new vector<unsigned>(it_iso->second->size(),0);
                             for(size_t i = 0; i<it_iso->second->size(); ++i)
                             iso2insert.second->at(i) = it_iso->second->at(i);
                             intronchain_cluster.insert(iso2insert);
                             }
                             */
                        } //if any splicing
                    } //if sense == 0
                    if(!iso_printed) {
                        //isos2print.push_back(isof);
                        if(isoform_reads_per_gene != NULL)
                            print_iso(isof, out, &cert_file);
                        else
                            print_iso(isof, out);
                    }
                } //for all isoforms
                
                std::set<unsigned> broken_start_codon;
                std::set<unsigned> no_end_codon;
                std::map<unsigned, string> protseq_diff;
                unsigned start_codon = gtf_reader.getStartCodonPos(it_ctg->first, it_gene->first);
                
                broken_start_codon.clear();
                no_end_codon.clear();
                protseq_diff.clear();
                //translate isoforms without novel splicing
                if (sense==0 && !anno_ic.empty() && start_codon != -1u)
                {
                    print_uniq_protseq(chr, it_ctg->first, it_gene->first, start_codon, anno_ic, false, false, ref_sequence,
                                       prot_nr, protseq_file, as_rna_file, broken_start_codon, no_end_codon, protseq_diff);
                } //if !novel_ic.empty()
                
                
                //translate isoforms with novel splicing
                protseq_diff.clear(); //should be empty anyway
                if (sense==0 && !novel_ic.empty() && start_codon != -1u)
                {
                    print_uniq_protseq(chr, it_ctg->first, it_gene->first, start_codon, novel_ic, true, false, ref_sequence,
                                       prot_nr, protseq_file, as_rna_file, broken_start_codon, no_end_codon, protseq_diff);
                } //if !novel_ic.empty()
                
                //print missing isoforms and potentially indicate translation status
                vector<iso_features>::iterator it_i2p = isos2print.begin();
                for (; it_i2p != isos2print.end(); ++it_i2p) {
                    unsigned iso_nr = it_i2p->nof_sup->front();
                    assert(restore_cnt0.find(iso_nr)!=restore_cnt0.end());
                    it_i2p->nof_sup->front() = restore_cnt0.at(iso_nr);
                    if (broken_start_codon.find(iso_nr) != broken_start_codon.end()) {
                        it_i2p->annot.push_back("no valid start codon");
                    }
                    if (no_end_codon.find(iso_nr) != no_end_codon.end()) {
                        it_i2p->annot.push_back("no stop codon or empty protein");
                    }
                    if (protseq_diff.find(iso_nr) != protseq_diff.end()) {
                        it_i2p->annot.push_back(protseq_diff.at(iso_nr));
                    }
                    
                    if(isoform_reads_per_gene != NULL)
                        print_iso(*it_i2p, out, &cert_file);
                    else
                        print_iso(*it_i2p, out);
                }
                
            } //for sense, antisense
            
            if(Parameters::pwcount_file != "")
            {
                //count pairwise intron retentions, introns in exons, and combinations thereof from single exon reads
                if(intret_chain_per_gene.find(it_ctg->first) != intret_chain_per_gene.end())
                {
                    if(intret_chain_per_gene.at(it_ctg->first).find(it_gene->first) != intret_chain_per_gene.at(it_ctg->first).end())
                    {
                        if(!intret_chain_per_gene.at(it_ctg->first).at(it_gene->first).first.empty())
                        {
                            isoforms_t& current_chains = intret_chain_per_gene.at(it_ctg->first).at(it_gene->first).first;
                            isoforms_t::iterator it_iso = current_chains.begin();
                            for(; it_iso != current_chains.end(); ++it_iso)
                            {
                                if(it_iso->first.size()>=2)
                                {
                                    unsigned tot_sup = 0;
                                    vector<unsigned>::const_iterator it = it_iso->second->begin();
                                    for(; it!=it_iso->second->end(); ++it)
                                        tot_sup += *it;
                                    set<unsigned> ri;
                                    pair<unsigned,unsigned> single_exon(it_iso->first.front().second, it_iso->first.back().first);
                                    for(unsigned i=0; i<gene_introns.size(); ++i)
                                    {
                                        if(single_exon.first < gene_introns[i].first &&
                                           single_exon.second > gene_introns[i].second ) //single exon read spans this gene intron
                                            ri.insert(i);
                                    }
                                    count_pw_introns( gene_introns, single_exon, tot_sup, ri, cnt_retained, cnt_spliced,
                                                     cnt_i_retained, cnt_i_spliced, cnt_pair_retained, cnt_pair_spliced );
                                    
                                    set<unsigned> ie; //will stay empty
                                    vector<pair<unsigned,unsigned> > single_exon_iso;
                                    single_exon_iso.push_back(std::make_pair(it_iso->first.front().second, it_iso->first.back().first));
                                    count_pw_combi( gene_exons, gene_introns, single_exon_iso, introns_per_exon, tot_sup, ie, ri,
                                                   cnt_i_novel_comb, cnt_i_annot_comb, cnt_pair_novel_comb, cnt_pair_annot_comb  );
                                }
                                if(it_iso->first.size()>=1)
                                {
                                    unsigned tot_sup = 0;
                                    vector<unsigned>::const_iterator it = it_iso->second->begin();
                                    for(; it!=it_iso->second->end(); ++it)
                                        tot_sup += *it;
                                    set<unsigned> ie; //will stay empty
                                    vector<pair<unsigned,unsigned> > single_exon;
                                    single_exon.push_back(std::make_pair(it_iso->first.front().second, it_iso->first.back().first));
                                    count_pw_intinex( gene_exons, single_exon, introns_per_exon, tot_sup, ie, cnt_retained_ex, cnt_spliced_ex,
                                                     cnt_i_retained_ex, cnt_i_spliced_ex, cnt_pair_retained_ex, cnt_pair_spliced_ex );
                                }
                            }
                        }
                    }
                }
                
                
                //check for (pairs of) alternative exons
                for(unsigned i=1; i<gene_exons.size()-1; ++i)
                {
                    unsigned cnt_tot_i = cnt_skipped[i] + cnt_incl[i];
                    if ( cnt_skipped[i]/float(cnt_tot_i) >= 0.05 && cnt_skipped[i]/float(cnt_tot_i) <=0.95 ) {
                        //pwcount_file << "first alternative: " << cnt_skipped[i]/float(cnt_tot_i) << endl;
                        for(unsigned j=i+1; j<gene_exons.size()-1; ++j)
                        {
                            unsigned cnt_tot_j = cnt_skipped[j] + cnt_incl[j];
                            if ( cnt_skipped[j]/float(cnt_tot_j) >= 0.05 && cnt_skipped[j]/float(cnt_tot_j) <=0.95 ) {
                                pwcount_file << "found pair of alternative exons: " << i << " and " << j << " from 0-" << gene_exons.size()-1 << " in " << isof.gene_name << endl;
                                if (cnt_i_skipped.find(std::make_pair(i,j)) != cnt_i_skipped.end()) {
                                    pwcount_file << cnt_i_skipped.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_i_incl.find(std::make_pair(i,j)) != cnt_i_incl.end()) {
                                    pwcount_file << cnt_i_incl.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_skipped.find(std::make_pair(i,j)) != cnt_pair_skipped.end()) {
                                    pwcount_file << cnt_pair_skipped.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_incl.find(std::make_pair(i,j)) != cnt_pair_incl.end()) {
                                    pwcount_file << cnt_pair_incl.at(std::make_pair(i,j));
                                } else pwcount_file << "0";
                                pwcount_file << endl;
                            }
                        }
                    }
                }
                
                
                //check for (pairs of) alternative introns
                for(unsigned i=0; i<gene_introns.size(); ++i)
                {
                    unsigned cnt_tot_i = cnt_retained[i] + cnt_spliced[i];
                    if ( cnt_retained[i]/float(cnt_tot_i) >= 0.05 && cnt_retained[i]/float(cnt_tot_i) <=0.95 ) {
                        //pwcount_file << "first alternative intron: " << cnt_retained[i]/float(cnt_tot_i) << endl;
                        for(unsigned j=i+1; j<gene_introns.size(); ++j)
                        {
                            unsigned cnt_tot_j = cnt_retained[j] + cnt_spliced[j];
                            if ( cnt_retained[j]/float(cnt_tot_j) >= 0.05 && cnt_retained[j]/float(cnt_tot_j) <=0.95 ) {
                                pwcount_file << "found pair of alternative introns: " << i << " and " << j << " from 0-" << gene_introns.size()-1 << " in " << isof.gene_name << endl;
                                
                                if (cnt_i_retained.find(std::make_pair(i,j)) != cnt_i_retained.end() ) {
                                    pwcount_file << cnt_i_retained.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_i_spliced.find(std::make_pair(i,j)) != cnt_i_spliced.end() ) {
                                    pwcount_file << cnt_i_spliced.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_retained.find(std::make_pair(i,j)) != cnt_pair_retained.end()) {
                                    pwcount_file << cnt_pair_retained.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_spliced.find(std::make_pair(i,j)) != cnt_pair_spliced.end()) {
                                    pwcount_file << cnt_pair_spliced.at(std::make_pair(i,j));
                                } else pwcount_file << "0";
                                pwcount_file << endl;
                            }
                        }
                    }
                }
                
                //check for (pairs of) alternative introns in exons
                for(unsigned i=0; i<gene_exons.size(); ++i)
                {
                    unsigned cnt_tot_i = cnt_retained_ex[i] + cnt_spliced_ex[i];
                    if ( introns_per_exon.find(i) != introns_per_exon.end() && introns_per_exon.at(i).size() == 1 &&
                        cnt_retained_ex[i]/float(cnt_tot_i) >= 0.05 && cnt_retained_ex[i]/float(cnt_tot_i) <=0.95  ) {
                        //pwcount_file << "first alternative intron in exon: " << cnt_retained_ex[i]/float(cnt_tot_i) << endl;
                        for(unsigned j=i+1; j<gene_exons.size(); ++j)
                        {
                            unsigned cnt_tot_j = cnt_retained_ex[j] + cnt_spliced_ex[j];
                            if ( introns_per_exon.find(j) != introns_per_exon.end() && introns_per_exon.at(j).size() == 1 &&
                                cnt_retained_ex[j]/float(cnt_tot_j) >= 0.05 && cnt_retained_ex[j]/float(cnt_tot_j) <=0.95 ) {
                                pwcount_file << "found pair of alternative introns in exons: " << i << " and " << j << " from 0-" << gene_exons.size()-1 << " in " << isof.gene_name << endl;
                                
                                if (cnt_i_retained_ex.find(std::make_pair(i,j)) != cnt_i_retained_ex.end() ) {
                                    pwcount_file << cnt_i_retained_ex.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_i_spliced_ex.find(std::make_pair(i,j)) != cnt_i_spliced_ex.end() ) {
                                    pwcount_file << cnt_i_spliced_ex.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_retained_ex.find(std::make_pair(i,j)) != cnt_pair_retained_ex.end()) {
                                    pwcount_file << cnt_pair_retained_ex.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_spliced_ex.find(std::make_pair(i,j)) != cnt_pair_spliced_ex.end()) {
                                    pwcount_file << cnt_pair_spliced_ex.at(std::make_pair(i,j));
                                } else pwcount_file << "0";
                                pwcount_file << endl;
                            }
                        }
                    }
                }
                
                //check for (pairs of) alternative introns in exons and intron retentions
                for(unsigned i=0; i<gene_exons.size(); ++i)
                {
                    unsigned cnt_tot_i = cnt_retained_ex[i] + cnt_spliced_ex[i];
                    if ( introns_per_exon.find(i) != introns_per_exon.end() && introns_per_exon.at(i).size() == 1 &&
                        cnt_retained_ex[i]/float(cnt_tot_i) >= 0.05 && cnt_retained_ex[i]/float(cnt_tot_i) <=0.95  ) {
                        //pwcount_file << "first alternative intron in exon: " << cnt_retained_ex[i]/float(cnt_tot_i) << endl;
                        for(unsigned j=0; j<gene_introns.size(); ++j)
                        {
                            unsigned cnt_tot_j = cnt_retained[j] + cnt_spliced[j];
                            if ( cnt_retained[j]/float(cnt_tot_j) >= 0.05 && cnt_retained[j]/float(cnt_tot_j) <=0.95 ) {
                                pwcount_file << "found pair of alternative intron in exon and intron retention: " << i << " and " << j << " in " << isof.gene_name << endl;
                                
                                if (cnt_i_annot_comb.find(std::make_pair(i,j)) != cnt_i_annot_comb.end() ) {
                                    pwcount_file << cnt_i_annot_comb.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_i_novel_comb.find(std::make_pair(i,j)) != cnt_i_novel_comb.end() ) {
                                    pwcount_file << cnt_i_novel_comb.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_annot_comb.find(std::make_pair(i,j)) != cnt_pair_annot_comb.end()) {
                                    pwcount_file << cnt_pair_annot_comb.at(std::make_pair(i,j)) << " ";
                                } else pwcount_file << "0 ";
                                if (cnt_pair_novel_comb.find(std::make_pair(i,j)) != cnt_pair_novel_comb.end()) {
                                    pwcount_file << cnt_pair_novel_comb.at(std::make_pair(i,j));
                                } else pwcount_file << "0";
                                pwcount_file << endl;
                            }
                        }
                    }
                }
            }
        }

        
        if(Parameters::dimer_file != "")
        {
            //store dimers before going to next contig
            //std::multiset<pair<unsigned,unsigned> >::iterator it_i;
            set<pair<unsigned,unsigned> >::iterator it_i;
            for (it_i = novel_fw_introns.begin(); it_i != novel_fw_introns.end(); ++it_i) {
                string dimer = get_dimer(*it_i, ref_sequence, false);
                if (novel_dimers.find(dimer) != novel_dimers.end()) {
                    novel_dimer_occ.at(dimer) += 1;
                } else {
                    novel_dimers.insert(dimer);
                    novel_dimer_occ[dimer] = 1;
                }
            }
            for (it_i = novel_rv_introns.begin(); it_i != novel_rv_introns.end(); ++it_i) {
                string dimer = get_dimer(*it_i, ref_sequence, true);
                if (novel_dimers.find(dimer) != novel_dimers.end()) {
                    novel_dimer_occ.at(dimer) += 1;
                } else {
                    novel_dimers.insert(dimer);
                    novel_dimer_occ[dimer] = 1;
                }
            }
            for (it_i = anno_fw_introns.begin(); it_i != anno_fw_introns.end(); ++it_i) {
                string dimer = get_dimer(*it_i, ref_sequence, false);
                if (anno_dimers.find(dimer) != anno_dimers.end()) {
                    anno_dimer_occ.at(dimer) += 1;
                } else {
                    anno_dimers.insert(dimer);
                    anno_dimer_occ[dimer] = 1;
                }
            }
            for (it_i = anno_rv_introns.begin(); it_i != anno_rv_introns.end(); ++it_i) {
                string dimer = get_dimer(*it_i, ref_sequence, true);
                if (anno_dimers.find(dimer) != anno_dimers.end()) {
                    anno_dimer_occ.at(dimer) += 1;
                } else {
                    anno_dimers.insert(dimer);
                    anno_dimer_occ[dimer] = 1;
                }
            }
        }
    }
    
    //now let's look for clustered intron retention chains from single exon reads
    it_ctg = intret_chain_per_gene.begin();
    for (; it_ctg != intret_chain_per_gene.end(); ++it_ctg)
    {
        string chr = gtf_reader.getContigNames().at(it_ctg->first);
        
        //needed later to translate into protein
        std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it_refseq;
        it_refseq = reference_sequences->find(chr);
        if(it_refseq == reference_sequences->end()) {
            //cerr << "Reference \"" << chr << "\" missing." << endl;
            continue;
        }
        const NamedDnaSequence& ref_sequence = *it_refseq->second;
        
        it_gene = it_ctg->second.begin();
        for (; it_gene != it_ctg->second.end(); ++it_gene)
        {
            iso_features isof;
            isof.gene_name = gtf_reader.getGeneName(it_ctg->first, it_gene->first);
            isof.chr = chr;
            
            //cout << endl << "chr" << gtf_reader.getContigNames().at(it_ctg->first) << "\t";
            //cout << gtf_reader.getGeneName(it_ctg->first, it_gene->first);
            if(gtf_reader.is_gene_on_rv(it_ctg->first, it_gene->first)) {
                //cout << " (-)" << endl;
                isof.gene_strand = false;
            }
            else {
                //cout << " (+)" << endl;
                isof.gene_strand = true;
            }
            //cout << "-------------------------------------------------------------------------" << endl;
            
            unsigned gene_tss = gtf_reader.getTss(it_ctg->first, it_gene->first);
            unsigned gene_tes = gtf_reader.getTes(it_ctg->first, it_gene->first);
            
            for (int sense = 0; sense<2; ++sense)
            {
                isos2print.clear();
                restore_cnt0.clear();
                novel_ic.clear();
                anno_ic.clear();
                isoforms_t::iterator it_iso;
                std::pair<isoforms_t,isoforms_t>& current_chains = intret_chain_per_gene.at(it_ctg->first).at(it_gene->first);
                if (sense==0) {
                    if (current_chains.first.empty())
                        continue;
                    it_iso = current_chains.first.begin();
                }
                else
                {
                    if (current_chains.second.empty()) {
                        continue;
                    }
                    it_iso = current_chains.second.begin();
                }
                
                for (; (sense==0 && it_iso != current_chains.first.end()) ||
                     (sense==1 && it_iso != current_chains.second.end()) ; ++it_iso, ++iso_nr)
                {
                    //if (iso_nr > 1)
                    //    cout << endl;
                    
                    bool iso_printed = false;
                    std::stringstream ss;
                    ss << iso_nr;
                    isof.iso_id = "Iso_se_" + ss.str();
                    
                    if (sense==1) { //antisense
                        isof.iso_strand = !isof.gene_strand;
                    } else
                        isof.iso_strand = isof.gene_strand;
                    
                    isof.nof_sup = it_iso->second;
                    
                    //skip spanned intron, keep tss/tes
                    intronchain_t exon_chain;
                    exon_chain.push_back(std::make_pair(it_iso->first.front().second, it_iso->first.back().first));
                    isof.exons = exon_chain;
                    
                    //cout << "Single-Exon-Isoform " << iso_nr;
                    //if (sense==1) {
                    //    cout << " - antisense";
                    //}
                    //cout << " (";
                    //for (size_t ii=0; ii<it_iso->second->size(); ++ii) {
                    //    cout << it_iso->second->at(ii);
                    //    if (ii<it_iso->second->size()-1)
                    //        cout << ",";
                    //}
                    //cout << "): " << endl;
                    
                    isof.annot.clear();
                    isof.nof_annot.clear();
                    if (sense==0)
                    {
                        //isof.annot.push_back("relevant");
                        if(it_iso->first.size()>2) {
                            //isof.annot.push_back("intron retention ["  + std::to_string(it_iso->first.size()-2) + "]");
                            isof.annot.push_back("intron retention");
                            //isof.annot.push_back("novel_spliced");
                            isof.nof_annot.push_back(it_iso->first.size()-2);
                            
                            if(Parameters::retint_length_file != "")
                            {
                                //output retained intron length to check for bias
                                vector<pair<unsigned,unsigned> >::const_iterator it = it_iso->first.begin();
                                for(++it; it != it_iso->first.end()-1; ++it)
                                {
                                    retint_length_file << "retained intron: ";
                                    retint_length_file << isof.gene_name << ":" << "intron:[" << it->first << "," << it->second << "] - ";
                                    assert(it->second > it->first);
                                    retint_length_file << it->second - it->first << endl;
                                }
                            }
                            
                            isos2print.push_back(isof);
                            //print_iso(isof);
                            iso_printed = true;
                            assert(restore_cnt0.find(iso_nr)==restore_cnt0.end());
                            restore_cnt0[iso_nr] = it_iso->second->front();
                            it_iso->second->front() = iso_nr; //dirty! this field used to have different meaning (that's why it has to be printed before)
                            novel_ic.insert(*it_iso);
                        }
                        else
                        {
                            assert(it_iso->first.size()==2);
                            if(it_iso->first.front().first == gene_tss &&
                               it_iso->first.back().first == gene_tes+1) //gene_tes is inclusive
                            {
                                isof.annot.push_back("annotated");
                            } else
                            {
                                if( isof.gene_strand && (gene_tss < it_iso->first.front().first) ||
                                   !isof.gene_strand && (gene_tes+1 > it_iso->first.back().first) )
                                    isof.annot.push_back("truncated 5'");
                                
                                if( isof.gene_strand && (gene_tes+1 > it_iso->first.back().first) ||
                                   !isof.gene_strand && (gene_tss < it_iso->first.front().first) )
                                    isof.annot.push_back("truncated 3'");
                                
                                if( isof.gene_strand && (gene_tss > it_iso->first.front().first) ||
                                   !isof.gene_strand && (gene_tes+1 < it_iso->first.back().first) )
                                    isof.annot.push_back("novel TSS");
                                
                                if(isof.gene_strand && (gene_tes+1 < it_iso->first.back().first) ||
                                   !isof.gene_strand && (gene_tss > it_iso->first.front().first) )
                                    isof.annot.push_back("novel TES");
                                /*
                                 //at least one side truncated
                                 if( gene_tss < it_iso->first.front().first ||
                                 gene_tes+1 > it_iso->first.back().first )
                                 {
                                 isof.annot.push_back("truncated/annotated");
                                 }
                                 //at least one side extends beyond annotated tss/tes
                                 if( gene_tss > it_iso->first.front().first ||
                                 gene_tes+1 < it_iso->first.back().first )
                                 {
                                 isof.annot.push_back("novel TSS/TES");
                                 }
                                 */
                                isos2print.push_back(isof);
                                iso_printed = true;
                                assert(restore_cnt0.find(iso_nr)==restore_cnt0.end());
                                restore_cnt0[iso_nr] = it_iso->second->front();
                                it_iso->second->front() = iso_nr; //dirty! this field used to have different meaning (that's why it has to be printed before)
                                anno_ic.insert(*it_iso);
                            }
                        }
                    }
                    
                    //intronchain_t::const_iterator it_ic = it_iso->first.begin();
                    //for(; it_ic != it_iso->first.end(); ++it_ic) {
                    //    cout << "[" << it_ic->first << "," << it_ic->second << "]";
                    //    if (it_ic+1 != it_iso->first.end())
                    //        cout << " - ";
                    //}
                    //cout << endl;
                    if(!iso_printed)
                        print_iso(isof, out);
                }//for all isoforms
                
                std::set<unsigned> broken_start_codon;
                std::set<unsigned> no_end_codon;
                std::map<unsigned, string> protseq_diff;
                unsigned start_codon = gtf_reader.getStartCodonPos(it_ctg->first, it_gene->first);
                
                broken_start_codon.clear();
                no_end_codon.clear();
                protseq_diff.clear();
                if (sense==0 && !anno_ic.empty() && start_codon != -1u)
                {
                    print_uniq_protseq(chr, it_ctg->first, it_gene->first, start_codon, anno_ic, false, true, ref_sequence,
                                       prot_nr, protseq_file, as_rna_file, broken_start_codon, no_end_codon, protseq_diff);
                }
                
                protseq_diff.clear(); //should be empty anyway
                if (sense==0 && !novel_ic.empty() && start_codon != -1u)
                {
                    print_uniq_protseq(chr, it_ctg->first, it_gene->first, start_codon, novel_ic, true, true, ref_sequence,
                                       prot_nr, protseq_file, as_rna_file, broken_start_codon, no_end_codon, protseq_diff);
                }
                
                //print missing isoforms and potentially indicate translation status
                vector<iso_features>::iterator it_i2p = isos2print.begin();
                for (; it_i2p != isos2print.end(); ++it_i2p) {
                    unsigned iso_nr = it_i2p->nof_sup->front();
                    assert(restore_cnt0.find(iso_nr)!=restore_cnt0.end());
                    it_i2p->nof_sup->front() = restore_cnt0[iso_nr];
                    if (broken_start_codon.find(iso_nr) != broken_start_codon.end()) {
                        it_i2p->annot.push_back("no valid start codon");
                    }
                    if (no_end_codon.find(iso_nr) != no_end_codon.end()) {
                        it_i2p->annot.push_back("no stop codon or empty protein");
                    }
                    if (protseq_diff.find(iso_nr) != protseq_diff.end())
                        it_i2p->annot.push_back(protseq_diff.at(iso_nr));
                    print_iso(*it_i2p, out);
                }
                
            }//for sense, antisense
        }//for all genes
    }//for all contigs
    
    pwcount_file.close();
    protseq_file.close();
    as_rna_file.close();
    if(Parameters::hexamer_file != "")
        hexamer_file.close();
    if(Parameters::retint_length_file != "")
        retint_length_file.close();
    
    if(Parameters::dimer_file != "")
    {
        std::ofstream dimer_file;
        dimer_file.open(Parameters::dimer_file);
        dimer_file << "Dimers of annotated introns:" << endl;
        std::set<string>::const_iterator it_dimer;
        for (it_dimer = anno_dimers.begin(); it_dimer != anno_dimers.end(); ++it_dimer) {
            dimer_file << *it_dimer << ": " << anno_dimer_occ.at(*it_dimer) << endl;
        }
        dimer_file << "Dimers of novel introns:" << endl;
        for (it_dimer = novel_dimers.begin(); it_dimer != novel_dimers.end(); ++it_dimer) {
            dimer_file << *it_dimer << ": " << novel_dimer_occ.at(*it_dimer) << endl;
        }
        dimer_file.close();
    }
    
    if(isoform_reads_per_gene != NULL)
        cert_file.close();
}


void NovelTrans::count_pw_combi(const vector<std::pair<unsigned, unsigned> >& gene_exons, const vector<std::pair<unsigned, unsigned> >& gene_introns,
                                vector<std::pair<unsigned,unsigned> >& read_exons, std::map<unsigned, std::set<pair<unsigned,unsigned> > >& introns_per_exon,
                                unsigned tot_sup,
                                const set<unsigned>& ie,
                                const set<unsigned>& ri,
                                std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_novel_comb,
                                std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_annot_comb,
                                std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_novel_comb,
                                std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_annot_comb
                                ) const
{
    unsigned fake_certificate;
    pair<unsigned,unsigned> premRNA(read_exons.begin()->first, read_exons.back().second);

    for(unsigned i=0; i<gene_exons.size(); ++i)
    {
        for(unsigned j=0; j<gene_introns.size(); ++j)
        {
            if(premRNA.first >= gene_introns[j].first ||
                    premRNA.second <= gene_introns[j].second )
                continue; //intron j not contained in pre-mRNA

            if (ie.find(i) != ie.end())
            {
                if (ri.find(j) != ri.end() ) { //both novel
                    if (cnt_pair_novel_comb.find(std::make_pair(i,j)) != cnt_pair_novel_comb.end()) {
                        cnt_pair_novel_comb.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_novel_comb[std::make_pair(i,j)] = tot_sup;
                } else
                {
                    if (cnt_i_novel_comb.find(std::make_pair(i,j)) != cnt_i_novel_comb.end()) {
                        cnt_i_novel_comb.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_novel_comb[std::make_pair(i,j)] = tot_sup;
                }
            } else if (introns_per_exon.find(i) != introns_per_exon.end() &&
                       is_strictly_contained(read_exons, *(introns_per_exon.at(i).begin()), fake_certificate ))
            {
                if (ie.find(j) != ie.end() ) {
                    if (cnt_i_annot_comb.find(std::make_pair(i,j)) != cnt_i_annot_comb.end()) {
                        cnt_i_annot_comb.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_annot_comb[std::make_pair(i,j)] = tot_sup;
                } else
                {
                    if (cnt_pair_annot_comb.find(std::make_pair(i,j)) != cnt_pair_annot_comb.end()) {
                        cnt_pair_annot_comb.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_annot_comb[std::make_pair(i,j)] = tot_sup;
                }
            }
        }
    }
}


void NovelTrans::count_pw_intinex(const vector<std::pair<unsigned, unsigned> >& gene_exons, vector<std::pair<unsigned,unsigned> >& read_exons, std::map<unsigned, std::set<pair<unsigned,unsigned> > >& introns_per_exon, unsigned tot_sup,
                                  const set<unsigned>& ie,
                                  vector<unsigned>& cnt_retained,
                                  vector<unsigned>& cnt_spliced,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_retained,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_spliced,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_retained,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_spliced
                                  ) const
{
    unsigned fake_certificate;

    //count retained/spliced (pairs of) introns
    for(unsigned i=0; i<gene_exons.size(); ++i)
    {
        if (ie.find(i) != ie.end())
            cnt_spliced[i] += tot_sup;
        else if( introns_per_exon.find(i) != introns_per_exon.end() &&   //novel intron for exon i has been seen
                 is_strictly_contained(read_exons, *(introns_per_exon.at(i).begin()), fake_certificate ) )  //just check arbitrary novel intron - if multiple exists, exons will be ignored later
        {
                cnt_retained[i] += tot_sup;
        }
        for(unsigned j=i+1; j<gene_exons.size(); ++j)
        {
            if (ie.find(i) != ie.end())
            {
                if (ie.find(j) != ie.end() ) { //both spliced
                    if (cnt_pair_spliced.find(std::make_pair(i,j)) != cnt_pair_spliced.end()) {
                        cnt_pair_spliced.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_spliced[std::make_pair(i,j)] = tot_sup;
                } else if (introns_per_exon.find(j) != introns_per_exon.end() &&
                           is_strictly_contained(read_exons, *(introns_per_exon.at(j).begin()), fake_certificate ))
                {
                    if (cnt_i_spliced.find(std::make_pair(i,j)) != cnt_i_spliced.end()) {
                        cnt_i_spliced.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_spliced[std::make_pair(i,j)] = tot_sup;
                }
            } else if (introns_per_exon.find(i) != introns_per_exon.end() &&
                       is_strictly_contained(read_exons, *(introns_per_exon.at(i).begin()), fake_certificate ))
            {
                if (ie.find(j) != ie.end() ) {
                    if (cnt_i_retained.find(std::make_pair(i,j)) != cnt_i_retained.end()) {
                        cnt_i_retained.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_retained[std::make_pair(i,j)] = tot_sup;
                } else if (introns_per_exon.find(j) != introns_per_exon.end() &&
                           is_strictly_contained(read_exons, *(introns_per_exon.at(j).begin()), fake_certificate ))
                {
                    if (cnt_pair_retained.find(std::make_pair(i,j)) != cnt_pair_retained.end()) {
                        cnt_pair_retained.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_retained[std::make_pair(i,j)] = tot_sup;
                }
            }
        }
    }
}



void NovelTrans::count_pw_introns(const vector<std::pair<unsigned, unsigned> >& gene_introns, const pair<unsigned,unsigned>& premRNA, unsigned tot_sup,
                                  const set<unsigned>& ri,
                                  vector<unsigned>& cnt_retained,
                                  vector<unsigned>& cnt_spliced,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_retained,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_i_spliced,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_retained,
                                  std::map<std::pair<unsigned, unsigned>, unsigned>& cnt_pair_spliced
                                  ) const
{
    //count retained/spliced (pairs of) introns
    for(unsigned i=0; i<gene_introns.size(); ++i)
    {
        if(premRNA.first >= gene_introns[i].first ||
           premRNA.second <= gene_introns[i].second )
            continue; //intron i not contained in pre-mRNA
        if (ri.find(i) != ri.end())
            cnt_retained[i] += tot_sup;
        else
            cnt_spliced[i] += tot_sup;
        for(unsigned j=i+1; j<gene_introns.size(); ++j)
        {
            if(premRNA.first >= gene_introns[j].first ||
                    premRNA.second <= gene_introns[j].second )
                continue; //intron j not contained in pre-mRNA

            if (ri.find(i) != ri.end())
            {
                if (ri.find(j) != ri.end() ) { //both retained
                    if (cnt_pair_retained.find(std::make_pair(i,j)) != cnt_pair_retained.end()) {
                        cnt_pair_retained.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_retained[std::make_pair(i,j)] = tot_sup;
                } else {
                    if (cnt_i_retained.find(std::make_pair(i,j)) != cnt_i_retained.end()) {
                        cnt_i_retained.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_retained[std::make_pair(i,j)] = tot_sup;
                }
            } else {
                if (ri.find(j) != ri.end() ) {
                    if (cnt_i_spliced.find(std::make_pair(i,j)) != cnt_i_spliced.end()) {
                        cnt_i_spliced.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_i_spliced[std::make_pair(i,j)] = tot_sup;
                } else {
                    if (cnt_pair_spliced.find(std::make_pair(i,j)) != cnt_pair_spliced.end()) {
                        cnt_pair_spliced.at(std::make_pair(i,j)) += tot_sup;
                    } else
                        cnt_pair_spliced[std::make_pair(i,j)] = tot_sup;
                }
            }
        }
    }
}



void NovelTrans::filter_inv_by_dimers(const vector<std::pair<unsigned,unsigned> >& introns,
                                      vector<std::pair<unsigned,unsigned> >& introns_filtered,
                                      const NamedDnaSequence& ref_sequence,
                                      bool on_reverse) const
{
    vector<std::pair<unsigned,unsigned> >::const_iterator it;
    for(it = introns.begin()+1; it != introns.end()-1; ++it)
    {
        if( !is_canonical(*it, ref_sequence, on_reverse) )
            introns_filtered.push_back(*it);
    }
}

void NovelTrans::filter_by_dimers(const vector<std::pair<unsigned,unsigned> >& introns,
                                  vector<std::pair<unsigned,unsigned> >& introns_filtered,
                                  const NamedDnaSequence& ref_sequence,
                                  bool on_reverse) const
{
    vector<std::pair<unsigned,unsigned> >::const_iterator it;
    for(it = introns.begin()+1; it != introns.end()-1; ++it)
    {
        if( is_canonical(*it, ref_sequence, on_reverse) )
            introns_filtered.push_back(*it);
    }
}


/*
 * For each novel intron chain cluster, the leftmost tss and rightmost tes are determined. For the resulting
 * mRNA, translation is started at each start codon. The longest ORF is kept and output in fasta file. IDs 
 * of novel isoforms without ORF are recorded and returned.
 */
void NovelTrans::print_max_orf(string chr, isoforms_t& novel_ic, bool single_exon_iso, bool gene_on_rv, const NamedDnaSequence& ref_sequence,
                               unsigned& prot_nr, std::ofstream& protseq_file, std::ofstream& novel_rna_file, set<unsigned>& no_orf, unsigned single_exon_nr) const
{
    //go through all novel intron chain clusters and translate into proteins
    vector<vector<intronchain_cnt_t> > ic_clusters;
    cluster_by_ic(novel_ic, ic_clusters);
    vector<vector<intronchain_cnt_t> >::const_iterator it_cluster = ic_clusters.begin();
    for(; it_cluster != ic_clusters.end(); ++it_cluster)
    {
        //cout << "next cluster: " << endl;
        assert(!it_cluster->empty());
        vector<intronchain_cnt_t>::const_iterator it_ic = it_cluster->begin();

        vector<unsigned> corr_iso;
        corr_iso.clear();
        if(!single_exon_iso) //not set for single exon iso
            corr_iso.push_back(it_ic->second->front());
        else
            corr_iso.push_back(single_exon_nr);

        unsigned leftmost = it_ic->first.front().second;
        unsigned rightmost = it_ic->first.back().first;
        //determine leftmost tss and rightmost tes
        for(it_ic++; it_ic != it_cluster->end(); ++it_ic)
        {
            if(it_ic->first.front().second < leftmost)
                leftmost = it_ic->first.front().second;
            if(it_ic->first.back().first > rightmost)
                rightmost = it_ic->first.back().first;
            if(!single_exon_iso)
                corr_iso.push_back(it_ic->second->front());
        }

        intronchain_t virt_iso_exons;  //exon chain that will be translated
        if (single_exon_iso) {
            virt_iso_exons.push_back(std::make_pair(leftmost, rightmost));
        } else
        {
            get_ic_exons(it_cluster->begin()->first, virt_iso_exons); //exon chain of first isoform in cluster

            //extend tss/tes to leftmost/rightmost
            virt_iso_exons.front().first = leftmost;
            virt_iso_exons.back().second = rightmost;
        }
        
        //preparing header for novel rna sequence in fasta
        std::stringstream ss;
        ss << ">NR" << "|";
        vector<unsigned>::const_iterator it_corr_iso = corr_iso.begin();
        for (; it_corr_iso != corr_iso.end(); ++it_corr_iso) {
            if (it_corr_iso != corr_iso.begin())
                ss << ",";
            ss << "NG_";
            if (!single_exon_iso)
                ss << "I_";
            ss << *it_corr_iso;
        }
        novel_rna_file << ss.str() << "|" << chr;
        novel_rna_file << endl;

        //ready to translate
        string mrna = "";
        string protseq = "";
        string longest_protseq = "";
        if(!gene_on_rv)
        {
            for(vector<pair<unsigned,unsigned> >::const_iterator it_virt_iso_ex = virt_iso_exons.begin(); it_virt_iso_ex != virt_iso_exons.end(); ++it_virt_iso_ex)
            {
                mrna += ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second);
            }
            novel_rna_file << mrna << endl;
            
            unsigned i = 0;
            while(i<mrna.size()-8) //start codon, end codon, at least one aa
            {
                if(mrna.substr(i,3)=="ATG") {
                    protseq = translate(mrna.substr(i));
                    if(protseq.size() > longest_protseq.size())
                        longest_protseq = protseq;
                }
                i++;
            }
            if(longest_protseq.size() == 0 && !single_exon_iso) {
                no_orf.insert(corr_iso.begin(), corr_iso.end());
            } else if(longest_protseq.size() == 0 && single_exon_iso)
                no_orf.insert(single_exon_nr);
        } else
        {
            for(vector<pair<unsigned,unsigned> >::const_reverse_iterator it_virt_iso_ex = virt_iso_exons.rbegin(); it_virt_iso_ex != virt_iso_exons.rend(); ++it_virt_iso_ex)
            {
                mrna += getRevCompl(ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second));
            }
            novel_rna_file << mrna << endl;
            
            unsigned i = 0;
            while(i<mrna.size()-8) //start codon, end codon, at least one aa
            {
                if(mrna.substr(i,3)=="ATG") {
                    protseq = translate(mrna.substr(i));
                    if(protseq.size() > longest_protseq.size())
                        longest_protseq = protseq;
                }
                i++;
            }
            if(longest_protseq.size() == 0 && !single_exon_iso) {
                no_orf.insert(corr_iso.begin(), corr_iso.end());
            } else if(longest_protseq.size() == 0 && single_exon_iso)
                no_orf.insert(single_exon_nr);
        }

        if(longest_protseq.size()>0) {
            std::stringstream ss;
            ss << ">NP" << prot_nr << "|";
            vector<unsigned>::const_iterator it_corr_iso = corr_iso.begin();
            for (; it_corr_iso != corr_iso.end(); ++it_corr_iso) {
                if (it_corr_iso != corr_iso.begin())
                    ss << ",";
                ss << "NG_";
                if (!single_exon_iso)
                    ss << "I_";
                ss << *it_corr_iso;
            }
            protseq_file << ss.str() << "|" << chr;
            protseq_file << endl;
            protseq_file << longest_protseq << endl;
            prot_nr++;
        }
    } //for it_cluster
}

int NovelTrans::get_max_orf(string chr, intronchain_t& virt_iso_exons, bool gene_on_rv, const NamedDnaSequence& ref_sequence) const
{
    string mrna = "";
    string protseq = "";
    string longest_protseq = "";
    if(!gene_on_rv)
    {
        for(vector<pair<unsigned,unsigned> >::const_iterator it_virt_iso_ex = virt_iso_exons.begin(); it_virt_iso_ex != virt_iso_exons.end(); ++it_virt_iso_ex)
        {
            mrna += ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second);
        }
        
        unsigned i = 0;
        while(i<mrna.size()-8) //start codon, end codon, at least one aa
        {
            if(mrna.substr(i,3)=="ATG") {
                protseq = translate(mrna.substr(i));
                if(protseq.size() > longest_protseq.size())
                    longest_protseq = protseq;
            }
            i++;
        }
    } else
    {
        for(vector<pair<unsigned,unsigned> >::const_reverse_iterator it_virt_iso_ex = virt_iso_exons.rbegin(); it_virt_iso_ex != virt_iso_exons.rend(); ++it_virt_iso_ex)
        {
            mrna += getRevCompl(ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second));
        }
        
        unsigned i = 0;
        while(i<mrna.size()-8) //start codon, end codon, at least one aa
        {
            if(mrna.substr(i,3)=="ATG") {
                protseq = translate(mrna.substr(i));
                if(protseq.size() > longest_protseq.size())
                    longest_protseq = protseq;
            }
            i++;
        }
    }
    return longest_protseq.size();
}

string NovelTrans::compare_protseq(const intronchain_t& exon_chain_ref, const intronchain_t& exon_chain_novel, unsigned start_codon,
                                   unsigned novel_stop_codon, unsigned ref_stop_codon, bool reverse) const
{     
    //cut reference at novel stop codon if possible
    if(!reverse)
        ref_stop_codon = std::min(ref_stop_codon, novel_stop_codon);
    else
        ref_stop_codon = std::max(ref_stop_codon, novel_stop_codon);

    vector<pair<bool, pair<int,int> > > indel_seq; //first: is insertion? second-first: how many bases second-second: where in ref?
    vector<pair<unsigned, pair<bool,bool> > > all_ss; //vector elements store coordinate of ss, whether ss is ref, and whether ss is start site
    bool hit_start_codon = false;
    intronchain_t::const_iterator it_ex = exon_chain_ref.begin();
    int rev_tot_ref_length = 0;
    if(!reverse)  
    {
        for (; it_ex != exon_chain_ref.end(); ++it_ex) {
            if (it_ex->second <= start_codon) //skip UTR
                continue;
            if (it_ex->first <= start_codon) {  //cut exon at start codon
                hit_start_codon = true;
                all_ss.push_back( std::make_pair(start_codon, std::make_pair(true, true) ) );
                if (it_ex->second < ref_stop_codon)
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(true, false) ) );
                else {
                    all_ss.push_back( std::make_pair(ref_stop_codon, std::make_pair(true, false) ) );
                    break;
                }
            } else {
                assert(hit_start_codon);
                all_ss.push_back( std::make_pair(it_ex->first, std::make_pair(true, true) ) );
                if (it_ex->second < ref_stop_codon)
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(true, false) ) );
                else {
                    all_ss.push_back( std::make_pair(ref_stop_codon, std::make_pair(true, false) ) );
                    break;
                }
            }
        }
    }
    else //keep all ss left of start_codon and right of stop codon
    {
        bool hit_stop_codon = false;
        for (; it_ex != exon_chain_ref.end(); ++it_ex) {
            if (it_ex->second <= ref_stop_codon+1)
                continue;
            if (it_ex->first <= ref_stop_codon+1) {
                hit_stop_codon = true;
                all_ss.push_back( std::make_pair(ref_stop_codon+1, std::make_pair(true, true) ) );
                if (it_ex->second <= start_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(true, false) ) );
                    rev_tot_ref_length += it_ex->second - ref_stop_codon - 1;
                } else {
                    all_ss.push_back( std::make_pair(start_codon+1, std::make_pair(true, false) ) );
                    rev_tot_ref_length += start_codon + 1 -ref_stop_codon - 1;
                    break;
                }
            }  else {
                all_ss.push_back( std::make_pair(it_ex->first, std::make_pair(true, true) ) );
                if (it_ex->second <= start_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(true, false) ) );
                    rev_tot_ref_length += it_ex->second - it_ex->first;
                } else {
                    all_ss.push_back( std::make_pair(start_codon+1, std::make_pair(true, false) ) );
                    rev_tot_ref_length += start_codon + 1 - it_ex->first;
                    break;
                }
            }
        }
    }

    hit_start_codon = false;
    it_ex = exon_chain_novel.begin();
    if(!reverse)
    {
        for (; it_ex != exon_chain_novel.end(); ++it_ex) {
            if (it_ex->second <= start_codon) //skip UTR
                continue;
            if (it_ex->first <= start_codon) {  //cut exon at start codon
                hit_start_codon = true;
                all_ss.push_back( std::make_pair(start_codon, std::make_pair(false, true) ) );
                if (it_ex->second < novel_stop_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(false, false) ) );
                } else {
                    all_ss.push_back( std::make_pair(novel_stop_codon, std::make_pair(false, false) ) );
                    break;
                }
            } else {
                all_ss.push_back( std::make_pair(it_ex->first, std::make_pair(false, true) ) );
                if (it_ex->second < novel_stop_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(false, false) ) );
                } else {
                    all_ss.push_back( std::make_pair(novel_stop_codon, std::make_pair(false, false) ) );
                    break;
                }
            }
        }
    } else //keep all ss left of start_codon and right of stop codon
    {
        bool hit_stop_codon = false;
        for (; it_ex != exon_chain_novel.end(); ++it_ex) {
            if (it_ex->second <= novel_stop_codon+1)
                continue;
            if (it_ex->first <= novel_stop_codon+1) {
                hit_stop_codon = true;
                all_ss.push_back( std::make_pair(novel_stop_codon+1, std::make_pair(false, true) ) );
                if (it_ex->second <= start_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(false, false) ) );
                } else {
                    all_ss.push_back( std::make_pair(start_codon+1, std::make_pair(false, false) ) );
                    break;
                }
            } else {
                all_ss.push_back( std::make_pair(it_ex->first, std::make_pair(false, true) ) );
                if (it_ex->second <= start_codon) {
                    all_ss.push_back( std::make_pair(it_ex->second, std::make_pair(false, false) ) );
                } else {
                    all_ss.push_back( std::make_pair(start_codon+1, std::make_pair(false, false) ) );
                    break;
                }
            }
        }
    }

    sort(all_ss.begin(), all_ss.end(), anno_ss_order);

    int curr_start_ref = -1;
    int curr_start_novel = -1;
    int prev_end_ref = -1;
    int prev_end_novel = -1;
    unsigned total_ref = 0;
    int frame = 0; //insertion of 1 in novel = deletion of 2 in novel = frame 1, insertion of 2 in novel = deletion of 1 in novel = frame 2
    vector<pair<unsigned, pair<bool,bool> > >::const_iterator it_ss = all_ss.begin();
    for (; it_ss != all_ss.end(); ++it_ss) {
        bool is_ref = it_ss->second.first;
        bool is_start = it_ss->second.second;
        if (is_start)
        {
            if (is_ref) //opening reference exons
            {
                if (curr_start_novel >= 0) //novel exon is already open
                {
                    assert(curr_start_novel == -1 || it_ss->first >= curr_start_novel);
                    assert(prev_end_ref == -1 || it_ss->first > prev_end_ref);
                    unsigned ins = it_ss->first - std::max(curr_start_novel, prev_end_ref); //could be 0
                    frame = (frame + ins) % 3;                                      
                    if (ins > 0) {
                        //cerr << "ins " << ins << endl;
                        if(!reverse)
                            indel_seq.push_back(std::make_pair(true, std::make_pair(ins, total_ref)));
                        else {
                            assert(rev_tot_ref_length >= total_ref);
                            indel_seq.push_back(std::make_pair(true, std::make_pair(ins, rev_tot_ref_length - total_ref)));
                        }
                    }
                }
                curr_start_ref = it_ss->first;
            } else //opening novel exon
            {
                if (curr_start_ref >= 0) //ref exon is already open
                {
                    assert(curr_start_ref == -1 || it_ss->first >= curr_start_ref);
                    assert(prev_end_novel == -1 || it_ss->first > prev_end_novel);
                    int del = it_ss->first - std::max(curr_start_ref, prev_end_novel); //could be 0
                    frame = (frame - del) % 3;
                    if (frame < 0) {
                        frame += 3;
                    }                   
                    if (del > 0) {
                        //cerr << "del " << del << endl;
                        int pos = curr_start_ref >= prev_end_novel ? total_ref + 1 : total_ref + prev_end_novel - curr_start_ref + 1;
                        if(!reverse)
                            indel_seq.push_back(std::make_pair(false, std::make_pair(del, pos)));
                        else {
                            assert(pos-1 + del <= rev_tot_ref_length);
                            indel_seq.push_back(std::make_pair(false, std::make_pair(del, rev_tot_ref_length - (pos-1) - del)));
                        }
                    }
                }
                curr_start_novel = it_ss->first;
            }
        } else //end site
        {
            if (is_ref) //closing reference exon
            {
                if ( curr_start_novel < 0) //no novel exon currently open
                {
                    assert(curr_start_ref == -1 || it_ss->first > curr_start_ref);
                    assert(prev_end_novel == -1 || it_ss->first >= prev_end_novel);
                    int del = it_ss->first - std::max(curr_start_ref, prev_end_novel);
                    frame = (frame - del) % 3;
                    if (frame < 0) {
                        frame += 3;
                    }                    
                    if (del > 0) {
                        //cerr << "del " << del << endl;
                        int pos = curr_start_ref >= prev_end_novel ? total_ref + 1 : total_ref + prev_end_novel - curr_start_ref + 1;
                        if(!reverse)
                            indel_seq.push_back(std::make_pair(false, std::make_pair(del, pos)));
                        else {
                            assert(pos-1 + del <= rev_tot_ref_length);
                            indel_seq.push_back(std::make_pair(false, std::make_pair(del, rev_tot_ref_length - (pos-1) - del)));
                        }
                    }
                }
                total_ref += it_ss->first - curr_start_ref;
                curr_start_ref = -1;
                prev_end_ref = it_ss->first;
            } else //closing novel exon
            {
                if ( curr_start_ref < 0) //no reference exon currently open
                {
                    assert(curr_start_novel == -1 || it_ss->first > curr_start_novel);
                    assert(prev_end_ref == -1 || it_ss->first >= prev_end_ref);
                    unsigned ins = it_ss->first - std::max(curr_start_novel, prev_end_ref);
                    frame = (frame + ins) % 3;                                       
                    if (ins > 0) {
                        //cerr << "ins " << ins << endl;
                        if(!reverse)
                            indel_seq.push_back(std::make_pair(true, std::make_pair(ins, total_ref)));
                        else {
                            assert(rev_tot_ref_length >= total_ref);
                            indel_seq.push_back(std::make_pair(true, std::make_pair(ins, rev_tot_ref_length - total_ref)));
                        }
                    }
                }
                curr_start_novel = -1;
                prev_end_novel = it_ss->first;
            }
        }
    }
    std::stringstream res_ss;
    if(!reverse) {
        frame = 0;
        vector<pair<bool, pair<int,int> > >::const_iterator it_indel = indel_seq.begin();
        for(; it_indel != indel_seq.end(); ++it_indel) {
            if (it_indel->first) { //insertion
                frame = (frame + it_indel->second.first) % 3;
                res_ss << "I" << it_indel->second.first << "_" << it_indel->second.second << "^" << frame;
                //res_ss << "I" << it_indel->second.first << "^" << frame;
                //cerr << "I" << it_indel->second.first << "_" << it_indel->second.second << "^" << frame;
            } else { // deletion
                frame = (frame - it_indel->second.first) % 3;
                if (frame < 0) {
                    frame += 3;
                }
                res_ss << "D" << it_indel->second.first <<  "_" << it_indel->second.second << "^" << frame;
                //res_ss << "D" << it_indel->second.first << "^" << frame;
                //cerr << "D" << it_indel->second.first <<  "_" << it_indel->second.second << "^" << frame;
            }
        }
        return res_ss.str();
    }
    else
    {
        frame = 0;
        vector<pair<bool, pair<int,int> > >::const_reverse_iterator it_indel = indel_seq.rbegin();
        for(; it_indel != indel_seq.rend(); ++it_indel) {
            if (it_indel->first) { //insertion
                frame = (frame + it_indel->second.first) % 3;
                res_ss << "I" << it_indel->second.first << "_" << it_indel->second.second << "^" << frame;
                //res_ss << "I" << it_indel->second.first << "^" << frame;
            } else { // deletion
                frame = (frame - it_indel->second.first) % 3;
                if (frame < 0) {
                    frame += 3;
                }
                res_ss << "D" << it_indel->second.first << "_" << it_indel->second.second << "^" << frame;
                //res_ss << "D" << it_indel->second.first << "^" << frame;
            }
        }
        return res_ss.str();
    }
}


bool NovelTrans::anno_ss_order(const pair<unsigned, pair<bool,bool> >& anno_ss_1, const pair<unsigned, pair<bool,bool> >& anno_ss_2)
{    
    if (anno_ss_1.first == anno_ss_2.first) {
        return !anno_ss_1.second.second && anno_ss_2.second.second; //end sites before start sites, since half open intervals
    }
    return anno_ss_1.first < anno_ss_2.first;
}

/*
 * First cluster by intron chain. Initially create Isoform with same intron chain (if not single exon) and leftmost/rightmost tss/tes.
 * Then, if truncated on 3' or 5' wrt to gene_tss/gene_tes, extend, provided leftmost/rightmost does not lie in intron. Extend through 
 * current exon and all exons to come. Skip UTR until annotated start codon hit, start translating. Outputs only all unique protein sequences
 * in the end.
 * Assumes that Iso nr for (header) is located in novel_ic - second - front.
 */
void NovelTrans::print_uniq_protseq(string chr, unsigned ctg, unsigned gene, unsigned start_codon, isoforms_t& novel_ic, bool novel, bool single_exon_iso,
                                    const NamedDnaSequence& ref_sequence, unsigned& prot_nr, std::ofstream& protseq_file, std::ofstream& as_rna_file,
                                    set<unsigned>& broken_start_codon, set<unsigned>& no_end_codon, std::map<unsigned, string>& protseq_diff) const
{
    vector<pair<unsigned, unsigned> > gene_exons;
    gtf_reader.get_exons(gene_exons, ctg, gene);
    bool gene_on_rv = gtf_reader.is_gene_on_rv(ctg, gene);
    unsigned gene_tss = gtf_reader.getTss(ctg, gene);
    unsigned gene_tes = gtf_reader.getTes(ctg, gene);
   
    //debugging output
    /*
     if(gene_on_rv)
     cerr << "gene (reverse): " << endl;
     else
     cerr << "gene: " << endl;
     for(vector<pair<unsigned,unsigned> >::const_iterator it_d = gene_exons.begin(); it_d != gene_exons.end(); ++it_d)
     {
     cerr << "[" << it_d->first << "," << it_d->second << "]" << " - ";
     }
     cerr << endl;
     cerr << "start codon: " << start_codon << endl;
    */

    //go through all novel intron chain clusters and translate into proteins
    vector<pair<string, vector<unsigned> > > uniq_protseq; //better implemented by a set instead of a vector
    vector<vector<intronchain_cnt_t> > ic_clusters;
    cluster_by_ic(novel_ic, ic_clusters);
    vector<vector<intronchain_cnt_t> >::const_iterator it_cluster = ic_clusters.begin();
    for(; it_cluster != ic_clusters.end(); ++it_cluster)
    {
        //cerr << "next cluster: " << endl;
        assert(!it_cluster->empty());
        vector<intronchain_cnt_t>::const_iterator it_ic = it_cluster->begin();
        //std::stringstream ss;
        //ss << "P" << prot_nr;
        
        //debugging output
        /*
         vector<pair<unsigned,unsigned> > debug_exons;
         get_ic_exons(it_ic->first,debug_exons);
         for(vector<pair<unsigned,unsigned> >::const_iterator it_d = debug_exons.begin(); it_d != debug_exons.end(); ++it_d)
         {
         cerr << "[" << it_d->first << "," << it_d->second << "]" << " - ";
         }
         cerr << endl;
        */
        vector<unsigned> corr_iso;
        corr_iso.push_back(it_ic->second->front());
        
        unsigned leftmost = it_ic->first.front().second;
        unsigned rightmost = it_ic->first.back().first;
        //ss << "|Iso_" << it_ic->second->front();
        //determine leftmost tss and rightmost tes       
        for(it_ic++; it_ic != it_cluster->end(); ++it_ic)
        {
            //debugging output
            /*
             debug_exons.clear();
             get_ic_exons(it_ic->first,debug_exons);
             for(vector<pair<unsigned,unsigned> >::const_iterator it_d = debug_exons.begin(); it_d != debug_exons.end(); ++it_d)
             {
             cerr << "[" << it_d->first << "," << it_d->second << "]" << " - ";
             }
             cerr << endl;
            */
            
            if(it_ic->first.front().second < leftmost)
                leftmost = it_ic->first.front().second;
            if(it_ic->first.back().first > rightmost)
                rightmost = it_ic->first.back().first;
            
            //ss << ",Iso_" << it_ic->second->front();
            corr_iso.push_back(it_ic->second->front());
        }
        //cerr << "leftmost: " << leftmost << endl;
        //cerr << "rightmost: " << rightmost << endl;
        intronchain_t virt_iso_exons;  //exon chain that will be translated
        
        if (single_exon_iso) {            
            virt_iso_exons.push_back(std::make_pair(leftmost, rightmost));
        } else
        {
            get_ic_exons(it_cluster->begin()->first, virt_iso_exons); //exon chain of first isoform in cluster
            //cerr << "exon chain: " << virt_iso_exons.size() << endl;

            //extend tss/tes to leftmost/rightmost
            virt_iso_exons.front().first = leftmost;
            virt_iso_exons.back().second = rightmost;
        }
        
        //if truncated 3' (with resepect to start codon on reverse strand) extend till end of annotated transcript (UTR cut off later if reverse strand)
        if((!gene_on_rv && rightmost < gene_tes+1) ||  //rightmost is excluding, gene_tes including
           (gene_on_rv && rightmost <= start_codon))
        {
            //find annotated exon containing rightmost
            vector<pair<unsigned,unsigned> >::const_iterator it_gene_exons = gene_exons.begin();
            while(it_gene_exons != gene_exons.end() && it_gene_exons->second < rightmost)
                it_gene_exons++;
            assert(it_gene_exons != gene_exons.end());
            if(it_gene_exons->first < rightmost) //rightmost contained in (current) annotated exon
            {
                virt_iso_exons.back().second = it_gene_exons->second; //extend last exon in isoform
                it_gene_exons++;
                if(it_gene_exons != gene_exons.end()) {
                    virt_iso_exons.insert(virt_iso_exons.end(), it_gene_exons, gene_exons.cend()); //append remaining annotated exons (if any)
                }
            }
            //debuggin output
            /*
             cerr << "after extension at 3': " << endl;
             for(vector<pair<unsigned,unsigned> >::const_iterator it_d = virt_iso_exons.begin(); it_d != virt_iso_exons.end(); ++it_d)
             {
             cerr << "[" << it_d->first << "," << it_d->second << "]" << " - ";
             }
             cerr << endl;
            */
        }
        
        //if trancated 5' (with respect to start codon on forward strand) extend till start of annotated transcript (UTR cut off later)
        if( (!gene_on_rv && leftmost > start_codon) ||
           (gene_on_rv && leftmost > gene_tss) )
        {
            //find annotated exon containing leftmost
            vector<pair<unsigned,unsigned> >::const_iterator it_gene_exons = gene_exons.begin();
            while(it_gene_exons != gene_exons.end() && it_gene_exons->second <= leftmost)
                it_gene_exons++;
            if (it_gene_exons != gene_exons.end() && it_gene_exons->first <= leftmost) { //leftmost contained in (current) annotated exon
                virt_iso_exons.front().first = it_gene_exons->first; //extend first exon in isoform
                virt_iso_exons.insert(virt_iso_exons.begin(), gene_exons.cbegin(), it_gene_exons); //append remaining annotated exons to the left
            }
            //debuggin output
            /*
             cerr << "after extension at 5': " << endl;
             for(vector<pair<unsigned,unsigned> >::const_iterator it_d = virt_iso_exons.begin(); it_d != virt_iso_exons.end(); ++it_d)
             {
             cerr << "[" << it_d->first << "," << it_d->second << "]" << " - ";
             }
             cerr << endl;
            */
        }
        
        //ready to translate
        string mrna = "";
        string protseq = "";
        if(!gene_on_rv)
        {
            bool valid_sc = build_mrna_fw(virt_iso_exons, ref_sequence, start_codon, mrna);
            
            if (novel && mrna.size()>=9)
            {
                std::stringstream ss;
                ss << ">NAS" << "|";
                vector<unsigned>::const_iterator it_corr_iso = corr_iso.begin();
                for (; it_corr_iso != corr_iso.end(); ++it_corr_iso) {
                    if (it_corr_iso != corr_iso.begin())
                        ss << ",";
                    ss << "Iso_";
                    if (single_exon_iso)
                        ss << "se_";
                    ss << *it_corr_iso;
                }
                as_rna_file << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(ctg, gene);
                as_rna_file << endl;
                as_rna_file << mrna << endl;
            }
            
            int longest_orf = get_max_orf(chr, virt_iso_exons, gene_on_rv, ref_sequence);
            if(valid_sc) {
                protseq = translate(mrna);
                if (protseq.size()==0)
                    no_end_codon.insert(corr_iso.begin(), corr_iso.end()); //or empty protein (just start and stop codon)
            } else {
                broken_start_codon.insert(corr_iso.begin(), corr_iso.end());
                //cerr << "longest orf: " << longest_orf << endl;
            }
            
            //if protein sequence produced from isoform class compare to reference
            if (protseq.size()>0 && novel) {
                unsigned stop_codon = getStopCodonPos(virt_iso_exons, protseq.size(), start_codon);
                //cerr << start_codon << " " << stop_codon << endl;
                unsigned ref_stop_codon = ref_stop_codons.at(ctg).at(gene);
                string diff = compare_protseq(gene_exons, virt_iso_exons, start_codon, stop_codon, ref_stop_codon, false);
                //add offset of stop codon to diff string
                std::stringstream ss;
                ss << diff;
                if(diff.size()>0)
                {
                    ss << "=";
                    if(ref_stop_codon > stop_codon)
                        ss << ref_stop_codon - stop_codon << "-";
                    else
                        ss << stop_codon - ref_stop_codon << "+";
                }
                for (vector<unsigned>::const_iterator it_iso = corr_iso.begin(); it_iso != corr_iso.end(); ++it_iso) {
                    assert( protseq_diff.find(*it_iso) == protseq_diff.end() );
                    //cout << *it_iso << endl;
                    protseq_diff[*it_iso] = ss.str();
                }
            }

            /*
             if(protseq.size()>0) {
             protseq_file << ">" << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(it_ctg->first, it_gene->first) << endl;
             //for (unsigned i = 0; i < protseq.length(); i += 80) {
             //    protseq_file << protseq.substr(i, 80) << endl;
             //}
             protseq_file << protseq << endl;
             }*/
        } else
        {
            bool valid_sc = build_mrna_rv(virt_iso_exons, ref_sequence, start_codon, mrna);
            
            if (novel && mrna.size()>=9)
            {
                std::stringstream ss;
                ss << ">NAS" << "|";
                vector<unsigned>::const_iterator it_corr_iso = corr_iso.begin();
                for (; it_corr_iso != corr_iso.end(); ++it_corr_iso) {
                    if (it_corr_iso != corr_iso.begin())
                        ss << ",";
                    ss << "Iso_";
                    if (single_exon_iso)
                        ss << "se_";
                    ss << *it_corr_iso;
                }
                as_rna_file << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(ctg, gene);
                as_rna_file << endl;
                as_rna_file << mrna << endl;
            }
            
            int longest_orf = get_max_orf(chr, virt_iso_exons, gene_on_rv, ref_sequence);
            if(valid_sc) {
                protseq = translate(mrna);
                if (protseq.size()==0)
                    no_end_codon.insert(corr_iso.begin(), corr_iso.end()); //or empty protein (just start and stop codon)
            } else {
                broken_start_codon.insert(corr_iso.begin(), corr_iso.end());
                //cerr << "longest orf: " << longest_orf << endl;
            }

            //if protein sequence produced from isoform class compare to reference
            if (protseq.size()>0 && novel) {
                unsigned stop_codon = getStopCodonPosRev(virt_iso_exons, protseq.size(), start_codon);
                unsigned ref_stop_codon = ref_stop_codons.at(ctg).at(gene);
                string diff = compare_protseq(gene_exons, virt_iso_exons, start_codon, stop_codon, ref_stop_codons.at(ctg).at(gene), true);
                //add offset of stop codon to diff string
                std::stringstream ss;
                ss << diff;
                if(diff.size()>0)
                {
                    ss << "=";
                    if(ref_stop_codon >= stop_codon)
                        ss << ref_stop_codon - stop_codon << "+";
                    else
                        ss << stop_codon - ref_stop_codon << "-";
                }
                for (vector<unsigned>::const_iterator it_iso = corr_iso.begin(); it_iso != corr_iso.end(); ++it_iso) {
                    assert( protseq_diff.find(*it_iso) == protseq_diff.end() );
                    protseq_diff[*it_iso] = ss.str();
                }
            }
            /*
             if(protseq.size()>0) {
             protseq_file << ">" << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(it_ctg->first, it_gene->first) << endl;
             //for (unsigned i = 0; i < protseq.length(); i += 80) {
             //    protseq_file << protseq.substr(i, 80) << endl;
             //}
             protseq_file << protseq << endl;
             }*/
        }
        //prot_nr++;
        
        if(protseq.size()>0) {
            vector<pair<string, vector<unsigned> > >::iterator it_protseq;
            bool found = false;
            for (it_protseq = uniq_protseq.begin(); it_protseq != uniq_protseq.end(); ++it_protseq) {
                //if identical protein seq found, just add corresponding isoforms to list
                if (protseq==it_protseq->first) {
                    it_protseq->second.insert(it_protseq->second.end(), corr_iso.begin(), corr_iso.end());
                    found = true;
                    break;
                }
            }
            if (!found) { //append new protein sequence
                uniq_protseq.push_back(std::make_pair(protseq, corr_iso));
            }
        }
    } //for it_cluster
    
    string protseq = "";
    string mrna = "";
    bool valid_sc;
    if (!gene_on_rv) {
        valid_sc = build_mrna_fw(gene_exons, ref_sequence, start_codon, mrna);
    } else {
        valid_sc = build_mrna_rv(gene_exons, ref_sequence, start_codon, mrna);
    }
    if (valid_sc) {
        protseq = translate(mrna);
    }
    
    if (uniq_protseq.size()>0) {
        std::stringstream ss;
        ss << ">AP" << prot_nr;
        protseq_file << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(ctg, gene) << endl;
        protseq_file << protseq << endl;
    }
    
    //output all unique protein sequences for gene
    vector<pair<string, vector<unsigned> > >::iterator it_protseq;
    for (it_protseq = uniq_protseq.begin(); it_protseq != uniq_protseq.end(); ++it_protseq)
    {
        std::stringstream ss;
        ss << ">P" << prot_nr << "|";
        vector<unsigned>::const_iterator it_corr_iso = it_protseq->second.begin();
        for (; it_corr_iso != it_protseq->second.end(); ++it_corr_iso) {
            if (it_corr_iso != it_protseq->second.begin())
                ss << ",";
            ss << "Iso_";
            if (single_exon_iso)
                ss << "se_";
            ss << *it_corr_iso;
        }
        protseq_file << ss.str() << "|" << chr << "|" << gtf_reader.getGeneName(ctg, gene);
        if(!novel)
            protseq_file << " - no novel splicing";
        protseq_file << endl;
        protseq_file << it_protseq->first << endl;
        prot_nr++;
    }   
}

//return true iff start codon is ATG
bool NovelTrans::build_mrna_fw(intronchain_t virt_iso_exons, const NamedDnaSequence& ref_sequence, unsigned start_codon, string& mrna) const
{
    mrna = "";
    bool hit_start_codon = false;
    vector<pair<unsigned,unsigned> >::const_iterator it_virt_iso_ex;
    for(it_virt_iso_ex = virt_iso_exons.begin(); it_virt_iso_ex != virt_iso_exons.end(); ++it_virt_iso_ex)
    {
        if(it_virt_iso_ex->second <= start_codon) //skip UTR
            continue;
        if(it_virt_iso_ex->first <= start_codon) {
            hit_start_codon = true;
            mrna += ref_sequence.substr(start_codon, it_virt_iso_ex->second);
        } else if(hit_start_codon)
        {
            mrna += ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second);
        }
    }
    if(mrna.substr(0,3)=="ATG") {
        return true;
    } else
        return false;
}


//return true iff start codon is ATG
bool NovelTrans::build_mrna_rv(intronchain_t virt_iso_exons, const NamedDnaSequence& ref_sequence, unsigned start_codon, string& mrna) const
{
    mrna = "";
    bool hit_start_codon = false;
    vector<pair<unsigned,unsigned> >::const_reverse_iterator it_virt_iso_ex;
    for(it_virt_iso_ex = virt_iso_exons.rbegin(); it_virt_iso_ex != virt_iso_exons.rend(); ++it_virt_iso_ex)
    {
        if(it_virt_iso_ex->first > start_codon) //skip UTR
            continue;
        if(it_virt_iso_ex->second > start_codon) {
            hit_start_codon = true;
            mrna += getRevCompl(ref_sequence.substr(it_virt_iso_ex->first, start_codon+1));
        } else if(hit_start_codon)
        {
            mrna += getRevCompl(ref_sequence.substr(it_virt_iso_ex->first, it_virt_iso_ex->second));
        }
    }
    if(mrna.substr(0,3)=="ATG") {
        return true;
    } else
        return false;
}


//start codon: first (5') base of (untranslated) start codon
//returns: first (5') untranslated base
unsigned NovelTrans::getStopCodonPosRev(intronchain_t exon_chain, unsigned prot_length, unsigned start_codon) const
{
    intronchain_t::const_reverse_iterator it_exch;
    bool hit_start_codon = false;
    int mrna_length = 0;
    for (it_exch = exon_chain.rbegin(); it_exch != exon_chain.rend(); ++it_exch)
    {
        if(it_exch->first > start_codon) //skip UTR
            continue;
        if(it_exch->second > start_codon) {
            hit_start_codon = true;
            int add_mrna = start_codon - it_exch->first + 1 - 3;
            if (add_mrna >= prot_length * 3) {
                return start_codon - 3 - (prot_length*3);
            }
            mrna_length += add_mrna;
        } else
        {
            assert(hit_start_codon);
            unsigned add_mrna = it_exch->second - it_exch->first;
            if ( (mrna_length + add_mrna) >= prot_length * 3) {
                unsigned result = it_exch->second - (prot_length*3) + mrna_length - 1;
                assert(result >= it_exch->first - 1);
                return result;
            }
            mrna_length += add_mrna;
        }
    }
}

//start codon: first (5') base of (untranslated) start codon
//returns: first (5') untranslated base
unsigned NovelTrans::getStopCodonPos(intronchain_t exon_chain, unsigned prot_length, unsigned start_codon) const
{
    intronchain_t::const_iterator it_exch;
    bool hit_start_codon = false;
    int mrna_length = 0; //can be negative
    for (it_exch = exon_chain.begin(); it_exch != exon_chain.end(); ++it_exch)
    {
        if(it_exch->second <= start_codon) //skip UTR
            continue;
        if(it_exch->first <= start_codon) {
            hit_start_codon = true;
            int add_mrna = it_exch->second - start_codon - 3; //could be negative if not complete start codon
            if (add_mrna >= prot_length * 3) {
                return start_codon + 3 + (prot_length*3);
            }
            mrna_length += add_mrna;
        } else
        {
            assert(hit_start_codon);
            unsigned add_mrna = it_exch->second - it_exch->first;
            if ( (mrna_length + add_mrna) >= prot_length * 3) {
                return it_exch->first + (prot_length*3)-mrna_length;
            }
            mrna_length += add_mrna;
        }
    }
}



/*
 * helper function
 */
string NovelTrans::getComplBase(char base) const
{
    switch(base) {
        case 'A': return "T";
        case 'T': return "A";
        case 'G': return "C";
        case 'C': return "G";
        default: return "N";
    }
}

string NovelTrans::getRevCompl(string dna) const
{
    string result = "";
    string::const_reverse_iterator it = dna.rbegin();
    for (; it!=dna.rend(); ++it) {
        result += getComplBase(*it);
    }
    return result;
}

string NovelTrans::translate(string dna) const
{        
    if(dna.size()<6) return "";

    if(dna.substr(0,3)!="ATG")
        return "";
        //cout << dna.substr(0,3) << endl;

    string result = "";
    size_t start = 3;
    while(start+2 <= dna.size()-1)
    {
        char aa = gencode(dna.substr(start,3));
        if(aa=='*') return result;
        result += aa;
        start += 3;
    }
    return "";
}

char NovelTrans::gencode(string dna) const
{
    assert(dna.size()==3);
    if(dna=="TTT") return 'F';
    if(dna=="TTC") return 'F';
    
    if(dna=="TTA") return 'L';
    if(dna=="TTG") return 'L';
    if(dna=="CTT") return 'L';
    if(dna=="CTC") return 'L';
    if(dna=="CTA") return 'L';
    if(dna=="CTG") return 'L';
    
    if(dna=="ATT") return 'I';
    if(dna=="ATC") return 'I';
    if(dna=="ATA") return 'I';
    
    if(dna=="ATG") return 'M';
    
    if(dna=="GTT") return 'V';
    if(dna=="GTC") return 'V';
    if(dna=="GTA") return 'V';
    if(dna=="GTG") return 'V';
    
    if(dna=="TCT") return 'S';
    if(dna=="TCC") return 'S';
    if(dna=="TCA") return 'S';
    if(dna=="TCG") return 'S';
    
    if(dna=="CCT") return 'P';
    if(dna=="CCC") return 'P';
    if(dna=="CCA") return 'P';
    if(dna=="CCG") return 'P';
    
    if(dna=="ACT") return 'T';
    if(dna=="ACC") return 'T';
    if(dna=="ACA") return 'T';
    if(dna=="ACG") return 'T';
    
    if(dna=="GCT") return 'A';
    if(dna=="GCC") return 'A';
    if(dna=="GCA") return 'A';
    if(dna=="GCG") return 'A';
    
    if(dna=="TAT") return 'Y';
    if(dna=="TAC") return 'Y';
    
    if(dna=="CAT") return 'H';
    if(dna=="CAC") return 'H';
    
    if(dna=="CAA") return 'Q';
    if(dna=="CAG") return 'Q';
    
    if(dna=="AAT") return 'N';
    if(dna=="AAC") return 'N';
    
    if(dna=="AAA") return 'K';
    if(dna=="AAG") return 'K';
    
    if(dna=="GAT") return 'D';
    if(dna=="GAC") return 'D';
    
    if(dna=="GAA") return 'E';
    if(dna=="GAG") return 'E';
    
    if(dna=="TGT") return 'C';
    if(dna=="TGC") return 'C';
    
    if(dna=="TGG") return 'W';
    
    if(dna=="CGT") return 'R';
    if(dna=="CGC") return 'R';
    if(dna=="CGA") return 'R';
    if(dna=="CGG") return 'R';
    
    if(dna=="AGT") return 'S';
    if(dna=="AGC") return 'S';
    
    if(dna=="AGA") return 'R';
    if(dna=="AGG") return 'R';
    
    if(dna=="GGT") return 'G';
    if(dna=="GGC") return 'G';
    if(dna=="GGA") return 'G';
    if(dna=="GGG") return 'G';
    
    if(dna=="TAA") return '*';
    if(dna=="TAG") return '*';
    if(dna=="TGA") return '*';
    
    assert(false);
}

string NovelTrans::get_dimer(const std::pair<unsigned, unsigned>& intron, const NamedDnaSequence& ref_sequence, bool on_reverse) const
{
    if(intron.first + 3 >= intron.second) //intron length < 4
        return "";
    
    string leftdimer = "";
    string rightdimer = "";
    if(!on_reverse) //forward strand
    {
        leftdimer += ref_sequence[intron.first];
        leftdimer += ref_sequence[intron.first+1];
        rightdimer += ref_sequence[intron.second-2];
        rightdimer += ref_sequence[intron.second-1];
    } else if(on_reverse) //reverse strand
    {
        leftdimer += getComplBase(ref_sequence[intron.second-1]);
        leftdimer += getComplBase(ref_sequence[intron.second-2]);
        rightdimer += getComplBase(ref_sequence[intron.first+1]);
        rightdimer += getComplBase(ref_sequence[intron.first]);
    }
    return leftdimer + rightdimer;
}

string NovelTrans::get_hexamers(const std::pair<unsigned, unsigned>& intron, const NamedDnaSequence& ref_sequence, bool on_reverse) const
{
    if(intron.first + 11 >= intron.second) //intron length < 12
        return "";

    string leftdimer = "";
    string rightdimer = "";
    if(!on_reverse) //forward strand
    {
        leftdimer += ref_sequence[intron.first];
        leftdimer += ref_sequence[intron.first+1];
        leftdimer += ref_sequence[intron.first+2];
        leftdimer += ref_sequence[intron.first+3];
        leftdimer += ref_sequence[intron.first+4];
        leftdimer += ref_sequence[intron.first+5];
        rightdimer += ref_sequence[intron.second-6];
        rightdimer += ref_sequence[intron.second-5];
        rightdimer += ref_sequence[intron.second-4];
        rightdimer += ref_sequence[intron.second-3];
        rightdimer += ref_sequence[intron.second-2];
        rightdimer += ref_sequence[intron.second-1];
    } else if(on_reverse) //reverse strand
    {
        leftdimer += getComplBase(ref_sequence[intron.second-1]);
        leftdimer += getComplBase(ref_sequence[intron.second-2]);
        leftdimer += getComplBase(ref_sequence[intron.second-3]);
        leftdimer += getComplBase(ref_sequence[intron.second-4]);
        leftdimer += getComplBase(ref_sequence[intron.second-5]);
        leftdimer += getComplBase(ref_sequence[intron.second-6]);
        rightdimer += getComplBase(ref_sequence[intron.first+5]);
        rightdimer += getComplBase(ref_sequence[intron.first+4]);
        rightdimer += getComplBase(ref_sequence[intron.first+3]);
        rightdimer += getComplBase(ref_sequence[intron.first+2]);
        rightdimer += getComplBase(ref_sequence[intron.first+1]);
        rightdimer += getComplBase(ref_sequence[intron.first]);
    }
    return leftdimer + rightdimer;
}

bool NovelTrans::is_canonical(const std::pair<unsigned, unsigned>& intron, const NamedDnaSequence& ref_sequence, bool on_reverse) const
{
    if(intron.first + 3 >= intron.second) //intron length < 4
        return false;

    string leftdimer = "";
    string rightdimer = "";
    if(!on_reverse) //forward strand
    {
        leftdimer += ref_sequence[intron.first];
        leftdimer += ref_sequence[intron.first+1];
        rightdimer += ref_sequence[intron.second-2];
        rightdimer += ref_sequence[intron.second-1];
    } else if(on_reverse) //reverse strand
    {
        leftdimer += getComplBase(ref_sequence[intron.second-1]);
        leftdimer += getComplBase(ref_sequence[intron.second-2]);
        rightdimer += getComplBase(ref_sequence[intron.first+1]);
        rightdimer += getComplBase(ref_sequence[intron.first]);
    }
    //cerr << leftdimer << " " << rightdimer << endl;
    if(leftdimer == "GT" && rightdimer == "AG")
        return true;
    else
        return false;
}

/*
 * returns flanking dimers of given intron
 */
std::pair<string, string> NovelTrans::get_intron_dimers(const std::string& ref_name,
                                                        const std::pair<unsigned,unsigned> intron) const
{
    std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it;
    it = reference_sequences->find(ref_name);
    if(it == reference_sequences->end()) {
        //cerr << "Reference \"" << ref_name << "\" missing." << endl;
        return std::pair<string,string>("XX","XX");
    }
    
    const NamedDnaSequence& ref_sequence = *it->second;
    
    string leftdimer = "";
    string rightdimer = "";
    if(intron.first + 3 < intron.second) //forward strand and intron length >= 4
    {
        leftdimer += ref_sequence[intron.first];
        leftdimer += ref_sequence[intron.first+1];
        rightdimer += ref_sequence[intron.second-2];
        rightdimer += ref_sequence[intron.second-1];
        //cout << leftdimer << " " << rightdimer << endl;
    } else if(intron.second + 3 < intron.first)
    {
        leftdimer += getComplBase(ref_sequence[intron.first-1]);
        leftdimer += getComplBase(ref_sequence[intron.first-2]);
        rightdimer += getComplBase(ref_sequence[intron.second+1]);
        rightdimer += getComplBase(ref_sequence[intron.second]);
        //cout << leftdimer << " " << rightdimer << endl;
    }
    return std::pair<string,string>(leftdimer,rightdimer);
}


/*
 * helper function checkes whether query strictly contains one
 * of the reference intervals
 */
void NovelTrans::contains_strictly(const vector<std::pair<unsigned,unsigned> >& reference, const std::pair<unsigned,unsigned> query, vector<unsigned>& certificates) const
{
    certificates.clear();
    vector<std::pair<unsigned,unsigned> >::const_iterator it_proxy;
    it_proxy = std::lower_bound(reference.begin(), reference.end(), query);
    if(it_proxy != reference.end())
    {
        while (it_proxy != reference.end() && it_proxy->first <= query.second)
        {
            if (it_proxy->first > query.first && it_proxy->second < query.second)
            {
                certificates.push_back(it_proxy-reference.begin());
            }
            it_proxy++;
        }
    }
}

/*
 * helper function checkes whether query interval is strictly contained in 
 * one of the reference intervals 
 */
bool NovelTrans::is_strictly_contained(const vector<std::pair<unsigned,unsigned> >& reference, const std::pair<unsigned,unsigned> query, unsigned& certificate_idx) const
{
    vector<std::pair<unsigned,unsigned> >::const_iterator it_ref = reference.begin();
    while (it_ref != reference.end() && it_ref->first < query.first)
    {
        if (it_ref->second > query.second) {
            //certificate.first = it_ref->first;
            //certificate.second = it_ref->second;
            certificate_idx = it_ref - reference.begin();
            return true;
        }
        it_ref++;
    }
    return false;
}


/*
 * Returns true iff query does not overlap any reference exon
 */
bool NovelTrans::is_exon_novel(const vector<std::pair<unsigned, unsigned> >& reference, const std::pair<unsigned, unsigned>& query) const
{
    //copy reference exons to appropriate format
    vector<std::pair<std::pair<unsigned, unsigned>, unsigned> > _reference;
    vector<std::pair<unsigned, unsigned> >::const_iterator it;
    unsigned i = 0;
    for(it=reference.begin(); it!=reference.end(); ++it, ++i)
    {
        _reference.push_back(make_pair(*it,i)); //could set i=0, but assertion would fail in get_all_overlap
    }
    vector<std::pair<unsigned,unsigned> > overlap_exons;
    get_all_overlap(_reference, query, overlap_exons);

    return overlap_exons.empty();
}


/*
 * Returns list of pairs. First elements denotes second element in overlapping reference (for example gene idx).
 * Second element denotes amount of overlap
 */
void NovelTrans::get_all_overlap(const vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >& reference, 
                 const std::pair<unsigned,unsigned>& query,
				 vector<std::pair<unsigned,unsigned> >& overlap_genes) const
{
  assert(overlap_genes.empty());

  //for debugging
  std::set<unsigned> genes;
   
  vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator it_ref = reference.begin();
  while (it_ref != reference.end() && it_ref->first.first < query.second) //while reference might overlap query (not too far right)
  {
    if(it_ref->first.second <= query.first) { //reference lies left of query, no overlap
      it_ref++;
      continue;
    }

    else if(it_ref->first.first <= query.first && it_ref->first.second < query.second) //gene overlaps (only) left read boundary
    {      
      int overlap = it_ref->first.second - query.first;
      //cerr << "overlap on left read boundary: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;
      
      if(overlap>0) {
	overlap_genes.push_back(std::make_pair(it_ref->second,overlap));  
	
	//debugging
	assert(genes.find(it_ref->second) == genes.end());
	genes.insert(it_ref->second);
      }
      
    } else if(it_ref->first.first >= query.first && it_ref->first.second <= query.second) //reference fully contained in query
    {
      int overlap = it_ref->first.second - it_ref->first.first;
      //cerr << "reference fully contained: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;

      if(overlap>0) {
	overlap_genes.push_back(std::make_pair(it_ref->second,overlap));        

	//debugging
	assert(genes.find(it_ref->second) == genes.end());
	genes.insert(it_ref->second);
      }
    } else if(it_ref->first.first > query.first && it_ref->first.second > query.second) //gene overlaps (only) right read boundary
    {
      int overlap = query.second - it_ref->first.first;
      //cerr << "overlap on right read boundary: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;

      if(overlap>0) {
	overlap_genes.push_back(std::make_pair(it_ref->second,overlap));       

	//debugging
	assert(genes.find(it_ref->second) == genes.end());
	genes.insert(it_ref->second);
      }
    } else if(it_ref->first.first <= query.first && it_ref->first.second >= query.second) //query fully contained
    {
      int overlap = query.second - query.first;
      //cerr << "query fully contained: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;
      
      if(overlap>0) {
	overlap_genes.push_back(std::make_pair(it_ref->second,overlap));

	//debugging
	assert(genes.find(it_ref->second) == genes.end());
	genes.insert(it_ref->second);
      }
    }
    it_ref++;
  }
}


int NovelTrans::get_sub_overlap(const vector<pair<unsigned,unsigned> >& query_intervals,
                                const vector<pair<unsigned,unsigned> >& reference_intervals) const
{
    int overlap = 0;
    vector<pair<unsigned,unsigned> >::const_iterator it_q = query_intervals.begin();
    for(; it_q != query_intervals.end(); ++it_q)
    {
        vector<pair<unsigned,unsigned> >::const_iterator it_ref = reference_intervals.begin();
        while (it_ref != reference_intervals.end() && it_ref->first < it_q->second)
        {
            if(it_ref->second <= it_q->first) {
                it_ref++;
                continue;
            }
            else if(it_ref->first <= it_q->first && it_ref->second < it_q->second) //reference overlaps (only) left query boundary
            {
                overlap += it_ref->second - it_q->first;
            } else if(it_ref->first >= it_q->first && it_ref->second <= it_q->second) //reference fully contained in query
            {
                overlap += it_ref->second - it_ref->first;
            } else if(it_ref->first > it_q->first && it_ref->second > it_q->second) //reference overlaps (only) right query boundary
            {
                overlap += it_q->second - it_ref->first;
            } else if(it_ref->first <= it_q->first && it_ref->second >= it_q->second) //query fully contained
            {
                overlap += it_q->second - it_q->first;
            }
            it_ref++;
        }
    }
    return overlap;
}



void NovelTrans::get_all_overlap(unsigned contig_id,
                                 std::map<unsigned, vector<std::pair<std::pair<unsigned,unsigned>,unsigned> > >& ctg_references,
                                 const std::pair<unsigned,unsigned>& query,
                                 const vector<pair<unsigned,unsigned> >& query_intervals,
                                 vector<std::pair<unsigned,unsigned> >& overlap_genes) const
{
  assert(overlap_genes.empty());
  const vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >& reference = ctg_references.at(contig_id);

  //for debugging
  std::set<unsigned> genes;

  vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator it_ref = reference.begin();
  while (it_ref != reference.end() && it_ref->first.first < query.second)
  {
      if(it_ref->first.second <= query.first) {
          it_ref++;
          continue;
      }
      else if(it_ref->first.first <= query.first && it_ref->first.second < query.second) //gene overlaps (only) left read boundary
      {
          vector<std::pair<unsigned,unsigned> > gene_exons;
          gtf_reader.get_exons(gene_exons, contig_id, it_ref->second);
          int overlap = get_sub_overlap(query_intervals, gene_exons);

          if(overlap>0) {
              overlap_genes.push_back(std::make_pair(it_ref->second,overlap));

              //debugging
              assert(genes.find(it_ref->second) == genes.end());
              genes.insert(it_ref->second);
          }

      } else if(it_ref->first.first >= query.first && it_ref->first.second <= query.second) //reference fully contained in query
      {
          vector<std::pair<unsigned,unsigned> > gene_exons;
          gtf_reader.get_exons(gene_exons, contig_id, it_ref->second);
          int overlap = get_sub_overlap(query_intervals, gene_exons);

          if(overlap>0) {
              overlap_genes.push_back(std::make_pair(it_ref->second,overlap));

              //debugging
              assert(genes.find(it_ref->second) == genes.end());
              genes.insert(it_ref->second);
          }
      } else if(it_ref->first.first > query.first && it_ref->first.second > query.second) //gene overlaps (only) right read boundary
      {
          vector<std::pair<unsigned,unsigned> > gene_exons;
          gtf_reader.get_exons(gene_exons, contig_id, it_ref->second);
          int overlap = get_sub_overlap(query_intervals, gene_exons);

          if(overlap>0) {
              overlap_genes.push_back(std::make_pair(it_ref->second,overlap));

              //debugging
              assert(genes.find(it_ref->second) == genes.end());
              genes.insert(it_ref->second);
          }
      } else if(it_ref->first.first <= query.first && it_ref->first.second >= query.second) //query fully contained
      {
          vector<std::pair<unsigned,unsigned> > gene_exons;
          gtf_reader.get_exons(gene_exons, contig_id, it_ref->second);
          int overlap = get_sub_overlap(query_intervals, gene_exons);

          if(overlap>0) {
              overlap_genes.push_back(std::make_pair(it_ref->second,overlap));

              //debugging
              assert(genes.find(it_ref->second) == genes.end());
              genes.insert(it_ref->second);
          }
      }
      it_ref++;
  }
}



int NovelTrans::get_max_overlap(const vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >& reference, 
				const std::pair<unsigned,unsigned>& query,
				vector<vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator>& max_overlap_genes) const
{
  assert(max_overlap_genes.empty());
  //cerr << endl << "get_max_overlap " << query.first << " " << query.second << endl;

  int max_overlap = 0;
  vector<std::pair<std::pair<unsigned,unsigned>,unsigned> >::const_iterator it_ref = reference.begin();
  while (it_ref != reference.end() && it_ref->first.first < query.second)
  {        
    if(it_ref->first.second <= query.first) {
      it_ref++;
      continue;
    }
    else if(it_ref->first.first <= query.first && it_ref->first.second < query.second) //gene overlaps (only) left read boundary
    {

      int overlap = it_ref->first.second - query.first;
      //cerr << "overlap on left read boundary: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;

      if(overlap > 0 && overlap == max_overlap) {
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
      else if(overlap > max_overlap) {
	max_overlap_genes.clear();
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
    } else if(it_ref->first.first >= query.first && it_ref->first.second <= query.second) //reference fully contained in query
    {
      int overlap = it_ref->first.second - it_ref->first.first;
      //cerr << "reference fully contained: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;

      if(overlap > 0 && overlap == max_overlap) {
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
      else if(overlap > max_overlap) {
	max_overlap_genes.clear();
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
    } else if(it_ref->first.first > query.first && it_ref->first.second > query.second) //gene overlaps (only) right read boundary
    {
      int overlap = query.second - it_ref->first.first;
      //cerr << "overlap on right read boundary: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;

      if(overlap > 0 && overlap == max_overlap) {	
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
      else if(overlap > max_overlap) {
	max_overlap_genes.clear();
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
    } else if(it_ref->first.first <= query.first && it_ref->first.second >= query.second) //query fully contained
    {
      int overlap = query.second - query.first;
      //cerr << "query fully contained: " << it_ref->first.first << " " << it_ref->first.second << " - " << overlap << endl;
      
      if(overlap > 0 && overlap == max_overlap) {
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }
      else if(overlap > max_overlap) {
	max_overlap_genes.clear();
	max_overlap_genes.push_back(it_ref);
	max_overlap = overlap;
      }      
    }
    it_ref++;
  }
  //cerr << "max overlap: " << max_overlap << " (" << max_overlap_genes.size() << ")" << endl;
  return max_overlap;
}
