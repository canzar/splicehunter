/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */


#include <boost/program_options.hpp>
#include <iostream>
#include <string>
#include <dirent.h>
#include <vector>

#include "NovelTrans.h"
#include "Parameters.h"

using std::string;
using std::cout;
using std::cerr;
using std::endl;
using std::vector;

namespace po = boost::program_options;


/*
 * when a directory is provided, bam files are recognized from ".bam",
 * trimming reports from "*trimrep*". If no trimming reports are provided
 * FORWARD direction is assumed (case: isoseq), unless recover_read_dirs()
 * is called. Otherwise (trimming reports exist) the number of
 * trimming reports must match the number of bam files. Reports/bams are matched
 * through their lexicographic order. If no directory is provided the alignments
 * are read from stdin.
 */

bool has_suffix(const string &str, const string &suffix)
{
    return str.size() >= suffix.size() && str.compare(str.size()-suffix.size(), suffix.size(), suffix) == 0;
}

int main(int argc, char* argv[])
{
    po::options_description desc("Allowed options");
    desc.add_options()
            ("help,h", "print help message")
            ("dir,m", po::value<string>(),             "joint analysis of all samples in given directory (required if alignments not in stdin)")
            ("gtf,g", po::value<string>()->required(), "annotation as gtf (required)")
            ("ref,f", po::value<string>(),             "reference sequence")                                                                     
            ("ewin,e", po::value<unsigned>(), "error-free splice site window")
            ("swin,w", po::value<unsigned>(), "snap erroneous novel splice site to annotated splice site within window")
            ("snap,s", po::value<unsigned>(), "snap read start/end sites to annotated TSS/TES if within snap distance")
            ("twin,t", po::value<unsigned>(), "snap read start/end sites to leftmost/rightmost start/end within window")
            ("uniq,u", po::value<float>(), "ambiguity resolving threshold")
            ("ovdiff,d", po::value<float>(), "min overlap on different strand")
            ("ovsame,o", po::value<float>(), "min overlap on same strand")
            ("fl,l", po::bool_switch()->default_value(false), "consider only FL reads")
            ("cert,c", po::bool_switch()->default_value(false), "output certificates")
            ("maplength,a", po::value<float>(), "minimum required fraction of aligned bases")
            ("mapqual,q", po::value<float>(), "minimum required fraction of identically aligned bases")
            ("protfile,p", po::value<string>(),             "output protein sequence to file")
            ("rnafile,r", po::value<string>(),              "output novel rna sequence to file")
            ("asfile,A", po::value<string>(),              "output AS rna sequence to file")
            ("hexamer,x", po::value<string>(),              "output intron hexamers to file")
            ("dimer,i", po::value<string>(),              "output intron dimers to file")
            ("retlength,n", po::value<string>(),              "output retained intron length to file")
            ("pwcount,j", po::value<string>(),              "output pairwise splicing event counts to file")
            ("ncprotseq,y", po::value<string>(),              "output longest ORFs of all annotated genes to file")
            ("annoint,z", po::value<string>(),              "output hexamers and length of annotated introns to file")
            ("iso,I", po::value<string>(),              "output isoforms to file")
    
    ;

    po::variables_map options;
    try {
        po::store(po::parse_command_line(argc, argv, desc), options);
        if (options.count("help")) {
            cout << desc << "\n";
            return 1;
        }
        po::notify(options);
    } catch(std::exception& e) {
        cerr << "Error: " << e.what() << "\n";
        return 1;
    }

    //options for assigning reads to genes
    if(options.count("ewin"))
        Parameters::ss_window = options["ewin"].as<unsigned>();
    if(options.count("swin"))
        Parameters::ss_snap_window = options["swin"].as<unsigned>();
    if(options.count("twin"))
        Parameters::tsstes_window = options["twin"].as<unsigned>();
    if(options.count("snap"))
        Parameters::snap_dist = options["snap"].as<unsigned>();
    if(options.count("uniq"))
        Parameters::ambig_res_thr = options["uniq"].as<float>();
    if(options.count("ovdiff"))
        Parameters::min_overlap_diff_strand = options["ovdiff"].as<float>();
    if(options.count("ovsame"))
        Parameters::min_overlap_same_strand = options["ovsame"].as<float>();
    
    if(options.count("mapqual"))
        Parameters::mapqual = options["mapqual"].as<float>();
    if(options.count("maplength"))
        Parameters::mapqual = options["maplength"].as<float>();
    
    if(options.count("protfile"))
        Parameters::protseq_file = options["protfile"].as<string>();
    if(options.count("rnafile"))
        Parameters::novel_rna_file = options["rnafile"].as<string>();
    if(options.count("asfile"))
        Parameters::as_rna_file = options["asfile"].as<string>();
    if(options.count("hexamer"))
        Parameters::hexamer_file = options["hexamer"].as<string>();
    if(options.count("dimer"))
        Parameters::dimer_file = options["dimer"].as<string>();
    if(options.count("retlength"))
        Parameters::retint_length_file = options["retlength"].as<string>();
    if(options.count("pwcount"))
        Parameters::pwcount_file = options["pwcount"].as<string>();
    if(options.count("ncprotseq"))
        Parameters::nc_anno_file = options["ncprotseq"].as<string>();
    if(options.count("annoint"))
        Parameters::anno_intron_file = options["annoint"].as<string>();
    if(options.count("iso"))
        Parameters::iso_file = options["iso"].as<string>();


    DIR *pdir = 0;
    dirent *pent = 0;
    try {
        //read bams from specified directory
        vector<string> bams;
        vector<string> batch;
        vector<string> sample;
        vector<string> bams_tmp;
        if(options.count("dir"))
        {
            pdir = opendir(options["dir"].as<string>().c_str());
            if(pdir == 0)
            {
                throw std::runtime_error("Error opening directory \".\"" );
            }
            while(pent = readdir(pdir))
            {
                if(has_suffix(pent->d_name, ".bam"))
                {
                    bams_tmp.push_back(string(pent->d_name));
                }
            }           
            sort(bams_tmp.begin(), bams_tmp.end());
            for (vector<string>::iterator it_b = bams_tmp.begin(); it_b != bams_tmp.end(); ++it_b)
            {
                bams.push_back(options["dir"].as<string>() + "/" + *it_b);
                
                size_t end = it_b->find_first_of("_");
                batch.push_back(it_b->substr(0,end));
                
                size_t start = it_b->find_last_of("_");
                end = it_b->find_first_of(".");
                sample.push_back(it_b->substr(start+1,end-start-1));
            }
            cerr << "Read " << bams.size() << " bams from " << options["dir"].as<string>() << endl;
        }

        if(!options.count("dir"))
            bams.push_back(string("/dev/stdin"));

        string ref = "";
        if(options.count("ref"))
            ref = options["ref"].as<string>();
        
        NovelTrans nt(options["gtf"].as<string>().c_str(), bams, &batch, &sample, ref);
        
        //nt.snp_stats();

        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoforms_t, NovelTrans::isoforms_t> > > isoforms_per_gene;
        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoforms_t, NovelTrans::isoforms_t> > > merged_isoforms_per_gene;
        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoforms_t, NovelTrans::isoforms_t> > > intret_chain_per_gene;
        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoforms_t, NovelTrans::isoforms_t> > > merged_intret_chain_per_gene;
        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoform_reads_t, NovelTrans::isoform_reads_t> > > isoform_reads_per_gene;
        std::map<unsigned, std::map<unsigned, std::pair<NovelTrans::isoform_reads_t, NovelTrans::isoform_reads_t> > > merged_isoform_reads_per_gene;
        if(options["cert"].as<bool>()) {
            nt.reads2isoforms(isoforms_per_gene, intret_chain_per_gene, options["fl"].as<bool>(), &isoform_reads_per_gene);
            nt.mergeTssTes(isoforms_per_gene, merged_isoforms_per_gene, Parameters::snap_dist, Parameters::tsstes_window, &isoform_reads_per_gene, &merged_isoform_reads_per_gene);
        }
        else {
            nt.reads2isoforms(isoforms_per_gene, intret_chain_per_gene, options["fl"].as<bool>());
            nt.mergeTssTes(isoforms_per_gene, merged_isoforms_per_gene, Parameters::snap_dist, Parameters::tsstes_window);
        }
        nt.mergeTssTes(intret_chain_per_gene, merged_intret_chain_per_gene, Parameters::snap_dist, Parameters::tsstes_window);

        if(options["cert"].as<bool>()) {
            nt.all_events_per_isoform(merged_isoforms_per_gene, merged_intret_chain_per_gene, &merged_isoform_reads_per_gene);
        } else
            nt.all_events_per_isoform(merged_isoforms_per_gene, merged_intret_chain_per_gene);

    } catch(std::exception& e) {
        std::cerr << "Error: " << e.what() << "\n";
        return 1;
    }
    return 0;
}
