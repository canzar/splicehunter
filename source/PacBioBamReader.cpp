/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include <sstream>
#include <stdexcept>

#include "PacBioBamReader.h"

using std::cerr;
using std::cout;
using std::endl;

PacBioBamReader::PacBioBamReader(const std::string& filename) { 
  this->counter = 0;  
  if(!bam_reader.Open(filename)) {
    std::ostringstream oss;
    oss << "Could not read BAM input from " << filename << ".";
    throw std::runtime_error(oss.str());
  }
  this->finished = !bam_reader.GetNextAlignment(next_read_aln);
  if(!this->finished) {
    counter = 1;
    this->unmapped = !next_read_aln.IsMapped();
    if(!next_read_aln.IsPrimaryAlignment()) {
      cerr << "Did not expect multi-mapped read: " << next_read_aln.Name << endl;    
      exit(0);
    }
  }
}

void PacBioBamReader::rewind() {
    bam_reader.Rewind();
    this->finished = !bam_reader.GetNextAlignment(next_read_aln);
    if(!this->finished) {
        counter = 1;
        this->unmapped = !next_read_aln.IsMapped();
        if(!next_read_aln.IsPrimaryAlignment()) {
            cerr << "Did not expect multi-mapped read: " << next_read_aln.Name << endl;
            exit(0);
        }
    }
}

bool PacBioBamReader::hasNext() const {
  return !finished;
}

void PacBioBamReader::advance() {
  assert(!finished);
  counter += 1; 
  finished = !bam_reader.GetNextAlignment(next_read_aln);
  if(!this->finished) {
    this->unmapped = !next_read_aln.IsMapped();
    if(!next_read_aln.IsPrimaryAlignment()) {
      cerr << "Did not expect multi-mapped read: " << next_read_aln.Name << endl;
      //exit(0);
    }
    //if(next_read_aln.IsReverseStrand()) {
      //cerr << "Read mapped to reverse strand. " << endl;
    //}
  }
}

const BamTools::BamAlignment* PacBioBamReader::getAlignment() const {
  return &next_read_aln;
}

bool PacBioBamReader::isUnmapped() const {
  return unmapped;
}

bool PacBioBamReader::isMultiMap() const {
    std::string tag = "XO";
    std::string value;
    next_read_aln.GetTag(tag, value);
    if(value == "UM")
        return true;
    return false;
}

bool PacBioBamReader::isUniqMap() const {
    std::string tag = "XO";
    std::string value;
    next_read_aln.GetTag(tag, value);
    if(value == "UU")
        return true;
    return false;
}

bool PacBioBamReader::isTranslocMap() const {
    std::string tag = "XO";
    std::string value;
    next_read_aln.GetTag(tag, value);
    if(value == "UT")
        return true;
    return false;
}

size_t PacBioBamReader::getNofAlnBases() const {        
  size_t m_length = 0;
  for(size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    if(cigar_op.Type=='M') {
      m_length += cigar_op.Length;
    }
  }
  return m_length;
}

size_t PacBioBamReader::sizeOfHardClip() const {
  size_t h_length = 0;
  for(size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    if(cigar_op.Type=='H') {
      h_length += cigar_op.Length;
    }
  }
  return h_length;
}

size_t PacBioBamReader::getFirstAlgRefBase() const
{
  return next_read_aln.Position;
}

size_t PacBioBamReader::getLastAlgRefBase() const
{
  size_t p_ref = next_read_aln.Position;  
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':
	  p_ref += cigar_op.Length; 
	  break;
        case 'I':
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':	  
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':  
	  p_ref += cigar_op.Length;	  	  
	  break;
        default:
	  assert(false);
    }    
  }
  return p_ref-1;
}

int PacBioBamReader::countIndelFlanks(size_t ss, size_t window_size) const
{  
  size_t p_read = 0;
  size_t p_ref = next_read_aln.Position;   
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':
	  p_read += cigar_op.Length;
	  p_ref += cigar_op.Length; 
	  break;
        case 'I':
	  p_read += cigar_op.Length;
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':
	  p_read += cigar_op.Length;
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':  	  
	  if(p_ref==ss) {	   
	    if(i>0) {
	      size_t nof_indels = 0;
	      size_t tot_m = 0;
	      for(int j=i-1; j>=0; --j)
	      {
		const BamTools::CigarOp& flank_cigar_op = next_read_aln.CigarData[j];
		switch (flank_cigar_op.Type) {
		  case 'M':
		    tot_m += flank_cigar_op.Length;
		    if(tot_m > window_size)
		      return nof_indels;
		    break;
		  case 'I':
		    nof_indels++;
		    break;
		  case 'D':
		    nof_indels++;
		    break;
		  case 'N':		   
		    cerr << "Less than 10 matching bases between introns" << endl;
		    return -1;		    
		}		
	      }
	      cerr << "Flanking region smaller than 10 matching bases" << endl;
	      return -1;
	    } else {
	      cerr << "Splice site " << ss << " has not flanking sequence." << endl;
	      return -1;
	    }
	  } else if(p_ref+cigar_op.Length==ss)
	  {	    
	    if(i<next_read_aln.CigarData.size()-1)
	    {
	      size_t nof_indels = 0;
	      size_t tot_m = 0;
	      for(int j=i+1; j<next_read_aln.CigarData.size(); ++j)
	      {
		const BamTools::CigarOp& flank_cigar_op = next_read_aln.CigarData[j];
		switch (flank_cigar_op.Type) {
		  case 'M':
		    tot_m += flank_cigar_op.Length;
		    if(tot_m > window_size)
		      return nof_indels;
		    break;
		  case 'I':
		    nof_indels++;
		    break;
		  case 'D':
		    nof_indels++;
		    break;
		  case 'N':		    
		    cerr << "Less than 10 matching bases between introns" << endl;
		    return -1;		    
		}		
	      }
	      cerr << "Flanking region smaller than 10 matching bases" << endl;
	      return -1;	      
	    } else {
	      cerr << "Splice site " << ss << " has not flanking sequence." << endl;
	      return -1;
	    }	    	    
	  }	  
	  p_ref += cigar_op.Length;
	  break;
        default:
	  assert(false);
    }
  }  
  cerr << "Splice site " << ss << " not found." << endl;    
  return 0;
}


bool PacBioBamReader::getGoodIntrons(std::vector<std::pair<unsigned, unsigned> >& introns,
                                     const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences,
                                     bool is_forward_read,
                                     int window_size,
                                     int snap_window_size,
                                     std::vector<std::pair<unsigned, unsigned> >& gene_introns) const
{
    bool not_rescued = false;
    
    assert(window_size>0); //otherwise call getIntrons
    
    const BamTools::RefVector& bam_ref_data = bam_reader.GetReferenceData();
    const std::string& ref_name = bam_ref_data[next_read_aln.RefID].RefName;
    std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it;
    it = reference_sequences->find(ref_name);
    if(it == reference_sequences->end()) {
        //cerr << "Reference \"" << ref_name << "\" missing." << endl;
        return not_rescued;
    }
    const NamedDnaSequence& ref_sequence = *it->second;
    
    size_t p_read = 0;
    size_t p_ref = next_read_aln.Position;
    //cerr << "sarting at " << p_ref << endl;
    for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
        const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
        switch (cigar_op.Type) {
            case 'M':
                p_read += cigar_op.Length;
                p_ref += cigar_op.Length;
                //cerr << "M: " << p_ref << endl;
                break;
            case 'I':
                p_read += cigar_op.Length;
                break;
            case 'D':
                p_ref += cigar_op.Length;
                break;
            case 'S':
                p_read += cigar_op.Length;
                break;
            case 'H':
                break;
            case 'P':
                break;
            case 'N':
                {
                //let's look to the left                
                bool bad_intron_left = false;
                if (i>0 && next_read_aln.CigarData[i-1].Type == 'M') { //proper N has to be proceeded by an M
                    int adj_window_size = window_size;
                    //check whether window size has to be reduced
                    if (next_read_aln.CigarData[i-1].Length < window_size) {  //if previous exon too small adjust window size or break if not limited by an N (but for example I/D)
                        if (i>1 && next_read_aln.CigarData[i-2].Type != 'N') {
                            //cerr << getReadName() << " " << p_ref << ": no proper left exon" << endl;
                            //p_ref += cigar_op.Length;
                            //break;
                            bad_intron_left = true;
                        } else
                            adj_window_size = next_read_aln.CigarData[i-1].Length;
                    }                    
                    assert(bad_intron_left || p_read >= adj_window_size);
                    assert(bad_intron_left || p_ref >= adj_window_size);
                    if(!bad_intron_left &&
                       ((is_forward_read && !next_read_aln.IsReverseStrand()) || (!is_forward_read && next_read_aln.IsReverseStrand())))
                    {
                        int nof_mismatch = 0;
                        for (size_t j=1; j <= adj_window_size; ++j) {
                            if (ref_sequence[p_ref-j] != next_read_aln.QueryBases[p_read-j]) { //if any base within window differs break (ignore intron)                                
                                nof_mismatch++;                                
                            }
                        }                        
                        if(nof_mismatch > 0) {
                            //cerr << getReadName() << " " << p_ref << ": mismatch to the left (fw)" << endl;
                            //p_ref += cigar_op.Length;
                            //break;
                            bad_intron_left = true;
                        }
                    }
                    else if(!bad_intron_left &&
                            ((is_forward_read && next_read_aln.IsReverseStrand()) || (!is_forward_read && !next_read_aln.IsReverseStrand())))
                    {
                        int nof_mismatch = 0;
                        for (size_t j=1; j <= adj_window_size; ++j) {
                            if (getComplBase(ref_sequence[p_ref-j]) != getComplBase(next_read_aln.QueryBases[p_read-j])) { //if any base within window differs break (ignore intron)                                
                                nof_mismatch++;                                
                            }
                        }                       
                        if(nof_mismatch > 0) {
                            //cerr << getReadName() << " " << p_ref << ": mismatch to the left (rv)" << endl;
                            //p_ref += cigar_op.Length;
                            //break;                            
                            bad_intron_left = true;                           
                        }
                    } else if(!bad_intron_left) //one of the previous two cases must hold unless bad_intron
                        assert(false);
                } else {
                    not_rescued = true;
                    p_ref += cigar_op.Length;
                    break;
                }

                //now let's look to the right
                bool bad_intron_right = false;
                if (i<next_read_aln.CigarData.size()-1 && next_read_aln.CigarData[i+1].Type == 'M') { //proper N has to be followed by an M
                    int adj_window_size = window_size;
                    if (next_read_aln.CigarData[i+1].Length < window_size) {  //if following exon too small adjust window size or break if not limited by an N (but for example I/D)
                        if (i<next_read_aln.CigarData.size()-2 && next_read_aln.CigarData[i+2].Type != 'N') {
                            //cerr << getReadName() << " " << p_ref << ": no proper right exon" << endl;
                            //p_ref += cigar_op.Length;
                            //break;
                            bad_intron_right = true;
                        } else
                            adj_window_size = next_read_aln.CigarData[i+1].Length;
                    }
                    assert(bad_intron_right || p_read + adj_window_size <= next_read_aln.QueryBases.size());
                    assert(bad_intron_right || p_ref + cigar_op.Length + adj_window_size <= ref_sequence.size());
                    if(!bad_intron_right &&
                       ((is_forward_read && !next_read_aln.IsReverseStrand()) || (!is_forward_read && next_read_aln.IsReverseStrand())))
                    {
                        int nof_mismatch = 0;
                        for (size_t j=0; j < adj_window_size; ++j) {                            
                            if (ref_sequence[p_ref + cigar_op.Length + j] != next_read_aln.QueryBases[p_read+j]) { //if any base within window differs break (ignore intron)                               
                                nof_mismatch++;                                
                            }
                        }
                        if(nof_mismatch > 0) {
                            //cerr << getReadName() << " " << p_ref << ": mismatch to the right (fw)" << endl;
                            //p_ref += cigar_op.Length;
                            //break;
                            bad_intron_right = true;
                        }
                    } else if(!bad_intron_right &&
                              ((is_forward_read && next_read_aln.IsReverseStrand()) || (!is_forward_read && !next_read_aln.IsReverseStrand())))
                    {
                        int nof_mismatch = 0;
                        for (size_t j=0; j < adj_window_size; ++j) {
                            if (getComplBase(ref_sequence[p_ref + cigar_op.Length + j]) != getComplBase(next_read_aln.QueryBases[p_read+j])) { //if any base within window differs break (ignore intron)                                
                                nof_mismatch++;                               
                            }
                        }
                        if(nof_mismatch > 0) {
                            //cerr << getReadName() << " " << p_ref << ": mismatch to the right (rv)" << endl;
                            //p_ref += cigar_op.Length;
                            //break;
                            bad_intron_right = true;
                        }
                    } else if(!bad_intron_right)
                        assert(false);
                } else {
                    not_rescued = true;
                    p_ref += cigar_op.Length;
                    break;
                }

                if(bad_intron_left || bad_intron_right) //try to rescue
                {
                    bool this_not_rescued = true;
                    std::vector<std::pair<unsigned, unsigned> >::const_iterator it_gi = gene_introns.begin();
                    std::vector<std::pair<unsigned, unsigned> >::const_iterator it_i;
                    int nof_sim_introns = 0;
                    for(;it_gi != gene_introns.end(); ++it_gi) {
                        unsigned dist_left = std::max(static_cast<long unsigned>(it_gi->first), p_ref) - std::min(static_cast<long unsigned>(it_gi->first),p_ref);
                        unsigned dist_right = std::max(static_cast<long unsigned>(it_gi->second), (p_ref+cigar_op.Length)) - std::min(static_cast<long unsigned>(it_gi->second), (p_ref+cigar_op.Length));                        
                        if (dist_left <= snap_window_size && dist_right <= snap_window_size) {
                            it_i = it_gi;
                            nof_sim_introns++;
                        }
                    }                    
                    if( nof_sim_introns==1 && //must be unique
                        getFirstAlgRefBase()< it_i->first && getLastAlgRefBase()>it_i->second ) { //snapped intron must not alignment range - consistency with other introns check in next step
                        if (introns.size()>0) {                            
                            if (introns.back().second < it_i->first) { //do not violate order of itrons
                                introns.push_back(*it_i);
                                this_not_rescued = false;
                                //cerr << "rescued intron " << ref_name << ": " << it_i->first << "-" << it_i->second << " in read " << getReadName() << endl;
                            }
                        } else {
                            this_not_rescued = false;
                            //cerr << "rescued intron " << ref_name << ": " << it_i->first << "-" << it_i->second << " in read " << getReadName() << endl;
                            introns.push_back(*it_i);
                        }
                    }
                    not_rescued = not_rescued || this_not_rescued;
                    p_ref += cigar_op.Length;
                    break;
                }
                else
                {
                    assert(introns.size()==0 || introns.back().second < p_ref);
                    introns.push_back(std::make_pair(p_ref,p_ref+cigar_op.Length));
                    p_ref += cigar_op.Length;
                    break;
                }
                }
            default:
                assert(false);
        }
    }
    return not_rescued;
}


void PacBioBamReader::getIntrons(std::vector<std::pair<unsigned, unsigned> >& introns) const
{ 
  size_t p_read = 0;
  size_t p_ref = next_read_aln.Position;   
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':
	  p_read += cigar_op.Length;
	  p_ref += cigar_op.Length; 
	  break;
        case 'I':
	  p_read += cigar_op.Length;
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':
	  p_read += cigar_op.Length;
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':  	  
	  //if(getReadName()=="c26420/1/1561")
	  //cerr << p_ref << " " << cigar_op.Length << endl;
	  introns.push_back(std::make_pair(p_ref,p_ref+cigar_op.Length)); 
	  p_ref += cigar_op.Length;
	  break;
        default:
	  assert(false);
    }
  }
}

char PacBioBamReader::getComplBase(char base) const 
{
  switch(base) {
    case 'A': return 'T';
    case 'T': return 'A';
    case 'G': return 'C';
    case 'C': return 'G';
    default: return 'N';
  }
}

void PacBioBamReader::getDimers(const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences, std::vector<std::pair<string, string> >& dimers, bool is_forward_read) const
{
  const BamTools::RefVector& bam_ref_data = bam_reader.GetReferenceData();
  const std::string& ref_name = bam_ref_data[next_read_aln.RefID].RefName;
  std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it;
  it = reference_sequences->find(ref_name);
  if(it == reference_sequences->end()) {
    //cerr << "Reference \"" << ref_name << "\" missing." << endl;
    return;
  } 
  
  const NamedDnaSequence& ref_sequence = *it->second;
  size_t p_read = 0;
  size_t p_ref = next_read_aln.Position;   
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':
	  p_read += cigar_op.Length;
	  p_ref += cigar_op.Length; 
	  break;
        case 'I':
	  p_read += cigar_op.Length;
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':
	  p_read += cigar_op.Length;
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':  
	  if(cigar_op.Length>=4) {
	    if((is_forward_read && next_read_aln.IsReverseStrand()) || (!is_forward_read && !next_read_aln.IsReverseStrand()))
	      cout <<  getComplBase(ref_sequence[p_ref+cigar_op.Length-1]) << getComplBase(ref_sequence[p_ref+cigar_op.Length-2]) << "/" << getComplBase(ref_sequence[p_ref+1]) << getComplBase(ref_sequence[p_ref]) << endl;
	    else if((is_forward_read && !next_read_aln.IsReverseStrand()) || (!is_forward_read && next_read_aln.IsReverseStrand()))
	      cout << ref_sequence[p_ref] << ref_sequence[p_ref+1] << "/" << ref_sequence[p_ref+cigar_op.Length-2] << ref_sequence[p_ref+cigar_op.Length-1] << endl;
	  }
	  //introns.push_back(std::make_pair(p_ref,p_ref+cigar_op.Length)); 
	  p_ref += cigar_op.Length;
	  break;
        default:
	  assert(false);
    }
  }
}


size_t PacBioBamReader::getExons(std::vector<std::pair<unsigned, unsigned> >& exons) const
{  
  size_t p_read = 0;
  size_t p_ref = next_read_aln.Position;  
  size_t current_exon_begin = next_read_aln.Position;
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':
	  p_read += cigar_op.Length;
	  p_ref += cigar_op.Length; 
	  break;
        case 'I':
	  p_read += cigar_op.Length;
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':
	  p_read += cigar_op.Length;	  
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':  
	  exons.push_back(std::make_pair(current_exon_begin, p_ref));
	  p_ref += cigar_op.Length;
	  current_exon_begin = p_ref;	  	  
	  break;
        default:
	  assert(false);
    }    
  }
  exons.push_back(std::make_pair(current_exon_begin, p_ref));
}


size_t PacBioBamReader::getNofIdentAlnBases(const std::unordered_map<std::string,NamedDnaSequence*>* reference_sequences) const 
{   
  const BamTools::RefVector& bam_ref_data = bam_reader.GetReferenceData();
  const std::string& ref_name = bam_ref_data[next_read_aln.RefID].RefName;
  std::unordered_map<std::string,NamedDnaSequence*>::const_iterator it;
  it = reference_sequences->find(ref_name);
  if(it == reference_sequences->end()) {
    //cerr << "Reference \"" << ref_name << "\" missing." << endl;
    return 0;
  } 

  size_t nof_ident_bases = 0;
  const NamedDnaSequence& ref_sequence = *it->second;
  size_t p_read = 0;
  size_t p_ref = next_read_aln.Position;   
  for (size_t i=0; i<next_read_aln.CigarData.size(); ++i) {
    const BamTools::CigarOp& cigar_op = next_read_aln.CigarData[i];
    switch (cigar_op.Type) {
	case 'M':	
	  for (size_t j=0; j<cigar_op.Length; ++j) {
	    if(next_read_aln.QueryBases[p_read] == ref_sequence[p_ref]) nof_ident_bases++;
	    p_read += 1;
	    p_ref += 1;
	  }
	  break;
        case 'I':
	  p_read += cigar_op.Length;
	  break;
        case 'D':
	  p_ref += cigar_op.Length;
	  break;
        case 'S':
	  p_read += cigar_op.Length;
	  break;
        case 'H':
	  break;
	case 'P':
	  break;
	case 'N':	  
	  p_ref += cigar_op.Length;	
	  break;
        default:
	  assert(false);
    }
  }
  return nof_ident_bases;
}

size_t PacBioBamReader::getReadLength() const {
  return next_read_aln.Length;
}

const std::string& PacBioBamReader::getRefName() const {
  const BamTools::RefVector& bam_ref_data = bam_reader.GetReferenceData();
  return bam_ref_data[next_read_aln.RefID].RefName;
}

bool PacBioBamReader::isReverseStrand() const {
  return next_read_aln.IsReverseStrand();
}

