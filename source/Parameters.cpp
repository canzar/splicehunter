/* This program is free software: you can redistribute it and/or modify
 *
 * Copyright (C) 2013-2015 Stefan Canzar
 *
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 2 of the License, or
 * (at your option) any later version.

 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.

 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

#include "Parameters.h"

namespace Parameters {

float min_overlap_diff_strand = 2.0;
float min_overlap_same_strand = 0.9;

float ambig_res_thr = 1.5;

unsigned ss_window = 10;
unsigned ss_snap_window = 10;
    
unsigned tsstes_window = -1;
unsigned snap_dist = -1;
    
float mapqual = 0.75;
float maplength = 0.67;
    
string protseq_file = "protseq.fa";
string novel_rna_file = "novel_rna.fa";
string as_rna_file = "as_rna.fa";
string hexamer_file = ""; //off by default
string dimer_file = ""; //off by default
string retint_length_file = ""; //off by default
string pwcount_file = ""; //off by default
string nc_anno_file = ""; //off by default
string anno_intron_file = ""; //off by default
string cert_file = "certificates.txt";
string iso_file = ""; //write to stdout by default
}
